/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonObject
 *  com.smartfoxserver.v2.api.ISFSApi
 *  com.smartfoxserver.v2.api.SFSApi
 *  com.smartfoxserver.v2.entities.SFSUser
 *  com.smartfoxserver.v2.entities.User
 *  com.smartfoxserver.v2.entities.data.ISFSObject
 *  com.smartfoxserver.v2.entities.data.SFSObject
 */
package gameserver.tithandler;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.api.SFSApi;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.TITExtension;

public abstract class TITZoneExtension
extends TITExtension {
    public TITApi getTITApi() {
        SFSApi sfsApi = (SFSApi)this.getApi();
        return new TITApi(sfsApi);
    }

    public void send(String cmd, SFSObject dataToClient, ArrayList<TITUser> userArr) {
        for (int i = 0; i < userArr.size(); ++i) {
            TITUser titUser = userArr.get(i);
            this.send(cmd, dataToClient, titUser);
        }
    }

    public void send(String cmd, SFSObject toClientData, TITUser user) {
        SFSObject sfsObject = new SFSObject();
        sfsObject.putUtfString("data", toClientData.toJson());
        super.send(cmd, (ISFSObject)sfsObject, (User)user.getSFSUser());
    }

    public void send(String cmd, JsonObject toClientData, ArrayList<TITUser> userArr) {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        this.send(cmd, sfsObject, userArr);
    }

    public void send(String cmd, JsonObject toClientData, TITUser user) {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        this.send(cmd, sfsObject, user);
    }
}

