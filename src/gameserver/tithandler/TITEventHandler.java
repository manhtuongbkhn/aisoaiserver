package gameserver.tithandler;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.api.SFSApi;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.KJS;

public abstract class TITEventHandler extends BaseServerEventHandler 
{
    protected SFSEvent event;
    protected TITUser user;
    protected SFSObject data;

    public void handleServerEvent(ISFSEvent event) throws SFSException 
    {
        init((SFSEvent)event);
        if (check()) 
        {
            changeUserStatusBegin();
            titHandleServerEvent();
            changeUserStatusEnd();
        } 
        else 
        {
            eventFalse();
        }
    }

    protected void init(SFSEvent event) 
    {
        this.event=event;
        
        switch (event.getType()) 
        {
            case USER_LOGIN: 
                data = (SFSObject)event.getParameter
                                                (SFSEventParam.LOGIN_IN_DATA);
                break;
            case USER_LOGOUT:
                data = (SFSObject)event.getParameter
                                                (SFSEventParam.LOGIN_OUT_DATA);
                break;
        }
        SFSUser sfsUser = (SFSUser)event.getParameter(SFSEventParam.USER);
        user = new TITUser(sfsUser);
    }

    public boolean check() 
    {
        if (checkUserStatus()) 
        {
            return checkRequestData();
        }
        return false;
    }

    public void eventFalse() 
    {
        trace(getClass().getName() + "Event False\n");
    }

    public void send(String cmd, SFSObject dataToClient,
                                                    ArrayList<TITUser> userArr) 
    {
        for (int i = 0; i < userArr.size();i++) 
        {
            TITUser titUser = userArr.get(i);
            send(cmd, dataToClient, titUser);
        }
    }

    public void send(String cmd, SFSObject toClientData, TITUser user) 
    {
        SFSObject sfsObject = new SFSObject();
        sfsObject.putUtfString(KJS.DATA, toClientData.toJson());
        super.send(cmd,sfsObject,user.getSFSUser());
    }

    public void send(String cmd, JsonObject toClientData,
                                                    ArrayList<TITUser> userArr) 
    {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        send(cmd, sfsObject, userArr);
    }

    public void send(String cmd, JsonObject toClientData, TITUser user) 
    {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        send(cmd, sfsObject, user);
    }

    public TITApi getTITApi() 
    {
        SFSApi sfsApi = (SFSApi)getApi();
        return new TITApi(sfsApi);
    }


    public void titTrace(String message) 
    {
        this.trace(message);
    }

    public TITUser getUser() 
    {
        return  user;
    }

    protected abstract void titHandleServerEvent() throws SFSException;

    protected abstract boolean checkUserStatus();

    protected abstract boolean checkRequestData();

    protected abstract void changeUserStatusBegin();

    protected abstract void changeUserStatusEnd();

}

