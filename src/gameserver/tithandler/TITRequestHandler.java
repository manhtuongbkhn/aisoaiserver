package gameserver.tithandler;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.api.SFSApi;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.SFSZone;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.TITZone;
import gameserver.titconfig.KJS;

public abstract class TITRequestHandler extends BaseClientRequestHandler
{
    protected TITZone zone;
    protected TITUser user;
    protected SFSObject fromClientData;

    public void handleClientRequest(User iuser, ISFSObject clientData) 
    {
        init(iuser, clientData);
        if (check()) 
        {
            changeUserStatusBegin();
            titHandClientRequest();
            changeUserStatusEnd();
        } 
        else 
        {
            requestFalse();
        }
    }

    public boolean check() 
    {
        if (checkUserStatus()) 
        {
            return checkRequestData();
        }
        return false;
    }

    public void init(User iuser, ISFSObject clientData) 
    {
        SFSUser sfsUser = (SFSUser)iuser;
        SFSZone sfsZone = (SFSZone)getParentExtension().getParentZone();
        user = new TITUser(sfsUser);
        zone = new TITZone(sfsZone);
        fromClientData = (SFSObject)clientData;
    }

    public void send(String cmd, SFSObject dataToClient,
                                                    ArrayList<TITUser> userArr) 
    {
        for (int i = 0; i < userArr.size();i++) 
        {
            TITUser titUser = userArr.get(i);
            send(cmd, dataToClient, titUser);
        }
    }

    public void send(String cmd, SFSObject toClientData, TITUser user) 
    {
        SFSObject sfsObject = new SFSObject();
        sfsObject.putUtfString(KJS.DATA,toClientData.toJson());
        super.send(cmd,sfsObject,user.getSFSUser());
    }

    public void send(String cmd, JsonObject toClientData,
                                                    ArrayList<TITUser> userArr) 
    {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        send(cmd, sfsObject, userArr);
    }

    public void send(String cmd, JsonObject toClientData, TITUser user) 
    {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        send(cmd, sfsObject, user);
    }

    public TITApi getTITApi() 
    {
        SFSApi sfsApi = (SFSApi)this.getApi();
        return new TITApi(sfsApi);
    }

    public void requestFalse() 
    {
        trace(getClass().getName() + "Request False\n");
    }

    public void titTrace(String message) 
    {
        trace(message);
    }

    public TITUser getUser() 
    {
        return  user;
    }

    protected abstract void titHandClientRequest();

    protected abstract boolean checkUserStatus();

    protected abstract boolean checkRequestData();

    protected abstract void changeUserStatusBegin();

    protected abstract void changeUserStatusEnd();
}

