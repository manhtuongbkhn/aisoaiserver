package gameserver.tithandler;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.tithandler.TITRequestHandler;

public abstract class TITRoomRequestHandler extends TITRequestHandler 
{
    protected TITRoom room;

    @Override
    public void handleClientRequest(User iuser, ISFSObject clientData) 
    {
        init(iuser, clientData);
        if (check()) 
        {
            changeUserStatusBegin();
            changeRoomStatusBegin();
            titHandClientRequest();
            changeUserStatusEnd();
            changeRoomStatusEnd();
        } 
        else 
            requestFalse();
    }

    @Override
    public void init(User iuser, ISFSObject clientData) 
    {
        super.init(iuser, clientData);
        room = user.getDefaultUserInfo().getRoom();
    }

    @Override
    public boolean check() 
    {
        if (checkUserStatus()) 
        {
            if (checkRoomStatus()) 
            {
                return checkRequestData();
            }
            return false;
        }
        return false;
    }
    
    protected abstract boolean checkRoomStatus();

    protected abstract void changeRoomStatusBegin();

    protected abstract void changeRoomStatusEnd();
}

