package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.exceptions.SFSException;
import gameserver.tithandler.TITRoomEventHanler;

public class RoomRemoveEventHandler extends TITRoomEventHanler
{
    @Override
    protected void titHandleServerEvent() throws SFSException 
    {
        switch(room.getDefaultRoomInfo().getType())
        {
            case 1:
                room.destroyInvitationManager();
                room.destroyMessageManager();
                room.destroyGameContext();
                room.destroyDefaultRoomInfo();
                break;
        }
    }
    
    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        
    }
    
    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
        
    }
    
    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        return true;
    }
    
}
