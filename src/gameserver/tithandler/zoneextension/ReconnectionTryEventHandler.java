/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  com.smartfoxserver.v2.exceptions.SFSException
 */
package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.exceptions.SFSException;
import gameserver.tithandler.TITEventHandler;

public class ReconnectionTryEventHandler
extends TITEventHandler {
    @Override
    protected void titHandleServerEvent() throws SFSException {
        this.trace(new Object[]{"Reconnection Try\n"});
    }

    @Override
    protected void changeUserStatusBegin() {
    }

    @Override
    protected void changeUserStatusEnd() {
    }

    @Override
    protected boolean checkUserStatus() {
        return true;
    }

    @Override
    protected boolean checkRequestData() {
        return true;
    }
}

