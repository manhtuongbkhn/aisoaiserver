package gameserver.tithandler.zoneextension;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.TITEventHandler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titdatabase.TITDatabase;

public class LogoutEventHandler extends TITEventHandler 
{
    @Override
    protected void titHandleServerEvent() throws SFSException 
    {
        releaseRoom();
        redloadFriendManager();
        releaseUserVariable();
        updateDatabase();
    }
 
    protected void releaseRoom() 
    {
        TITRoom room=null;
        Integer backupRoomId=user.getDefaultUserInfo().getBackupRoomId();
        
        if(backupRoomId!=null)
        {
            room=user.getDefaultUserInfo().getZone().getRoomById(backupRoomId);
        }
        
        if(room!=null)
        {
            switch (room.getDefaultRoomInfo().getType()) 
            {
                case 1:
                    getTITApi().exitRoom(user,room);
                    if (room.getDefaultRoomInfo().getOnlinePlayerCount()==0)
                    {
                            getTITApi().removeRoom(room);
                    }
                    else
                    {
                        if(room.getDefaultRoomInfo().getOwner().equals(user))
                        {
                            System.out.println("Change Owner");
                            int radom = TITFunction.randInt(room.
                                getDefaultRoomInfo().getOnlinePlayerCount());
                            TITUser newOwner = room.getDefaultRoomInfo().
                                            getOnlinePlayerArr().get(radom-1);
                            room.getDefaultRoomInfo().setOwner(newOwner);
                        }
                        reloadRoomInfo(room);
                        reloadPlayerList(room);
                    }
                    break;
                case 2:
                    checkChallengeRoomStatus(room);
                    break;
            }
        }
    }

    private void reloadRoomInfo(TITRoom room) 
    {
        SFSObject roomInfo=new SFSObject();
        room.getDefaultRoomInfo().getPublicInfo(roomInfo);
        this.send(CMDNF.ROOMINFO_NF,roomInfo,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }

    private void reloadPlayerList(TITRoom room) 
    {
        SFSArray userInfoArray = room.getDefaultRoomInfo().
                                                        getAllUserPublicInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,userInfoArray);
        this.send(CMDNF.ALLPLAYERINFO_NF,toClientData,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }
    
    private void checkChallengeRoomStatus(TITRoom room) 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
                break;
        }
    }

    private void redloadFriendManager() 
    {
        user.getFriendManager().destroy(user);
    }

    private void releaseUserVariable()
    {
        user.getUserServingManager().offAllServing();
        user.destroyServingManager();
        user.destroyFriendManager();
        user.destroyGameUserInfo();
        //user.destroyUserProfile();
        //user.destroyDefaultUserInfo();
    }
    
    protected void updateDatabase() 
    {
        updateOnline();
        updateSystemUserId();
    }

    protected void updateOnline() 
    {
        TITDatabase titDatabase = TITDatabase.getInstance();
        JsonObject input = new JsonObject();
        input.addProperty(KJS.USER_ID,user.getUserProfile().getUserId());
        input.addProperty(KJS.ONLINE,false);
        titDatabase.updateOnline(input);
    }

    protected void updateSystemUserId()
    {
        TITDatabase titDatabase = TITDatabase.getInstance();
        JsonObject input = new JsonObject();
        input.addProperty(KJS.USER_ID,user.getUserProfile().getUserId());
        input.addProperty(KJS.SYSTEM_USER_ID,-1);
        titDatabase.updateSystemUserId(input);
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        this.user.setStatus(TITUserStatus.OFFLINE);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

