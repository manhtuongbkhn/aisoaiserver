package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;

public class UserInfoRequestHandler
extends TITRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        if (user.getUserProfile() == null) 
        {
            try 
            {
                Thread.sleep(300);
            }
            catch (InterruptedException ex) {}
        }
        this.sendDataToClient();
    }

    private void sendDataToClient() 
    {
        SFSObject userInfo = this.user.getPublicInfo();
        this.send(CMDRP.USERPROFILE_RP,userInfo,user);
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        this.user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        this.user.setStatus(TITUserStatus.GETED_PROFILE);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case GETED_PROFILE: 
            {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

