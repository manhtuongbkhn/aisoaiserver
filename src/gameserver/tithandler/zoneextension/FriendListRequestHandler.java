package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class FriendListRequestHandler extends TITRequestHandler {
    @Override
    protected void titHandClientRequest() 
    {
        //System.out.println("Friend List Request");
        
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS,true);
        send(CMDRP.FRIENDLIST_RP,toClientData,user);
        
        user.getUserServingManager().getFriendListUserServing().onServing();
    }

    @Override
    protected void changeUserStatusBegin() {
    }

    @Override
    protected void changeUserStatusEnd() {
    }

    @Override
    protected boolean checkUserStatus() {
        switch (this.user.getStatus()) {
            case GETED_PROFILE: {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() {
        return true;
    }

}

