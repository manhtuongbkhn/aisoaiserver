package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class JoinRoomRequestHandler
extends TITRequestHandler {
    private boolean requestSucess = false;

    @Override
    protected void titHandClientRequest() 
    {
        int roomId = fromClientData.getInt(KJS.ROOM_ID);
        TITRoom room = user.getDefaultUserInfo().getZone().getRoomById(roomId);
        checkRoomExist(room);
    }

    private void checkRoomExist(TITRoom room) 
    {
        if (room != null) 
            this.checkRoomFull(room);
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,StrDefine.ROOM_NOT_EXIST);
            send(CMDRP.JOINROOM_RP,toClientData, this.user);
        }
    }

    private void checkRoomFull(TITRoom room) 
    {
        if (room.getDefaultRoomInfo().getPlayerCount()<
                                    room.getDefaultRoomInfo().getPlayerMax()) 
            this.checkRoomStatus(room); 
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,StrDefine.ROOM_FULL);
            this.send(CMDRP.JOINROOM_RP,toClientData,user);
        }
    }

    private void checkRoomStatus(TITRoom room) 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING: 
                checkRoomPass(room);
                break;
            default:
                SFSObject toClientData = new SFSObject();
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                            StrDefine.ROOM_STATUS_INVALIDATE);
                send(CMDRP.JOINROOM_RP,toClientData,user);
                break;
        }
    }

    private void checkRoomPass(TITRoom room) 
    {
        String roomPass = fromClientData.getUtfString(KJS.ROOM_PASS);
        if (roomPass.endsWith(room.getDefaultRoomInfo().getPass())) 
            joinRoom(room);
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,StrDefine.PASSWORD_FAIL);
            send(CMDRP.JOINROOM_RP,toClientData,user);
        }
    }

    private void joinRoom(TITRoom room) 
    {
        String pass=room.getDefaultRoomInfo().getPass();
        requestSucess = getTITApi().joinRoom(user,room,pass);
        SFSObject toClientData = new SFSObject();
        if (this.requestSucess) 
        {
            toClientData.putBool(KJS.SUCESS, true);
            send(CMDRP.JOINROOM_RP,toClientData,user);
        } 
        else 
        {
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString("system_message", "SYSTEM_ERROR");
            this.send("join_room_rp", toClientData, this.user);
        }
    }

    @Override
    protected void changeUserStatusBegin() {
        this.user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() {
        if (this.requestSucess) {
            this.user.setStatus(TITUserStatus.WAITING_INROOM);
        } else {
            this.user.setStatus(TITUserStatus.GETED_PROFILE);
        }
    }

    @Override
    protected boolean checkUserStatus() {
        switch (this.user.getStatus()) {
            case GETED_PROFILE: {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() {
        Integer roomId = this.fromClientData.getInt("room_id");
        String roomName = this.fromClientData.getUtfString("room_name");
        String roomPass = this.fromClientData.getUtfString("room_pass");
        if (roomId == null) {
            return false;
        }
        if (roomName == null) {
            return false;
        }
        if (roomPass == null) {
            return false;
        }
        return true;
    }

}

