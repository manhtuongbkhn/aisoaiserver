package gameserver.tithandler.zoneextension;

import gameserver.titsystem.titservice.challenge.ChallengeService;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.user.TITChallengeUserInfo;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class CancelChallengeRequestHandler extends TITRequestHandler
{
    private boolean requestSucess=false;
    @Override
    protected void titHandClientRequest() 
    {
        TITChallengeUserInfo challengeUserInfo=user.getChallengeUserInfo();
        int playType=challengeUserInfo.getPlayType();
        int playerCount=challengeUserInfo.getPlayerCount();
        boolean sucess=ChallengeService.unregisterUser(user,playType,playerCount);
        requestSucess=sucess;
        SFSObject toClientData=new SFSObject();
        toClientData.putBool(KJS.SUCESS,sucess);
        send(CMDRP.CANCEL_CHALLENGE_RP,toClientData,user);
    }
    
    @Override
    protected void changeUserStatusBegin() 
    {
        user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        if(requestSucess)
            user.setStatus(TITUserStatus.GETED_PROFILE);
        else
            user.setStatus(TITUserStatus.WAITING_INQUEUE);
    }
    
    @Override
    protected boolean checkUserStatus() 
    {
        switch(user.getStatus())
        {
            case WAITING_INQUEUE:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}
