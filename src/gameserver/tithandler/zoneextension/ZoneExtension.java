package gameserver.tithandler.zoneextension;

import gameserver.titsystem.titservice.challenge.ChallengeService;
import com.smartfoxserver.v2.core.SFSEventType;
import java.sql.Connection;
import java.sql.SQLException;
import gameserver.titsystem.titservice.TopPlayerService;
import gameserver.tithandler.TITZoneExtension;
import gameserver.titconfig.CMDRQ;
import gameserver.titdatabase.TITDatabase;
import gameserver.titgameinfo.TITGameInfoFactory;

public class ZoneExtension extends TITZoneExtension 
{
    public void init() 
    {
        initSystem();
        initEventHandler();
        initRequestHandler();
    }

    private void initSystem() 
    {
        initDatabase();
        TITGameInfoFactory.init();
        initChallenge();
        initTopPlayerManager();
        defautZoneExtension=ZoneExtension.this;
    }

    private void initEventHandler() 
    {
        addEventHandler(SFSEventType.USER_LOGIN,LoginEventHandler.class);
        addEventHandler(SFSEventType.USER_JOIN_ZONE,JoinZoneEventHandler.class);
        addEventHandler(SFSEventType.USER_LOGOUT,LogoutEventHandler.class);
        addEventHandler(SFSEventType.USER_DISCONNECT,
                                                DisconnectEventHandler.class);
        addEventHandler(SFSEventType.USER_RECONNECTION_TRY,
                                            ReconnectionTryEventHandler.class);
        addEventHandler(SFSEventType.USER_RECONNECTION_SUCCESS,
                                            ReconnectionTryEventHandler.class);
        addEventHandler(SFSEventType.ROOM_REMOVED,RoomRemoveEventHandler.class);
    }

    private void initRequestHandler() 
    {
        addRequestHandler(CMDRQ.KEEPCONNECTION_RQ,
                                            KeepConnectionRequestHandler.class);
        addRequestHandler(CMDRQ.USERPROFILE_RQ,
                                                UserInfoRequestHandler.class);
        addRequestHandler(CMDRQ.CHALLENGE_RQ,
                                                ChallengeRequestHandler.class);
        addRequestHandler(CMDRQ.CANCEL_CHALLENGE_RQ,
                                        CancelChallengeRequestHandler.class);
        addRequestHandler(CMDRQ.TOP_PLAYER_RQ,TopPlayerRequestHandler.class);
        addRequestHandler(CMDRQ.CANCEL_TOP_PLAYER_RQ,
                                        CancelTopPlayerRequestHandler.class);
        addRequestHandler(CMDRQ.CREATEROOM_RQ,
                                                CreateRoomRequestHandler.class);
        addRequestHandler(CMDRQ.JOINROOM_RQ,JoinRoomRequestHandler.class);
        addRequestHandler(CMDRQ.INVITATIONANSWER_RQ,
                                        InvitationAnswerRequestHandler.class);
        addRequestHandler(CMDRQ.ROOMLIST_RQ,
                                                RoomListRequestHandler.class);
        addRequestHandler(CMDRQ.CANCELROOMLIST_RQ,
                                            CancelRoomListRequestHandler.class);
        addRequestHandler(CMDRQ.FRIENDLIST_RQ,
                                                FriendListRequestHandler.class);
        addRequestHandler(CMDRQ.CANCELFRIENDLIST_RQ,
                                        CancelFriendListRequestHandler.class);
        addRequestHandler(CMDRQ.BACKROOMFINISH_RQ,
                                            BackRoomFinishRequestHandler.class);
        addRequestHandler(CMDRQ.EXITROOMFINISH_RQ,
                                            ExitRoomFinishRequestHandler.class);
        this.addRequestHandler("xxx",XXXRequestHandler.class);
    }

    private void initDatabase() 
    {
        try 
        {
            Connection connection =getParentZone().getDBManager().getConnection();
            TITDatabase.getInstance(connection);
            trace("\n------------Database Sucess------------\n");
        }
        catch (SQLException ex) 
        {
            trace("\n------------Database Error ------------\n");
            trace("\n--------" + ex.toString() + "---------\n");
        }
    }

    private void initChallenge() 
    {
        Thread thread=new Thread()
        {
            @Override
            public void run()
            {
                ChallengeService.init(ZoneExtension.this);
            }
        };
        thread.start();
    }
    
    private void initTopPlayerManager()
    {
        Thread thread=new Thread()
        {
            @Override
            public void run()
            {
                TopPlayerService.start();
            }
        };
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
    }
    
    private static TITZoneExtension defautZoneExtension;

    public static TITZoneExtension getDefautZoneExtension() 
    {
        return defautZoneExtension;
    }      
}

