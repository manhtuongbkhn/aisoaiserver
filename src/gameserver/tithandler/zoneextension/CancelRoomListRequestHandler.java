package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class CancelRoomListRequestHandler extends TITRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        //System.out.println("Cancel Room List Request");
        
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        send(CMDRP.CANCELROOMLIST_RP, toClientData,user);
        
        user.getUserServingManager().getRoomListUserServing().offServing();
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }
}

