package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.roomextension.playthread.NormalBeginRoomThread;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.tithandler.TITRequestHandler;
import gameserver.tithandler.roomextension.playthread.TITRoomService;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class CreateRoomRequestHandler extends TITRequestHandler
{
    private boolean requestSucess;
    private TITRoom room;

    @Override
    protected void titHandClientRequest() 
    {
        SFSObject dataToClient = new SFSObject();
        int roomType = 1;
        String roomPass = fromClientData.getUtfString(KJS.ROOM_PASS);
        int playerMax = fromClientData.getInt(KJS.PLAYER_MAX);
        room = getTITApi().createRoom(roomType, roomPass, playerMax,user);
        if (room != null) 
        {
            initRoomVariables();
            dataToClient.putBool(KJS.SUCESS,true);
            send(CMDRP.CREATEROOM_RP,dataToClient,user);
            requestSucess = true;
            SFSObject roomInfo=new SFSObject();
            room.getDefaultRoomInfo().getPublicInfo(roomInfo);
            send(CMDNF.ROOMINFO_NF,roomInfo,room.getDefaultRoomInfo().
                                                                getPlayerArr());
            SFSArray allUserInfo = room.getDefaultRoomInfo().
                                                        getAllUserPublicInfo();
            dataToClient = new SFSObject();
            dataToClient.putSFSArray(KJS.ARRAY,allUserInfo);
            send(CMDNF.ALLPLAYERINFO_NF,dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        } 
        else 
        {
            dataToClient.putBool(KJS.SUCESS, false);
            dataToClient.putUtfString(KJS.SYSTEM_MESSAGE,StrDefine.SYSTEM_ERROR);
            send(CMDRP.CREATEROOM_RP, dataToClient,user);
            requestSucess = false;
        }
    }

    private void initRoomVariables() 
    {
        String invite = fromClientData.getUtfString(KJS.ROOM_INVITE);
        
        if(invite.equals(""))
        {
            invite=StrDefine.DEFAULT_ROOM_INVITE;
        }
        
        int roomAvatar = fromClientData.getInt(KJS.ROOM_AVATAR);
        int playType = fromClientData.getInt(KJS.PLAY_TYPE);
        room.getDefaultRoomInfo().setInvite(invite);
        room.getDefaultRoomInfo().setAvatar(roomAvatar);
        room.getDefaultRoomInfo().setPlayType(playType);
        TITRoomService roomService=new TITRoomService(room);
        NormalBeginRoomThread beginRoomThread = new NormalBeginRoomThread(roomService);
        beginRoomThread.start();
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        if (requestSucess) 
            user.setStatus(TITUserStatus.WAITING_INROOM);
        else 
            user.setStatus(TITUserStatus.GETED_PROFILE);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case GETED_PROFILE: 
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        try 
        {
            String roomInvite = fromClientData.getUtfString(KJS.ROOM_INVITE);
            String roomPass = fromClientData.getUtfString(KJS.ROOM_PASS);
            Integer playerMax = fromClientData.getInt(KJS.PLAYER_MAX);
            Integer playType = fromClientData.getInt(KJS.PLAY_TYPE);
            Integer roomAvatar = fromClientData.getInt(KJS.ROOM_AVATAR);
            if (roomInvite == null) 
            {
                return false;
            }
            if (roomPass == null) 
            {
                return false;
            }
            if (playerMax == null) 
            {
                return false;
            }
            if (playType == null) 
            {
                return false;
            }
            if (roomAvatar == null) 
            {
                return false;
            }
            return this.checkValidateInfo(roomInvite, roomPass, playerMax, playType, roomAvatar);
        }
        catch (Exception ex) 
        {
            return false;
        }
    }

    private boolean checkValidateInfo(String roomInvite, String roomPass,
                                    int playerMax, int playType, int roomAvatar) 
    {
        return true;
    }
}

