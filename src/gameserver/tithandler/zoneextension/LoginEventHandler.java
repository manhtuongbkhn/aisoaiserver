package gameserver.tithandler.zoneextension;

import com.google.gson.JsonObject;
import com.smartfoxserver.bitswarm.sessions.Session;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.exceptions.SFSErrorCode;
import com.smartfoxserver.v2.exceptions.SFSErrorData;
import com.smartfoxserver.v2.exceptions.SFSLoginException;
import java.util.Date;
import gameserver.tithandler.TITEventHandler;
import gameserver.titconfig.KJS;
import gameserver.titdatabase.TITDatabase;
import gameserver.titfacebook.TITFacebook;

public class LoginEventHandler extends TITEventHandler 
{
    private JsonObject dataJson = new JsonObject();
    private TITFacebook facebook;
    private TITDatabase database;

    @Override
    public void titHandleServerEvent() throws SFSLoginException 
    {
        String accessToken = this.data.getUtfString(KJS.ACCESS_TOKEN);
        facebook = new TITFacebook(accessToken);
        database = TITDatabase.getInstance();
        dataJson.addProperty(KJS.ACCESS_TOKEN, accessToken);
        facebook.checkAccessToken(dataJson);
        if (dataJson.get(KJS.RESULT).getAsBoolean()) 
            checkFacebookId();
        else 
            loginError();
    }

    private void checkFacebookId() throws SFSLoginException 
    {
        System.out.println("Check Facebook Id");
        String userName = (String)event.getParameter(SFSEventParam.LOGIN_NAME);
        String facebookId = dataJson.get(KJS.FACEBOOK_ID).getAsString();
        if (facebookId.equals(userName)) 
            checkFacebookIdExist();
        else 
        {
            dataJson.addProperty(KJS.EXCEPTION, "Invalidate Facebook Id ");
            loginError();
        }
    }

    private void checkFacebookIdExist() throws SFSLoginException 
    {
        trace("check FacebookId Exist");
        String facebookId = dataJson.get(KJS.FACEBOOK_ID).getAsString();
        JsonObject input = new JsonObject();
        input.addProperty(KJS.FACEBOOK_ID,facebookId);
        database.checkIdFacebookExist(input);
        if (input.get(KJS.SUCESS).getAsBoolean()) 
        {
            if (input.get(KJS.RESULT).getAsBoolean()) 
            {
                String userId = input.get(KJS.USER_ID).getAsString();
                String registerDate = input.get(KJS.REGISTER_DATE).getAsString();
                dataJson.addProperty(KJS.USER_ID, userId);
                dataJson.addProperty(KJS.REGISTER_DATE,registerDate);
                checkAccessTokenExist();
            } 
            else 
                signUp();
        } 
        else 
        {
            loginError();
        }
    }

    private void checkAccessTokenExist() throws SFSLoginException 
    {
        System.out.println("Check Access Token Exist");
        database.checkAccessTokenExist(dataJson);
        if (dataJson.get(KJS.SUCESS).getAsBoolean()) 
        {
            if (!dataJson.get(KJS.RESULT).getAsBoolean())
            {
                updateProfile();
            }
            else 
            {
                loginError();
            }
        } 
        else 
        {
            loginError();
        }
    }

    private void updateProfile() throws SFSLoginException 
    {
        trace("update Profile");
        database.updateProfile(dataJson);
        if (dataJson.get(KJS.SUCESS).getAsBoolean()) 
        {
            JsonObject input = new JsonObject();
            String userId = dataJson.get(KJS.USER_ID).getAsString();
            String accessToken = dataJson.get(KJS.ACCESS_TOKEN).getAsString();
            Date timeLogin = new Date();
            String timeLoginStr = timeLogin.toLocaleString();
            input.addProperty(KJS.USER_ID,userId);
            input.addProperty(KJS.LOGIN_TIME, timeLoginStr);
            input.addProperty(KJS.LOGOUT_TIME, "");
            input.addProperty(KJS.ACCESS_TOKEN,accessToken);
            JsonObject jsonObject = database.getLoginHistoryTb().
                                                        insertRecord(input);
            if (jsonObject.get(KJS.SUCESS).getAsBoolean())
                getUserGameInfo();
            else
            {
                loginError();
            }
        } 
        else
            loginError();
    }
    
    private void getUserGameInfo() throws SFSLoginException
    {
        database.getUserGameInfo(dataJson);
        if (dataJson.get(KJS.SUCESS).getAsBoolean())
            getFriends();
        else
            loginError();
    }

    private void signUp() throws SFSLoginException 
    {
        System.out.println("SingUp");
        database.registerUser(dataJson);
        if (dataJson.get(KJS.SUCESS).getAsBoolean()) 
            getFriends();
        else
            loginError();
    }

    private void getFriends() throws SFSLoginException 
    {
        System.out.println("Get Friends");
        JsonObject jsonObject= facebook.getFriends();
        if(jsonObject.get(KJS.SUCESS).getAsBoolean())
        {
            dataJson.add(KJS.FRIENDS,jsonObject);
            updateOnline();
        }
        else
            loginError();
    }
    
    private void updateOnline() throws SFSLoginException 
    {
        System.out.println("Update Online");
        JsonObject input = new JsonObject();
        String userId = dataJson.get(KJS.USER_ID).getAsString();
        boolean online = true;
        input.addProperty(KJS.USER_ID, userId);
        input.addProperty(KJS.ONLINE,online);
        JsonObject result = database.updateOnline(input);
        if (result.get(KJS.SUCESS).getAsBoolean()) 
            loginSucess();
        else
            loginError();
    }
    
    private void loginSucess()
    {
        System.out.println("Login Sucess");
        Session session = (Session)event.getParameter(SFSEventParam.SESSION);
        session.setProperty(KJS.DATA,dataJson);
    }
    
    private void loginError() throws SFSLoginException 
    {
        System.out.println("Login Error");
        trace(dataJson.get(KJS.EXCEPTION).getAsString() + "\n");
        SFSErrorData errData = new SFSErrorData(SFSErrorCode.LOGIN_BAD_USERNAME);
        throw new SFSLoginException("", errData);
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        String accessToken = this.data.getUtfString(KJS.ACCESS_TOKEN);
        String userName = (String)event.getParameter(SFSEventParam.LOGIN_NAME);
        if (accessToken == null) 
            return false;
        if (userName == null) 
            return false;
        return true;
    }
}

