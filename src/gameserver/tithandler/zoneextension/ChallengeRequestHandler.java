package gameserver.tithandler.zoneextension;

import gameserver.titsystem.titservice.challenge.ChallengeService;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class ChallengeRequestHandler extends TITRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        trace("Challenge Request\n");
        int playType = fromClientData.getInt(KJS.PLAY_TYPE);
        int playerCount = fromClientData.getInt(KJS.PLAYER_COUNT);
        int currentHandTime=ChallengeService.registerUser
                                                (user, playType, playerCount);
        user.initChallengeInfo();
        user.getChallengeUserInfo().setPlayType(playType);
        user.getChallengeUserInfo().setPlayerCount(playerCount);
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        toClientData.putInt(KJS.TIME,currentHandTime);
        send(CMDRP.CHALLENGE_RP, toClientData,user);
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        user.setStatus(TITUserStatus.WAITING_INQUEUE);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case GETED_PROFILE:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() {
        Integer playType =  fromClientData.getInt(KJS.PLAY_TYPE);
        Integer playerCount = fromClientData.getInt(KJS.PLAYER_COUNT);
        if (playType == null) 
        {
            return false;
        }
        if (playerCount == null) 
        {
            return false;
        }
        return true;
    }

}

