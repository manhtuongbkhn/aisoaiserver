package gameserver.tithandler.zoneextension;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.smartfoxserver.bitswarm.sessions.Session;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.user.TITFriend;
import gameserver.tithandler.TITEventHandler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titdatabase.TITDatabase;

public class JoinZoneEventHandler extends TITEventHandler 
{
    @Override
    protected void titHandleServerEvent() throws SFSException 
    {
        this.putInfoToUserVar();
    }

    private void putInfoToUserVar()
    {
        user.initUserProfile();
        user.initDefaultUserInfo();
        user.initGameUserInfo();
        Session session = user.getDefaultUserInfo().getSession();
        JsonObject jsonData = (JsonObject)session.getProperty(KJS.DATA);
        user.getUserProfile().setData(jsonData);  
        user.getGameUserInfo().setData(jsonData);
        user.initUserServingManager();
        setFriends(jsonData);
        
        /*
        user.setUserId("01");
        user.setFacebookId("018383382929");
        user.setFullName("Tuong Nguyen");
        user.setEmail("");
        user.setBirthDay("");
        user.setGender("");
        user.setFacebookId("");
        user.setUrlAvatar("");
        user.setRegisterDate("");
        user.initServingProcessManager();
                */
        updateSystemUserId();
        session.removeProperty(KJS.DATA);
        sendJoinZoneNotify();
    }

    private void updateSystemUserId() 
    {
        int systemUserId = user.getDefaultUserInfo().getSystemUserId();
        TITDatabase titDatabase = TITDatabase.getInstance();
        JsonObject input = new JsonObject();
        input.addProperty(KJS.USER_ID,user.getUserProfile().getUserId());
        input.addProperty(KJS.SYSTEM_USER_ID,user.getDefaultUserInfo().
                                                            getSystemUserId());
        titDatabase.updateSystemUserId(input);
    }

    private void setFriends(JsonObject dataJson) 
    {
        user.initFriendManager();
        TITDatabase database = TITDatabase.getInstance();
        JsonObject friendsJs = dataJson.get(KJS.FRIENDS).getAsJsonObject();
        JsonArray friendsJsArr = friendsJs.getAsJsonArray(KJS.ARRAY);
        for (int i = 0; i < friendsJsArr.size(); ++i) 
        {
            JsonObject friendJs = friendsJsArr.get(i).getAsJsonObject();
            database.getFriendInfoByFacebookId(friendJs);
            if(friendJs.get(KJS.SUCESS).getAsBoolean()&&
                                        friendJs.get(KJS.RESULT).getAsBoolean())
            {
                addFriend(friendJs);
            }
            else
            {
            }
        }
    }

    private void addFriend(JsonObject friendJs) 
    {
        TITFriend friend=new TITFriend(friendJs);
        if (friendJs.get(KJS.ONLINE).getAsBoolean()) 
        {
            int systemUserId = friendJs.get(KJS.SYSTEM_USER_ID).getAsInt();
            TITUser friendUser = getTITApi().
                                            getUserBySystemUserId(systemUserId);
            if(friendUser!=null)
            {
                String userId1=friendJs.get(KJS.USER_ID).getAsString();
                String userId2=friendUser.getUserProfile().getUserId();
                if(userId1.equals(userId2))//No problem
                {
                    friend.online(friendUser);
                    friendUser.getFriendManager().online(user);
                }
            }
        }
        user.getFriendManager().addFriend(friend);
    }

    protected void sendJoinZoneNotify() 
    {
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        send(CMDNF.USERJOINZONE_NF,toClientData,user);
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        user.setStatus(TITUserStatus.LOGGED);
    }

    @Override
    protected void changeUserStatusEnd() {
        user.setStatus(TITUserStatus.GETED_PROFILE);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        if (user.getStatus() == TITUserStatus.NULL) 
        {
            return true;
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }
}

