/*
 * Decompiled with CFR 0_110.
 */
package gameserver.tithandler.zoneextension;

import gameserver.tithandler.TITRequestHandler;

public class KeepConnectionRequestHandler
extends TITRequestHandler {
    @Override
    protected void titHandClientRequest() {
    }

    @Override
    protected void changeUserStatusBegin() {
    }

    @Override
    protected void changeUserStatusEnd() {
    }

    @Override
    protected boolean checkUserStatus() {
        return true;
    }

    @Override
    protected boolean checkRequestData() {
        return true;
    }
}

