package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class InvitationAnswerRequestHandler extends TITRequestHandler 
{
    private boolean requestSucess = false;

    @Override
    protected void titHandClientRequest() 
    {
        Boolean answer = fromClientData.getBool(KJS.RESULT);
        if(answer)
            checkRoomExist();
    }
    
    private void checkRoomExist()
    {
        int roomId = fromClientData.getInt(KJS.ROOM_ID);
        TITRoom room = zone.getRoomById(roomId);
        if(room==null)
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.ROOM_NOT_EXIST);
            send(CMDRP.INVITATIONANSWER_RP,toClientData,user);
        }
        else
            checkRoomStatus(room);
    }
    
    public void checkRoomStatus(TITRoom room)
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
                checkInvitation(room);
                break;
            default:
                SFSObject toClientData = new SFSObject();
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                            StrDefine.ROOM_STATUS_INVALIDATE);
                send(CMDRP.INVITATIONANSWER_RP, toClientData,user);
                break;
        }
    }
    
    private void checkInvitation(TITRoom room)
    {
        if(room.getInvitationManager().isUserWasInvited(user))
        {
            room.getInvitationManager().removeByWasInvitedUser(user);
            checkPlayerCount(room);
        }
        else
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                            StrDefine.SYSTEM_ERROR);
            send(CMDRP.INVITATIONANSWER_RP,toClientData,user);
        }
    }
    
    private void checkPlayerCount(TITRoom room)
    {
        if(room.getDefaultRoomInfo().getPlayerCount()<
                                    room.getDefaultRoomInfo().getPlayerMax())
        {
            joinRoom(room);
        }
        else
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                            StrDefine.ROOM_FULL);
            send(CMDRP.INVITATIONANSWER_RP,toClientData,user);
        }
    }
    
    private void joinRoom(TITRoom room)
    {
        getTITApi().joinRoom(user, room, room.getDefaultRoomInfo().getPass());
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        send(CMDRP.INVITATIONANSWER_RP, toClientData,user);
        requestSucess = true;
    }
    
    @Override
    protected void changeUserStatusBegin() {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        if (requestSucess) 
        {
            user.setStatus(TITUserStatus.WAITING_INROOM);
        }
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case GETED_PROFILE: 
                return true;
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() 
    {
        String inviteUserfullName = fromClientData.getUtfString(KJS.FULL_NAME);
        Integer roomId = fromClientData.getInt(KJS.ROOM_ID);
        String inviteTime = fromClientData.getUtfString(KJS.TIME);
        Boolean answer = fromClientData.getBool(KJS.RESULT);
        if (inviteUserfullName == null) 
        {
            return false;
        }
        if (roomId == null) 
        {
            return false;
        }
        if (inviteTime == null) 
        {
            return false;
        }
        if (answer == null) 
        {
            return false;
        }
        return true;
    }

}

