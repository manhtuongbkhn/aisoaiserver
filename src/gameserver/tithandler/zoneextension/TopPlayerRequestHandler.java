package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class TopPlayerRequestHandler extends TITRequestHandler
{
    @Override
    protected void titHandClientRequest() 
    {
        //System.out.println("-Top Player Request");
        SFSObject toClientData=new SFSObject();
        toClientData.putBool(KJS.SUCESS,true);
        user.getUserServingManager().getTopPlayerUserServing().onServing();
        send(CMDRP.TOP_PLAYER_RP,toClientData,user);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch(user.getStatus())
        {
            case GETED_PROFILE:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        
    }
    
}
