package gameserver.tithandler.zoneextension;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.TITFunction;

public class RoomListRequestHandler extends TITRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        //System.out.println("Room List Request");
        //JsonObject optionVar=TITFunction.covertToJsonObject(fromClientData);
        user.getUserServingManager().getRoomListUserServing().onServing();
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        send(CMDRP.ROOMLIST_RP,toClientData,user);
    }

    private void roomListResponse() 
    {
    }

    @Override
    public void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case GETED_PROFILE: 
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

