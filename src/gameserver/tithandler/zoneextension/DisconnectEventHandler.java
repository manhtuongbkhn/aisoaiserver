package gameserver.tithandler.zoneextension;

import gameserver.tithandler.TITEventHandler;

public class DisconnectEventHandler
extends TITEventHandler {
    @Override
    public void titHandleServerEvent() 
    {
        System.out.println(user.getSFSUser().isPlayer());
        System.out.println(user.getSFSUser().isSpectator());
        System.out.println(user.getSFSUser().getLastJoinedRoom());
        getTITApi().logout(this.user);
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        
    }

    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }
}

