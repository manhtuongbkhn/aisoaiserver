package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class BackRoomFinishRequestHandler extends TITRequestHandler 
{
    private boolean requestSucess=false;
    
    @Override
    protected void titHandClientRequest() 
    {
        if (user.getDefaultUserInfo().inRoom()) 
        {
            TITRoom room = user.getDefaultUserInfo().getRoom();
            if(room.getDefaultRoomInfo().getType()==1)
            {
                if (user.getDefaultUserInfo().isSpectator()) 
                {
                    boolean sucess = getTITApi().spectatorToPlayer(user,room);
                    if (sucess) 
                    {
                        SFSObject toClientData = new SFSObject();
                        toClientData.putBool(KJS.SUCESS, true);
                        send(CMDRP.BACKROOMFINISH_RP,toClientData,user);
                        requestSucess=true;
                    } 
                    else 
                    {
                        SFSObject toClientData = new SFSObject();
                        toClientData.putBool(KJS.SUCESS,false);
                        toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                        StrDefine.SYSTEM_ERROR);
                        send(CMDRP.BACKROOMFINISH_RP,toClientData,user);
                    }
                }
            }
        } 
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.USER_NOT_IN_ROOM);
            send(CMDRP.BACKROOMFINISH_RP,toClientData,user);
        }
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        if(requestSucess)
            user.setStatus(TITUserStatus.WAITING_INROOM);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case GETED_PROFILE: 
            case SCORING_TOTALPOINT: 
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

