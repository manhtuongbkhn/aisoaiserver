package gameserver.tithandler.zoneextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class CancelTopPlayerRequestHandler extends TITRequestHandler
{
    @Override
    protected void titHandClientRequest() 
    {
        //System.out.println("Cancel Top Player");
        user.getUserServingManager().getTopPlayerUserServing().offServing();
        SFSObject toClientData=new SFSObject();
        toClientData.putBool(KJS.SUCESS,true);
        send(CMDRP.CANCEL_TOP_PLAYER_RP,toClientData,user);
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch(user.getStatus())
        {
            case GETED_PROFILE:
                return true;
            default:
                return false;               
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        
    }
    
}
