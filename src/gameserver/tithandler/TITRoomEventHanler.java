package gameserver.tithandler;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.ISFSEventParam;
import com.smartfoxserver.v2.core.SFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.exceptions.SFSException;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.tithandler.TITEventHandler;

public abstract class TITRoomEventHanler extends TITEventHandler 
{
    protected TITRoom room;

    @Override
    public void handleServerEvent(ISFSEvent event) throws SFSException 
    {
        init((SFSEvent)event);
        if (check()) 
        {
            changeUserStatusBegin();
            changeRoomStatusBegin();
            titHandleServerEvent();
            changeRoomStatusEnd();
            changeUserStatusEnd();
        } 
        else 
        {
            eventFalse();
        }
    }

    @Override
    protected void init(SFSEvent event) 
    {
        super.init(event);
        SFSRoom sfsRoom = (SFSRoom)event.getParameter((ISFSEventParam)SFSEventParam.ROOM);
        room = new TITRoom(sfsRoom);
    }

    @Override
    public boolean check() 
    {
        if (checkUserStatus()) 
        {
            if (checkRoomStatus()) 
            {
                return checkRequestData();
            }
            return false;
        }
        return false;
    }
    
    protected abstract boolean checkRoomStatus();

    protected abstract void changeRoomStatusBegin();

    protected abstract void changeRoomStatusEnd();
}

