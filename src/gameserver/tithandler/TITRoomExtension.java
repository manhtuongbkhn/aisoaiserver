/*
 * Decompiled with CFR 0_110.
 */
package gameserver.tithandler;

import gameserver.tithandler.TITExtension;

public abstract class TITRoomExtension
extends TITExtension {
    public void init() {
        this.initEventHandler();
        this.initRequestHandler();
    }

    protected void initEventHandler() {
    }

    protected void initRequestHandler() {
    }
}

