package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class AllPlayerInfoRequestHandler extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        SFSArray sfsArray = room.getDefaultRoomInfo().getAllUserPublicInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,sfsArray);
        send(CMDNF.ALLPLAYERINFO_NF, toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case WAITING_INROOM: 
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING: 
            case REFESHING:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

