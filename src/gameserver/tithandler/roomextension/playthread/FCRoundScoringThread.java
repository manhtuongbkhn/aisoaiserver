package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceRoundContext;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceUserRoundContext;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class FCRoundScoringThread extends TITRoundScoringThread
{
    public FCRoundScoringThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void calculateConvertPoint()
    {
        ForChoiceRoundContext roundContext=
                    room.getForChoiceGameContext().getCurrentRoundContext();
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for (int i = 1; i <= userArr.size(); i++) 
        {
            TITUser titUser = userArr.get(i-1);
            int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
            ForChoiceUserRoundContext userRoundGameContext=
                                roundContext.getUserRoundContext(roomUserId);
            int maxPoint=userRoundGameContext.getMaxPoint();
            int roundPoint = userRoundGameContext.getPoint();
            int convertPoint=roundPoint*1000/maxPoint;
            if(convertPoint>1000)
                convertPoint=1000;
            else
            {
                if(convertPoint<0)
                    convertPoint=0;
            }
            userRoundGameContext.setConvertPoint(convertPoint);
        }
    }
    
    @Override
    protected void sendRoundScoringInfo() 
    {
        Integer round = room.getGameContext().getCurrentRound();
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        ForChoiceRoundContext roundContext=room.
                            getForChoiceGameContext().getCurrentRoundContext();
        for(int i=0;i<userArr.size();i++)
        {
            TITUser user=userArr.get(i);
            int roomUserId=user.getDefaultUserInfo().getRoomUserId();
            ForChoiceUserRoundContext userRoundContext=roundContext.
                                                getUserRoundContext(roomUserId);
            TITGameInfo gameInfo=userRoundContext.getGameInfo();
            SFSObject toClientData = new SFSObject();
            toClientData.putInt(KJS.ROOM_ROUND,round);
            toClientData.putInt(KJS.GAME_ID, gameInfo.getGameId());
            toClientData.putUtfString(KJS.GAME_NAME,gameInfo.getGameName());
            toClientData.putInt(KJS.HARD_LEVEL,roundContext.getHardLevel());
            
            send(CMDNF.ROUNDSCORINGINFO_NF,toClientData,user);
        }
    }
    
    @Override
    public void nextThread()
    {
        int round=room.getGameContext().getCurrentRound();
        int roundCount=room.getGameContext().getRoundCount();
        if (round<roundCount) 
        {
            FCChoiceGameThread choiceGameThread = 
                                            new FCChoiceGameThread(roomService);
            choiceGameThread.start();
        } 
        else 
        {
            FCTotalScoringThread totalScoringThread = 
                                        new FCTotalScoringThread(roomService);
            totalScoringThread.run();
        } 
    }
}
