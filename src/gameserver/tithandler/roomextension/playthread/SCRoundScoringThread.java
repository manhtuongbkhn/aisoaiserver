package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titsystem.titentities.room.gamecontext.SameChoiceRoundContext;
import gameserver.titsystem.titentities.room.gamecontext.TITUserRoundGameContext;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class SCRoundScoringThread extends TITRoundScoringThread
{
    public SCRoundScoringThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void calculateConvertPoint()
    {
        SameChoiceRoundContext roundContext=room.getSameChoiceGameContext().
                                                    getCurrentRoundContext();
        int maxPoint=roundContext.getMaxPoint();
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for (int i = 1; i <= userArr.size(); i++) 
        {
            TITUser titUser = userArr.get(i-1);
            int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
            TITUserRoundGameContext userRoundGameContext=
                            roundContext.getUserRoundGameContext(roomUserId);
            int roundPoint = userRoundGameContext.getPoint();
            int convertPoint=roundPoint*1000/maxPoint;
            if(convertPoint>1000)
                convertPoint=1000;
            else
            {
                if(convertPoint<0)
                    convertPoint=0;
            }
            userRoundGameContext.setConvertPoint(convertPoint);
        }
    }
    
    @Override
    protected void sendRoundScoringInfo() 
    {
        Integer round = room.getSameChoiceGameContext().getCurrentRound();
        TITGameInfo gameInfo=room.getSameChoiceGameContext().
                                        getCurrentRoundContext().getGameInfo();
        int gameId = gameInfo.getGameId();
        String gameName = gameInfo.getGameName();
        int hardLevel = room.getSameChoiceGameContext().
                                    getCurrentRoundContext().getHardLevel();
        SFSObject toClientData = new SFSObject();
        toClientData.putInt(KJS.ROOM_ROUND,round);
        toClientData.putInt(KJS.GAME_ID, gameId);
        toClientData.putUtfString(KJS.GAME_NAME, gameName);
        toClientData.putInt(KJS.HARD_LEVEL, hardLevel);
        send(CMDNF.ROUNDSCORINGINFO_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }
    
    @Override
    public void nextThread()
    {
        int round=room.getGameContext().getCurrentRound();
        int roundCount=room.getGameContext().getRoundCount();
        if (round<roundCount) 
        {
            SCChoiceGameThread choiceGameThread = 
                                            new SCChoiceGameThread(roomService);
            choiceGameThread.start();
        } 
        else 
        {
            SCTotalScoringThread totalScoringThread = 
                                        new SCTotalScoringThread(roomService);
            totalScoringThread.run();
        } 
    }
}
