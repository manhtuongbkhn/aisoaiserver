package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titconfig.TITFilter;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceUserRoundContext;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;
import gameserver.titgameinfo.TITGameInfoFactory;

public class FCChoiceGameThread extends TITChoiceGameThread
{
    public FCChoiceGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void sendChoiceGameInfo() 
    {
        int round =room.getGameContext().getCurrentRound();
        int roundCount=room.getGameContext().getRoundCount();
        SFSObject toClientData = new SFSObject();
        if (round < roundCount) 
        {
            ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
            for(int i=0;i<userArr.size();i++)
            {
                TITUser choiceGameUser=userArr.get(i);
                //ChoiceGameUser
                int choiceGameUserSystemUserId=choiceGameUser.
                                        getDefaultUserInfo().getSystemUserId();
                String choiceGameUserUserId=choiceGameUser.
                                                getUserProfile().getUserId();
                String choiceGameUserFullName=choiceGameUser.
                                            getUserProfile().getFullName();
                String choiceGameUserAvatarUrl=choiceGameUser.
                                            getUserProfile().getAvatarUrl();
                //
                int choiceGameRoomUserId=choiceGameUser.
                                        getDefaultUserInfo().getRoomUserId();
                int wasChoicedGameUserRoomUserId=room.getForChoiceGameContext().
                    getCurrentRoundContext().getWasChoiceGameRoomUserId
                                                        (choiceGameRoomUserId);
                TITUser wasChoicedGameUser=room.getDefaultRoomInfo().
                            getUserByRoomUserId(wasChoicedGameUserRoomUserId);
                //WasChoicedGameUser
                int wasChoicedGameUserSystemUserId=wasChoicedGameUser.
                                        getDefaultUserInfo().getSystemUserId();
                String wasChoicedGameUserUserId=
                                wasChoicedGameUser.getUserProfile().getUserId();
                String wasChoicedGameUserFullName=
                            wasChoicedGameUser.getUserProfile().getFullName();
                String wasChoicedGameUserAvatarUrl=
                            wasChoicedGameUser.getUserProfile().getAvatarUrl();
                //
                ArrayList<Integer>gameIdArr=choiceGameUser.
                                            getGameUserInfo().getGameIdArr();
                int roomUserid=wasChoicedGameUser.
                                        getDefaultUserInfo().getRoomUserId();
                ArrayList<Integer>choicedGameIdArr=room.
                    getForChoiceGameContext().getChoicedGameIdArr(roomUserid);
                TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
                SFSArray gameInfoSFSArr = TITGameInfoFactory.
                                    covertGameIdArrToGameInfoSFSArr(gameIdArr);
                
                SFSObject choiceUserSFSObject=new SFSObject();
                SFSObject wasChoicedUserSFSObject=new SFSObject();
            
                choiceUserSFSObject.putInt
                                (KJS.SYSTEM_USER_ID,choiceGameUserSystemUserId);
                choiceUserSFSObject.putUtfString
                                        (KJS.FULL_NAME,choiceGameUserFullName);
                choiceUserSFSObject.putUtfString
                                    (KJS.AVATAR_URL,choiceGameUserAvatarUrl);
                choiceUserSFSObject.putUtfString
                                            (KJS.USER_ID,choiceGameUserUserId);
            
                wasChoicedUserSFSObject.putInt
                        (KJS.SYSTEM_USER_ID,wasChoicedGameUserSystemUserId);
                wasChoicedUserSFSObject.putUtfString
                                        (KJS.USER_ID,wasChoicedGameUserUserId);
                wasChoicedUserSFSObject.putUtfString
                                    (KJS.FULL_NAME,wasChoicedGameUserFullName);
                wasChoicedUserSFSObject.putUtfString
                                (KJS.AVATAR_URL,wasChoicedGameUserAvatarUrl);
                toClientData.putSFSObject(KJS.USER1,choiceUserSFSObject);
                toClientData.putSFSObject(KJS.USER2,wasChoicedUserSFSObject);
                
                String notification=choiceGameUserFullName+" chon game cho "+
                                                    wasChoicedGameUserFullName;
                
                toClientData.putInt(KJS.ROOM_ROUND,round);
                toClientData.putUtfString(KJS.NOTIFICATION,notification);
                toClientData.putSFSArray(KJS.ARRAY,gameInfoSFSArr);
                send(CMDNF.CHOICEGAMEINFO_NF,toClientData,choiceGameUser);
            }
        }
        
        if (round == roundCount) 
        {
            ArrayList<Integer> allGameIdArr=TITGameInfoFactory.getAllGameIdArr();
            SFSArray allGameInfoSFSArr = TITGameInfoFactory.
                                covertGameIdArrToGameInfoSFSArr(allGameIdArr);
            
            SFSObject choiceUserSFSObject=new SFSObject();
            SFSObject wasChoicedUserSFSObject=new SFSObject();
            
            choiceUserSFSObject.putInt(KJS.SYSTEM_USER_ID,-1);
            choiceUserSFSObject.putUtfString(KJS.USER_ID,"0000000a");
            
            wasChoicedUserSFSObject.putInt(KJS.SYSTEM_USER_ID,-2);
            wasChoicedUserSFSObject.putUtfString(KJS.USER_ID,"0000000b");
            
            toClientData.putSFSObject(KJS.USER1,choiceUserSFSObject);
            toClientData.putSFSObject(KJS.USER2,wasChoicedUserSFSObject);
            
            toClientData.putUtfString(KJS.NOTIFICATION,
                                            "He thong chon game cho moi nguoi!");
            toClientData.putInt(KJS.ROOM_ROUND, round);
            toClientData.putSFSArray(KJS.ARRAY,allGameInfoSFSArr);
            send(CMDNF.CHOICEGAMEINFO_NF, toClientData,room.getDefaultRoomInfo().
                                                                getPlayerArr());
        }
    }

    @Override
    protected void sendChoiceGameDownTime() 
    {
        int downTime=ServerConfig.CHOICEGAME_DOWNTIME;
        int delayMillis=ServerConfig.CHECK_CHOICED_GAME_DELAY_MILLIS;
        int secondCount=1000/delayMillis;
        int downCount=downTime*secondCount;
        for (int i=downCount;i>=0&&!room.getForChoiceGameContext().
                            getCurrentRoundContext().isAllUserChoicedGame();i--) 
        {
            SFSObject dataToClient = new SFSObject();
            if(i%secondCount==0)
            {
                int time=i/secondCount;
                dataToClient.putInt(KJS.DOWN_TIME,time);
            }
            sleep(delayMillis);
            send(CMDNF.CHOICEGAMEDOWNTIME_NF, dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }

    @Override
    protected void checkUserChoicedGame() 
    {
        int round=room.getGameContext().getCurrentRound();
        int rounCount=room.getGameContext().getRoundCount();
        if (round<rounCount) 
        {
            ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
            for(int i=0;i<userArr.size();i++)
            {
                TITUser choiceGameUser=userArr.get(i);
                int choiceGameRoomUserId=choiceGameUser.
                                        getDefaultUserInfo().getRoomUserId();
                int wasChoicedGameUserRoomUserId=room.getForChoiceGameContext().
                    getCurrentRoundContext().getWasChoiceGameRoomUserId
                                                        (choiceGameRoomUserId);
                ForChoiceUserRoundContext userRoundContext=room.
                    getForChoiceGameContext().getCurrentRoundContext().
                             getUserRoundContext(wasChoicedGameUserRoomUserId);
                if(!userRoundContext.isChoicedGame())
                {
                    ArrayList<Integer> gameIdArr = choiceGameUser.
                                            getGameUserInfo().getGameIdArr();
                    ArrayList<Integer> choicedGameIdArr=room.
                        getForChoiceGameContext().
                        getChoicedGameIdArr(wasChoicedGameUserRoomUserId);
                    TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
                    int size = gameIdArr.size();
                    int radom=TITFunction.randInt(size);
                    int radomGameId = gameIdArr.get(radom-1);
                    TITGameInfo gameInfo = TITGameInfoFactory.
                                        covertGameIdToGameInfo(radomGameId);
                    userRoundContext.setGameInfo(gameInfo);
                }
                room.getGameContext().getCurrentRoundContext().
                                        setStatus(TITRoundStatus.PLAYING_GAME);
            }
        } 
        else 
        {
            ArrayList<Integer> gameIdArr = TITGameInfoFactory.getAllGameIdArr();
            //TITFilter
            int size = gameIdArr.size();
            int radom=TITFunction.randInt(size);
            int radomGameId = gameIdArr.get(radom-1);
            TITGameInfo gameInfo = TITGameInfoFactory.
                                        covertGameIdToGameInfo(radomGameId);
            room.getForChoiceGameContext().setLastRoundGameInfo(gameInfo);
            room.getGameContext().getCurrentRoundContext().
                                        setStatus(TITRoundStatus.PLAYING_GAME);
        }
    }
    
    @Override
    protected void sendChoicedGameInfo() 
    {
        int round=room.getGameContext().getCurrentRound();
        int roundCount=room.getGameContext().getRoundCount();
        
        if(round<roundCount)
        {
            ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
            for(int i=0;i<userArr.size();i++)
            {
                TITUser choiceGameUser=userArr.get(i);
                String choiceGameUserFullName=
                                choiceGameUser.getUserProfile().getFullName();
                int choiceGameRoomUserId=choiceGameUser.
                                        getDefaultUserInfo().getRoomUserId();
                int wasChoicedGameUserRoomUserId=room.getForChoiceGameContext().
                    getCurrentRoundContext().getWasChoiceGameRoomUserId
                                                        (choiceGameRoomUserId);
                TITUser wasChoicedGameUser=room.getDefaultRoomInfo().
                            getUserByRoomUserId(wasChoicedGameUserRoomUserId);
                
                ForChoiceUserRoundContext choiceGameUserRoundContext=room.
                    getForChoiceGameContext().getCurrentRoundContext().
                                    getUserRoundContext(choiceGameRoomUserId);
                
                ForChoiceUserRoundContext wasChoicedGameUserRoundContext=room.
                    getForChoiceGameContext().getCurrentRoundContext().
                            getUserRoundContext(wasChoicedGameUserRoomUserId);
                
                SFSObject toClientData=new SFSObject();
                TITGameInfo gameInfo=wasChoicedGameUserRoundContext.getGameInfo();
                String wasChoicedGameFullName=wasChoicedGameUser.
                                                getUserProfile().getFullName();
                String notification=choiceGameUserFullName+" da chon game "+
                        gameInfo.getGameName()+" cho "+wasChoicedGameFullName;
                toClientData.putUtfString(KJS.NOTIFICATION,notification);
                toClientData.putInt(KJS.GAME_ID,gameInfo.getGameId());
                toClientData.putUtfString(KJS.GAME_NAME,gameInfo.getGameName());
                
                send(CMDNF.CHOICEDGAMEINFO_NF,toClientData,choiceGameUser);
            }
        }
        
        if(round==roundCount)
        {         
            SFSObject toClientData=new SFSObject();
            TITGameInfo gameInfo=room.getForChoiceGameContext().
                                                        getLastRoundGameInfo();
            toClientData.putUtfString(KJS.NOTIFICATION,"He thong da chon game "
                                                    +gameInfo.getGameName());
            toClientData.putInt(KJS.GAME_ID, gameInfo.getGameId());
            toClientData.putUtfString(KJS.GAME_NAME,gameInfo.getGameName());
            send(CMDNF.CHOICEDGAMEINFO_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }
    
    @Override
    protected void nextThread()
    {
        FCGuideGameThread guideGameThread = new FCGuideGameThread(roomService);
        guideGameThread.run();
    }
}
