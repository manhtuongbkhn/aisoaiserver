package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITChoiceGameThread extends TITRoomThread
{
    public TITChoiceGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    public void titRun() 
    {
        send(CMDNF.STARTCHOICEGAME_NF,new SFSObject(),
                                    room.getDefaultRoomInfo().getPlayerArr());
        int round=room.getGameContext().getCurrentRound();
        room.getGameContext().setCurrentRound(round+1);
        sleep(ServerConfig.VIEWDELAY_MILLIS);
        room.getGameContext().getCurrentRoundContext().
                                        setStatus(TITRoundStatus.CHOICING_GAME);
        sendChoiceGameInfo();
        sendChoiceGameUserInfo();
        sendChoiceGameDownTime();
        checkUserChoicedGame();
        sendChoicedGameInfo();
        sleep(ServerConfig.WAITUSERVIEW_MILLIS);
        nextThread();
    }
    
    protected void sendChoiceGameInfo()
    {
    }
    
    protected void sendChoiceGameUserInfo()
    {
        ArrayList<TITUser> userArr =room.getDefaultRoomInfo().getPlayerArr();
        SFSArray sfsArray = new SFSArray();
        for (int i = 0; i < userArr.size(); ++i) 
        {
            TITUser titUser = userArr.get(i);
            SFSObject sfsObject = titUser.getPublicInfo();
            int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
            int totalPoint = room.getGameContext().getTotalPoint(roomUserId);
            sfsObject.putInt(KJS.POINT, totalPoint);
            sfsArray.addSFSObject(sfsObject);
        }
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,sfsArray);
        send(CMDNF.CHOICEGAMEUSERINFO_NF,toClientData, userArr);
    }
    
    protected void sendChoiceGameDownTime()
    {
        
    }
    
    protected void checkUserChoicedGame()
    {
        
    }
    
    protected void sendChoicedGameInfo()
    {
        
    }
    
    protected void nextThread()
    {
        
    }
}
