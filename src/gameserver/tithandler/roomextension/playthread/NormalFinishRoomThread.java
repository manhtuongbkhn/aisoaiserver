package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.TITRoomStatus;
import gameserver.titconfig.CMDNF;

public class NormalFinishRoomThread extends TITFinishRoomThread
{
    public NormalFinishRoomThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    public void titRun()
    {
        kickSpectator();
        if (room.getDefaultRoomInfo().getPlayerCount() == 0) 
        {
            getTITApi().removeRoom(room);
        } 
        else 
        {
            room.getDefaultRoomInfo().setStatus(TITRoomStatus.WAITING);
        }
    }
    
    protected void kickSpectator() 
    {
        ArrayList<TITUser> userArr = room.getDefaultRoomInfo().getSpectatorArr();
        for (int i = 0; i < userArr.size(); ++i) 
        {
            TITUser titUser = userArr.get(i);
            getTITApi().exitRoom(titUser,room);
            send(CMDNF.WASKICKROOMBYSYSTEM_NF,new SFSObject(),titUser);
        }
    }
}
