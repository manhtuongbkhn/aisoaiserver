package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.TITRoomStatus;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITTotalScoringThread extends TITRoomThread 
{
    public TITTotalScoringThread(ITITRoomService roomService) 
    {
        super(roomService);
    }

    @Override
    public void titRun() 
    {
        room.getDefaultRoomInfo().setStatus(TITRoomStatus.SCORING_TOTALPOINT);
        send(CMDNF.STARTTOTALSCORING_NF, new SFSObject(),
                                    room.getDefaultRoomInfo().getPlayerArr());
        sleep(ServerConfig.VIEWDELAY_MILLIS);
        sendTotalScoringRoomInfo();
        sendTotalScoringUserInfo();
        room.getDefaultRoomInfo().setStatus(TITRoomStatus.REFESHING);
        room.getDefaultRoomInfo().clearBackupUserArr();
        setNewUserStatus();
        changeAllPlayerToSpectator();
        
        sendTotalScoringDownTime();
        nextThread();
    }

    protected void changeAllPlayerToSpectator() 
    {
        ArrayList<TITUser> userArr = room.getDefaultRoomInfo().getPlayerArr();
        for (int i = 0; i < userArr.size(); i++) 
        {
            TITUser titUser = userArr.get(i);
            getTITApi().playerToSpectator(titUser,room);
        }
    }

    protected void setNewUserStatus() 
    {
        for (int i = 0; i < room.getDefaultRoomInfo().getPlayerArr().size();i++) 
        {
            TITUser titUser = room.getDefaultRoomInfo().getPlayerArr().get(i);
            titUser.setStatus(TITUserStatus.SCORING_TOTALPOINT);
        }
    }

    protected void sendTotalScoringRoomInfo()
    {
        SFSObject toClientData=new SFSObject();
        room.getDefaultRoomInfo().getPublicInfo(toClientData);
        send(CMDNF.ROOMINFO_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }
    
    protected void sendTotalScoringUserInfo() 
    {
        ArrayList<TITUser> userArr=TITFunction.sortUserTotalPoint(room);
        SFSArray sfsArray = new SFSArray();
        int oldRank = 0;
        int oldPoint = 10000;
        for (int i = 1; i <= userArr.size(); i++) 
        {
            TITUser titUser = userArr.get(i - 1);
            SFSObject sfsObject = titUser.getPublicInfo();
            int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
            int totalPoint = room.getGameContext().getTotalPoint(roomUserId);
            ArrayList<Integer> roundPointArr=room.getGameContext().
                                                getRoundPointArr(roomUserId);
            sfsObject.putInt(KJS.TOTAl_POINT,totalPoint);
            sfsObject.putIntArray(KJS.POINT_ARR,roundPointArr);
            if (totalPoint == oldPoint) 
            {
                sfsObject.putInt(KJS.RANK,oldRank);
            }
            if (totalPoint < oldPoint) 
            {
                sfsObject.putInt(KJS.RANK, i);
                oldRank = i;
            }
            oldPoint = totalPoint;
            sfsArray.addSFSObject(sfsObject);
        }
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,sfsArray);
        send(CMDNF.TOTALSCORINGUSERINFO_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }

    protected void sendTotalScoringDownTime() 
    {
        int downTime=ServerConfig.TOTAL_SCORING_DOWNTIME;
        for (int i = downTime; i >= 0; i--) 
        {
            SFSObject dataToClient = new SFSObject();
            dataToClient.putInt(KJS.DOWN_TIME, i);
            sleep(1000);
            send(CMDNF.TOTALSCORINGDOWNTIME_NF,dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }
    
    protected void nextThread()
    {
        TITFinishRoomThread finishRoomThread;
        switch(room.getDefaultRoomInfo().getType())
        {
            case 1:
                finishRoomThread=new NormalFinishRoomThread(roomService);
                break;
            case 2:
            default:
                finishRoomThread=new ChallengeFinishRoomThread(roomService);
                break;
        }
        finishRoomThread.run();
    }
}

