package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.room.TITRoomStatus;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class ChallengeBeginRoomThread extends TITBeginRoomThread
{
    public ChallengeBeginRoomThread(ITITRoomService roomService) 
    {
        super(roomService);
    }

    @Override
    public void titRun() 
    {
        room.getDefaultRoomInfo().setStatus(TITRoomStatus.WAITING);
        changeUserStatus();
        room.initMessageManager();
        room.initInvitationManager();
        sleep(800);
        sendBeginGameDownTime();
        TITStartGameThread startGameThread = new TITStartGameThread(this.roomService);
        startGameThread.run();
    }

    public void sendBeginGameDownTime() 
    {
        int downTime=ServerConfig.BEGINGAME_DOWNTIME;
        for (int i = downTime; i >= 0; i -= 2) 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putInt(KJS.DOWN_TIME, i);
            this.send(CMDNF.BEGINGAMEDOWNTIME_NF, toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
            this.sleep(2000);
        }
    }
}

