package gameserver.tithandler.roomextension.playthread;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titconfig.TITFilter;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titsystem.titentities.room.gamecontext.SameChoiceRoundContext;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;
import gameserver.titgameinfo.TITGameInfoFactory;

public class SCChoiceGameThread extends TITChoiceGameThread
{
    public SCChoiceGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void sendChoiceGameInfo() 
    {
        int round =room.getGameContext().getCurrentRound();
        int roundCount=room.getGameContext().getRoundCount();
        SFSObject toClientData = new SFSObject();
        if (round < roundCount) 
        {
            TITUser choiceGameUser=room.getDefaultRoomInfo().
                                                    getPlayerArr().get(round-1);
            int systemUserId = choiceGameUser.getDefaultUserInfo().
                                                            getSystemUserId();
            String fullName = choiceGameUser.getUserProfile().getFullName();
            String avatarUrl = choiceGameUser.getUserProfile().getAvatarUrl();
            String userId=choiceGameUser.getUserProfile().getUserId();
            
            ArrayList<Integer> gameIdArr = choiceGameUser.getGameUserInfo()
                                                                .getGameIdArr();
            ArrayList<Integer> choicedGameIdArr=room.getSameChoiceGameContext().
                                                        getChoicedGameIdArr();
            TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
            SFSArray gameInfoSFSArr = TITGameInfoFactory.
                                    covertGameIdArrToGameInfoSFSArr(gameIdArr);
            
            SFSObject choiceUserSFSObject=new SFSObject();
            SFSObject wasChoicedUserSFSObject=new SFSObject();
            
            choiceUserSFSObject.putInt(KJS.SYSTEM_USER_ID,systemUserId);
            choiceUserSFSObject.putUtfString(KJS.FULL_NAME,fullName);
            choiceUserSFSObject.putUtfString(KJS.AVATAR_URL, avatarUrl);
            choiceUserSFSObject.putUtfString(KJS.USER_ID,userId);
            
            wasChoicedUserSFSObject.putInt(KJS.SYSTEM_USER_ID,-2);
            wasChoicedUserSFSObject.putUtfString(KJS.USER_ID,"00000000b");
            
            toClientData.putSFSObject(KJS.USER1,choiceUserSFSObject);
            toClientData.putSFSObject(KJS.USER2,wasChoicedUserSFSObject);
            toClientData.putUtfString(KJS.NOTIFICATION,
                                        fullName+" chon game cho moi nguoi!");
            toClientData.putInt(KJS.ROOM_ROUND, round);
            toClientData.putSFSArray(KJS.ARRAY,gameInfoSFSArr);
        }
        
        if (round == roundCount) 
        {
            ArrayList<Integer> allGameIdArr=TITGameInfoFactory.getAllGameIdArr();
            ArrayList<Integer> choicedGameIdArr=room.getSameChoiceGameContext().
                                                    getChoicedGameIdArr();
            TITFilter.gameIdFilter(allGameIdArr,choicedGameIdArr);
            SFSArray allGameInfoSFSArr = TITGameInfoFactory.
                                covertGameIdArrToGameInfoSFSArr(allGameIdArr);
            
            SFSObject choiceUserSFSObject=new SFSObject();
            SFSObject wasChoicedUserSFSObject=new SFSObject();
            
            choiceUserSFSObject.putInt(KJS.SYSTEM_USER_ID,-1);
            choiceUserSFSObject.putUtfString(KJS.USER_ID,"0000000a");
            
            wasChoicedUserSFSObject.putInt(KJS.SYSTEM_USER_ID,-2);
            wasChoicedUserSFSObject.putUtfString(KJS.USER_ID,"0000000b");
            
            toClientData.putSFSObject(KJS.USER1,choiceUserSFSObject);
            toClientData.putSFSObject(KJS.USER2,wasChoicedUserSFSObject);
            
            toClientData.putUtfString(KJS.NOTIFICATION,
                                        "He thong chon game cho moi nguoi!");
            toClientData.putInt(KJS.ROOM_ROUND, round);
            toClientData.putSFSArray(KJS.ARRAY,allGameInfoSFSArr);
        }
        send(CMDNF.CHOICEGAMEINFO_NF, toClientData,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }

    @Override
    protected void sendChoiceGameDownTime() 
    {
        int downTime=ServerConfig.CHOICEGAME_DOWNTIME;
        int delayMillis=ServerConfig.CHECK_CHOICED_GAME_DELAY_MILLIS;
        int secondCount=1000/delayMillis;
        int downCount=downTime*secondCount;
        for (int i=downCount;i>=0&&!room.getSameChoiceGameContext().
                                getCurrentRoundContext().isChoicedGame();i--) 
        {
            SFSObject dataToClient = new SFSObject();
            if(i%secondCount==0)
            {
                int time=i/secondCount;
                dataToClient.putInt(KJS.DOWN_TIME,time);
            }
            sleep(delayMillis);
            send(CMDNF.CHOICEGAMEDOWNTIME_NF, dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }

    @Override
    protected void checkUserChoicedGame() 
    {
        int round=room.getSameChoiceGameContext().getCurrentRound();
        int rounCount=room.getSameChoiceGameContext().getRoundCount();
        if (round<rounCount) 
        {
            if (!room.getSameChoiceGameContext().getCurrentRoundContext().
                                                                isChoicedGame()) 
            {
                TITUser choiceGameUser=room.getDefaultRoomInfo().
                                                    getPlayerArr().get(round-1);
 
                ArrayList<Integer> gameIdArr = choiceGameUser.getGameUserInfo().
                                                                getGameIdArr();
                ArrayList<Integer> choicedGameIdArr=room.
                            getSameChoiceGameContext().getChoicedGameIdArr();
                TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
                int size = gameIdArr.size();
                int radom=TITFunction.randInt(size);
                int radomGameId = gameIdArr.get(radom-1);
                TITGameInfo gameInfo = TITGameInfoFactory.
                                        covertGameIdToGameInfo(radomGameId);
                room.getSameChoiceGameContext().getCurrentRoundContext().
                                                        setGameInfo(gameInfo);
            }
        } 
        else 
        {
            ArrayList<Integer> gameIdArr = TITGameInfoFactory.getAllGameIdArr();
            ArrayList<Integer> choicedGameIdArr=room.getSameChoiceGameContext().
                                                        getChoicedGameIdArr();
            TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
            int size = gameIdArr.size();
            int radom=TITFunction.randInt(size);
            int radomGameId = gameIdArr.get(radom-1);
            TITGameInfo gameInfo = TITGameInfoFactory.
                                        covertGameIdToGameInfo(radomGameId);
            room.getSameChoiceGameContext().getCurrentRoundContext().
                                                        setGameInfo(gameInfo);
        }
        room.getSameChoiceGameContext().getCurrentRoundContext().
                                        setStatus(TITRoundStatus.PLAYING_GAME);
    }
    
    @Override
    protected void sendChoicedGameInfo() 
    {
        SFSObject dataToClient = new SFSObject();
        SameChoiceRoundContext roundContext = room.getSameChoiceGameContext().
                                                    getCurrentRoundContext();
        TITGameInfo gameInfo = roundContext.getGameInfo();
        String gameName = gameInfo.getGameName();
        Integer gameId = gameInfo.getGameId();
        int round=room.getSameChoiceGameContext().getCurrentRound();
        int roundCount=room.getSameChoiceGameContext().getRoundCount();
        if (round< roundCount) 
        {
            TITUser choiceGameUser=room.getDefaultRoomInfo().
                                                    getPlayerArr().get(round-1);
            String fullName = choiceGameUser.getUserProfile().getFullName();
            dataToClient.putUtfString(KJS.NOTIFICATION,
                                        fullName + " da chon game " + gameName);
            dataToClient.putInt(KJS.GAME_ID, gameInfo.getGameId());
            dataToClient.putUtfString(KJS.GAME_NAME,gameInfo.getGameName());
        }
        
        if (round==roundCount) 
        {
            dataToClient.putUtfString(KJS.NOTIFICATION,
                                        "He thong da chon game " + gameName);
            dataToClient.putInt(KJS.GAME_ID, gameInfo.getGameId());
            dataToClient.putUtfString(KJS.GAME_NAME,gameInfo.getGameName());
        }
        send(CMDNF.CHOICEDGAMEINFO_NF, dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }
    
    @Override
    protected void nextThread()
    {
        SCGuideGameThread guideGameThread = new SCGuideGameThread(roomService);
        guideGameThread.run();
    }
}

