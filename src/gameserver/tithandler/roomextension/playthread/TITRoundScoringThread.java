package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.TITUserRoundGameContext;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundContext;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITRoundScoringThread extends TITRoomThread 
{
    public TITRoundScoringThread(ITITRoomService roomService) 
    {
        super(roomService);
    }

    @Override
    public void titRun() 
    {
        room.getGameContext().getCurrentRoundContext().
                                        setStatus(TITRoundStatus.SCORING_POINT);
        send(CMDNF.STARTROUNDSCORING_NF, new SFSObject(),
                                    room.getDefaultRoomInfo().getPlayerArr());
        sleep(800);
        calculateConvertPoint();
        sendRoundScoringInfo();
        sendRoundScoringUserInfo();
        sendRoundScoringDownTime();
        nextThread();
    }

    protected void calculateConvertPoint()
    {
        
    }
    
    protected void sendRoundScoringInfo() 
    {
    }

    
    protected void sendRoundScoringUserInfo() 
    {
        ArrayList<TITUser> userArr=TITFunction.sortUserRoundPoint(room);
        SFSArray sfsArray = new SFSArray();
        int oldRank = 0;
        int oldPoint = 10000;
        TITRoundContext roundContext=room.getGameContext().
                                                    getCurrentRoundContext();
        for (int i = 1; i <= userArr.size(); i++) 
        {
            TITUser titUser = userArr.get(i - 1);
            SFSObject sfsObject = titUser.getPublicInfo();
            int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
            TITUserRoundGameContext userRoundGameContext=
                            roundContext.getUserRoundGameContext(roomUserId);
            int roundPoint = userRoundGameContext.getPoint();
            int convertPoint=userRoundGameContext.getConvertPoint();
            int trueCount = userRoundGameContext.getTrueCount();
            int falseCount = userRoundGameContext.getFalseCount();
            sfsObject.putInt(KJS.POINT, roundPoint);
            sfsObject.putInt(KJS.CONVERT_POINT,convertPoint);
            sfsObject.putInt(KJS.TRUE_COUNT, trueCount);
            sfsObject.putInt(KJS.FALSE_COUNT, falseCount);
            if (roundPoint == oldPoint) 
                sfsObject.putInt(KJS.RANK, oldRank);
            if (roundPoint < oldPoint) 
            {
                sfsObject.putInt(KJS.RANK, i);
                oldRank = i;
            }
            oldPoint = roundPoint;
            sfsArray.addSFSObject(sfsObject);
        }
        
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,sfsArray);
        send(CMDNF.ROUNDSCORINGUSERINFO_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }

    private void sendRoundScoringDownTime() 
    {
        int downTime=ServerConfig.ROUND_SCORING_DOWNTIME;
        for (int i = downTime; i >= 0;i--) 
        {
            SFSObject dataToClient = new SFSObject();
            dataToClient.putInt(KJS.DOWN_TIME, i);
            sleep(1000);
            send(CMDNF.ROUNDSCORINGDOWNTIME_NF,dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }
    
    protected void nextThread()
    {
    }
}

