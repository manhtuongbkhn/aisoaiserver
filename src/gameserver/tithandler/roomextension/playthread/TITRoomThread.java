package gameserver.tithandler.roomextension.playthread;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titconfig.ServerConfig;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;

abstract public class TITRoomThread extends Thread 
{
    protected ITITRoomService roomService;
    protected TITRoom room;
    
    @Override
    public void run()
    {
        try
        {
            titRun();
        }
        catch(Exception exc)
        {
            System.out.println("Exception TITRoom Thread");
        }
    }
    
    public TITRoomThread(ITITRoomService roomService) 
    {
        this.roomService = roomService;
        this.room = roomService.getRoom();
        setPriority(ServerConfig.PLAYING_THREAD_PRIORITY);
    }

    protected void trace(String message) 
    {
        this.roomService.titTrace(message);
    }

    public void send(String cmd, SFSObject toClientData, TITUser user) 
    {
        this.roomService.send(cmd, toClientData, user);
    }

    public void send(String cmd, SFSObject dataToClient, ArrayList<TITUser> userArr) 
    {
        this.roomService.send(cmd, dataToClient, userArr);
    }

    public void send(String cmd, JsonObject toClientData, TITUser user) 
    {
        this.roomService.send(cmd, toClientData, user);
    }

    public void send(String cmd, JsonObject toClientData, ArrayList<TITUser> userArr) 
    {
        this.roomService.send(cmd, toClientData, userArr);
    }

    public TITApi getTITApi() 
    {
        return this.roomService.getTITApi();
    }

    protected void sleep(int msTime) 
    {
        try 
        {
            Thread.sleep(msTime);
        }
        catch (InterruptedException ex) 
        {
            // empty catch block
        }
    }
    
    abstract public void titRun();
}

