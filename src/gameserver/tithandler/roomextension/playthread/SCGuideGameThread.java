package gameserver.tithandler.roomextension.playthread;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titconfig.TITFunction;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titsystem.titentities.room.gamecontext.SameChoiceRoundContext;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titgameinfo.TITGameScriptFactory;

public class SCGuideGameThread extends TITGuideGameThread
{
    public SCGuideGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void sendGameGuideInfo() 
    {
        SFSObject dataToClient = new SFSObject();
        SameChoiceRoundContext roundContext =room.getSameChoiceGameContext().
                                                    getCurrentRoundContext();
        TITGameInfo gameInfo = roundContext.getGameInfo();
        int gameId = gameInfo.getGameId();
        String gameName = gameInfo.getGameName();
        String gameGuide = gameInfo.getGameGuide();
        String mainType = gameInfo.getGameMainType();
        String secondType = gameInfo.getGameSecondaryType();
        dataToClient.putInt(KJS.GAME_ID, gameId);
        dataToClient.putUtfString(KJS.GAME_NAME, gameName);
        dataToClient.putUtfString(KJS.GAME_GUIDE, gameGuide);
        dataToClient.putUtfString(KJS.GAME_MAIN_TYPE, mainType);
        dataToClient.putUtfString(KJS.GAME_SECONDARY_TYPE, secondType);
        this.send(CMDNF.GAMEGUIDEINFO_NF, dataToClient,room.getDefaultRoomInfo().
                                                            getPlayerArr());
    }

    @Override
    protected void sendGameScript() 
    {
        SameChoiceRoundContext roundContext = room.getSameChoiceGameContext().
                                                    getCurrentRoundContext();
        TITGameInfo gameInfo = roundContext.getGameInfo();
        int gameId = gameInfo.getGameId();
        JsonObject result = TITGameScriptFactory.creteGameScript(gameId, 1);
        int time = result.get(KJS.TIME).getAsInt();
        int maxPoint=result.get(KJS.MAX_POINT).getAsInt();
        JsonArray gameScript = result.get(KJS.GAME_SCRIPT).getAsJsonArray();
        JsonArray gameAnswer = result.get(KJS.GAME_ANSWER).getAsJsonArray();
        roundContext.setGameOverTime(time);
        room.getSameChoiceGameContext().getCurrentRoundContext().
                                                        setMaxPoint(maxPoint);
        room.getSameChoiceGameContext().getCurrentRoundContext().
                                                    setGameScript(gameScript);
        room.getSameChoiceGameContext().getCurrentRoundContext().
                                                    setGameAnswer(gameAnswer);
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,
                                    TITFunction.covertToSFSArray(gameScript));
        this.send(CMDNF.GAMESCRIPT_NF, toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }
    
    @Override
    protected void nextThread()
    {
        SCPlayingGameThread playingGameThread = 
                                        new SCPlayingGameThread(roomService);
        playingGameThread.run();
    }
}

