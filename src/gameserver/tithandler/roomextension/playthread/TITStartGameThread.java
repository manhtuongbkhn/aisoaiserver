package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.TITRoomStatus;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITStartGameThread extends TITRoomThread 
{
    public TITStartGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }

    @Override
    public void titRun() 
    {
        room.getInvitationManager().reset();
        room.initGameContext();
        room.getGameContext().setCurrentRound(0);
        room.getDefaultRoomInfo().backupUserArr();
        room.getDefaultRoomInfo().setStatus(TITRoomStatus.PLAYING_GAME);
        initRoomUserId();
        changeUserStatus();
        sendStartGameDownTime();
        nextThread();
    }
    
    public void initRoomUserId()
    {
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for(int i=0;i<userArr.size();i++)
        {
            TITUser user=userArr.get(i);
            user.getDefaultUserInfo().setRoomUserId(i+1);
        }
    }
    
    private void changeUserStatus() 
    {
        for (int i = 0; i < room.getDefaultRoomInfo().getPlayerCount();i++) 
        {
            TITUser titUser = room.getDefaultRoomInfo().getPlayerArr().get(i);
            titUser.setStatus(TITUserStatus.PLAYING_GAME);
        }
    }

    private void sendStartGameDownTime() 
    {
        int downTime=ServerConfig.STARTGAME_DOWNTIME;
        for (int i=downTime;i>=0;i--) 
        {
            SFSObject dataToClient = new SFSObject();
            dataToClient.putInt(KJS.DOWN_TIME,i);
            sleep(1000);
            send(CMDNF.STARTGAMEDOWNTIME_NF,dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }
    
    public void nextThread()
    {
        TITChoiceGameThread choiceGameThread;
        switch(room.getDefaultRoomInfo().getPlayType())
        {
            case 1:
                choiceGameThread=new SCChoiceGameThread(roomService);
                break;
            case 2:
            default:
                choiceGameThread=new FCChoiceGameThread(roomService);
                break;
        }
        choiceGameThread.run();
    }
}

