package gameserver.tithandler.roomextension.playthread;

import gameserver.titsystem.titentities.room.TITRoomStatus;

public class NormalBeginRoomThread extends TITBeginRoomThread
{
    public NormalBeginRoomThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    

    @Override
    public void titRun() 
    {
        room.getDefaultRoomInfo().setStatus(TITRoomStatus.WAITING);
        changeUserStatus();
        room.initMessageManager();
        room.initInvitationManager();
    }
}

