package gameserver.tithandler.roomextension.playthread;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titsystem.titentities.TITSend;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;

public class TITRoomService implements ITITRoomService
{
    private TITRoom room;
    private TITApi api;
    private TITSend send;
        
    public TITRoomService(TITRoom room)
    {
        this.room=room;
        this.api=TITApi.getDefaultApi();
        this.send=TITSend.getDefaultSend();
    }
            
    @Override
    public void titTrace(String message) 
    {
        System.out.println(message);
    }

    @Override
    public void send(String cmd, SFSObject toClientData, TITUser user) 
    {
        send.send(cmd,toClientData,user);
    }

    @Override
    public void send(String cmd,SFSObject toClientData,
                                                    ArrayList<TITUser> userArr) 
    {
        send.send(cmd,toClientData,userArr);
    }

    @Override
    public void send(String cmd, JsonObject toClientData, TITUser user) 
    {
        send.send(cmd,toClientData,user);
    }

    @Override
    public void send(String cmd,JsonObject toClientData,
                                                    ArrayList<TITUser> userArr) 
    {
        send.send(cmd,toClientData,userArr);
    }

    @Override
    public TITApi getTITApi() 
    {
        return api;
    }

    @Override
    public TITRoom getRoom() 
    {
        return room;
    }
    
}
