package gameserver.tithandler.roomextension.playthread;

import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.user.TITUserStatus;

abstract public class TITBeginRoomThread extends TITRoomThread
{
    public TITBeginRoomThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    public void changeUserStatus() 
    {
        for (int i = 0; i < room.getDefaultRoomInfo().getPlayerCount();i++) 
        {
            TITUser titUser = room.getDefaultRoomInfo().getPlayerArr().get(i);
            titUser.setStatus(TITUserStatus.WAITING_INROOM);
        }
    }
}
