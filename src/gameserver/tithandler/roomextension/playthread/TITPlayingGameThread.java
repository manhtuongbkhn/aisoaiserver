package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundContext;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITPlayingGameThread extends TITRoomThread 
{
    private ReloadRoundPointThread reloadRoundPointThread;

    public TITPlayingGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }

    @Override
    public void titRun() 
    {
        room.getGameContext().getCurrentRoundContext().
                                        setStatus(TITRoundStatus.PLAYING_GAME);
        sendStartPlayingGame();
        sleep(ServerConfig.VIEWDELAY_MILLIS);
        initReloadRoundPointThread();
        sendAllUserInfoToAllUser();
        sendPlayingGameDownTime();
        reloadRoundPointThread.stopThread();
        nextThread();
    }

    protected void sendStartPlayingGame() 
    {
        
    }

    protected void initReloadRoundPointThread() 
    {
        reloadRoundPointThread = new ReloadRoundPointThread(roomService);
        reloadRoundPointThread.start();
    }

    protected void sendAllUserInfoToAllUser() 
    {
        SFSArray sfsArray = room.getDefaultRoomInfo().getAllUserPublicInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,sfsArray);
        ArrayList<TITUser> userArr = room.getDefaultRoomInfo().getPlayerArr();
        send(CMDNF.PLAYINGGAMEUSERINFO_NF, toClientData, userArr);
    }

    protected void sendPlayingGameDownTime() 
    {
        int timeGameOver=room.getGameContext().
                                    getCurrentRoundContext().getGameOverTime();
        for (int i = timeGameOver; i >= 0; --i) 
        {
            SFSObject dataToClient = new SFSObject();
            dataToClient.putInt(KJS.DOWN_TIME, i);
            sleep(1000);
            send(CMDNF.PLAYINGGAMEDOWNTIME_NF, dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
        sleep(2000);
    }

    protected void nextThread()
    {
        
    }
}
    class ReloadRoundPointThread extends TITRoomThread
    {
        private boolean stop;

        ReloadRoundPointThread(ITITRoomService roomService)
        {
            super(roomService);
            stop = false;
        }

        public void stopThread() 
        {
            stop = true;
        }

        @Override
        public void titRun()
        {
            while (!this.stop) 
            {
                ArrayList<Integer> pointArr = new ArrayList<Integer>();
                ArrayList<TITUser> userArr =
                                    room.getDefaultRoomInfo().getPlayerArr();
                TITRoundContext roundContext=room.getGameContext().
                                                    getCurrentRoundContext();
                for (int i = 0; i < userArr.size(); i++) 
                {
                    TITUser titUser = userArr.get(i);
                    int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
                    int point = roundContext.getUserRoundGameContext
                                                        (roomUserId).getPoint();
                    pointArr.add(point);
                }
                SFSObject toClientData = new SFSObject();
                toClientData.putIntArray(KJS.POINT_ARR, pointArr);
                send(CMDNF.RELOADPLAYINGPOINT_NF,toClientData, userArr);
                try 
                {
                    Thread.sleep(ServerConfig.RELOAD_POINT_TIME_MILLIS);
                }
                catch (InterruptedException ex) {}
            }
        }
    }

