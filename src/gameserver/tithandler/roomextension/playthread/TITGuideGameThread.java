package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITGuideGameThread extends TITRoomThread
{
    public TITGuideGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
   
    @Override
    public void titRun() 
    {
        room.getGameContext().getCurrentRoundContext().
                                    setStatus(TITRoundStatus.SHOWING_GAME_INFO);
        send(CMDNF.STARTGUIDEGAME_NF,new SFSObject(),
                                    room.getDefaultRoomInfo().getPlayerArr());
        sleep(ServerConfig.VIEWDELAY_MILLIS);
        sendGameGuideInfo();
        sendGuideGameUserInfo();
        sendGameScript();
        sendGuideGameDownTime();
        nextThread();
    }
    
    protected void sendGameGuideInfo()
    {
        
    }
    
    protected void sendGuideGameUserInfo() 
    {
        ArrayList<TITUser> userArr = room.getDefaultRoomInfo().getPlayerArr();
        SFSArray sfsArray = new SFSArray();
        for (int i = 0; i < userArr.size(); ++i) 
        {
            TITUser titUser = userArr.get(i);
            SFSObject sfsObject = titUser.getPublicInfo();
            int roomUserId=titUser.getDefaultUserInfo().getRoomUserId();
            int totalPoint = room.getGameContext().getTotalPoint(roomUserId);
            sfsObject.putInt(KJS.POINT,totalPoint);
            sfsArray.addSFSObject(sfsObject);
        }
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,sfsArray);
        send(CMDNF.GUIDEGAMEUSERINFO_NF,toClientData,userArr);
    }
    
    protected void sendGameScript()
    {
        
    }
    
    protected void sendGuideGameDownTime() 
    {
        int downTime=ServerConfig.GUIDEGAME_DOWNTIME;
        for (int i = downTime; i >= 0;i--) 
        {
            SFSObject dataToClient = new SFSObject();
            dataToClient.putInt(KJS.DOWN_TIME, i);
            sleep(1000);
            send(CMDNF.GUIDEGAMEDOWNTIME_NF, dataToClient,
                                    room.getDefaultRoomInfo().getPlayerArr());
        }
    }
    
    protected void nextThread()
    {
        
    }
}
