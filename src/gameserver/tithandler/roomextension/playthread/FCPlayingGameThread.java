package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceUserRoundContext;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class FCPlayingGameThread extends TITPlayingGameThread
{
    public FCPlayingGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void sendStartPlayingGame() 
    {
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for(int i=0;i<userArr.size();i++)
        {
            TITUser user=userArr.get(i);
            int roomUserId=user.getDefaultUserInfo().getRoomUserId();
            ForChoiceUserRoundContext userRoundContext=room.
                            getForChoiceGameContext().getCurrentRoundContext().
                                                getUserRoundContext(roomUserId);
            TITGameInfo gameInfo=userRoundContext.getGameInfo();
            SFSObject toClientData = new SFSObject();
            toClientData.putInt(KJS.GAME_ID,gameInfo.getGameId());
            toClientData.putUtfString(KJS.GAME_NAME, gameInfo.getGameName());
            send(CMDNF.STARTPLAYINGGAME_NF,toClientData,user);
        }
    }
    
    @Override
    protected void nextThread()
    {
        FCRoundScoringThread scoringThread=new FCRoundScoringThread(roomService);
        scoringThread.run();
    }
}
