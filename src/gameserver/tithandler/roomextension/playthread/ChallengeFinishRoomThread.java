package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;

public class ChallengeFinishRoomThread extends TITFinishRoomThread
{
    public ChallengeFinishRoomThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    public void titRun()
    {
        kickSpectator();
        getTITApi().removeRoom(room);
    }
    
    protected void kickSpectator() 
    {
        ArrayList<TITUser> userArr = room.getDefaultRoomInfo().getSpectatorArr();
        for (int i = 0; i < userArr.size(); ++i) 
        {
            TITUser titUser = userArr.get(i);
            getTITApi().exitRoom(titUser,room);
            send(CMDNF.WASKICKROOMBYSYSTEM_NF,new SFSObject(),titUser);
        }
    }
}
