package gameserver.tithandler.roomextension.playthread;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;

public interface ITITRoomService 
{
    public void titTrace(String message);

    public void send(String cmd, SFSObject toClientData, TITUser user);

    public void send(String cmd,SFSObject toClientData,
                                                    ArrayList<TITUser> userArr);

    public void send(String cmd, JsonObject toClientData,TITUser user);

    public void send(String cmd,JsonObject toClientData,
                                                    ArrayList<TITUser> userArr);

    public TITApi getTITApi();

    public TITRoom getRoom();
}

