package gameserver.tithandler.roomextension.playthread;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceRoundContext;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceUserRoundContext;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titgameinfo.TITGameScriptFactory;

public class FCGuideGameThread extends TITGuideGameThread
{
    public FCGuideGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }

    @Override
    protected void sendGameGuideInfo() 
    {
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for(int i=0;i<userArr.size();i++)
        {
            TITUser user=userArr.get(i);
            int roomUserId=user.getDefaultUserInfo().getRoomUserId();
            ForChoiceUserRoundContext userRoundContext=room.
                getForChoiceGameContext().getCurrentRoundContext().
                                                getUserRoundContext(roomUserId);
            TITGameInfo gameInfo=userRoundContext.getGameInfo();
            SFSObject toClientData=new SFSObject();
            toClientData.putInt(KJS.GAME_ID,gameInfo.getGameId());
            toClientData.putUtfString(KJS.GAME_NAME,gameInfo.getGameName());
            toClientData.putUtfString(KJS.GAME_GUIDE,gameInfo.getGameGuide());
            toClientData.putUtfString(KJS.GAME_MAIN_TYPE,
                                                    gameInfo.getGameMainType());
            toClientData.putUtfString(KJS.GAME_SECONDARY_TYPE,
                                            gameInfo.getGameSecondaryType());
            send(CMDNF.GAMEGUIDEINFO_NF,toClientData,user);
        }
    }

    @Override
    protected void sendGameScript() 
    {
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        ForChoiceRoundContext roundContext=room.getForChoiceGameContext().
                                                    getCurrentRoundContext();
        for(int i=0;i<userArr.size();i++)
        {
            TITUser user=userArr.get(i);
            int roomUserId=user.getDefaultUserInfo().getRoomUserId();
            ForChoiceUserRoundContext userRoundContext=roundContext.
                                                getUserRoundContext(roomUserId);
            TITGameInfo gameInfo=userRoundContext.getGameInfo();
            int gameId=gameInfo.getGameId();
            JsonObject result=TITGameScriptFactory.creteGameScript(gameId,1);
            int maxPoint=result.get(KJS.MAX_POINT).getAsInt();
            JsonArray gameScript = result.get(KJS.GAME_SCRIPT).getAsJsonArray();
            JsonArray gameAnswer = result.get(KJS.GAME_ANSWER).getAsJsonArray();
            int time = result.get(KJS.TIME).getAsInt();
            
            roundContext.setGameOverTime(time);
            userRoundContext.setMaxPoint(maxPoint);
            userRoundContext.setGameAnswer(gameAnswer);
            userRoundContext.setGameScript(gameScript);
            
            SFSObject toClientData = new SFSObject();
            toClientData.putSFSArray(KJS.ARRAY,
                                    TITFunction.covertToSFSArray(gameScript));
            send(CMDNF.GAMESCRIPT_NF, toClientData,user);
        }
    }
    
    @Override
    protected void nextThread()
    {
        FCPlayingGameThread playingGameThread = 
                                        new FCPlayingGameThread(roomService);
        playingGameThread.run();
    }
}
