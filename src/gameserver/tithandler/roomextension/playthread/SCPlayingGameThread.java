package gameserver.tithandler.roomextension.playthread;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class SCPlayingGameThread extends TITPlayingGameThread
{
    public SCPlayingGameThread(ITITRoomService roomService) 
    {
        super(roomService);
    }
    
    @Override
    protected void sendStartPlayingGame() 
    {
        SFSObject toClientData = new SFSObject();
        TITGameInfo gameInfo =room.getSameChoiceGameContext().
                                        getCurrentRoundContext().getGameInfo();
        toClientData.putInt(KJS.GAME_ID,gameInfo.getGameId());
        toClientData.putUtfString(KJS.GAME_NAME, gameInfo.getGameName());
        send(CMDNF.STARTPLAYINGGAME_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }
    
    @Override
    protected void nextThread()
    {
        SCRoundScoringThread scoringThread=new SCRoundScoringThread(roomService);
        scoringThread.run();
    }
}
