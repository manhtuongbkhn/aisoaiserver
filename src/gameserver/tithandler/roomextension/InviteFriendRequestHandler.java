package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.TITInvitation;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class InviteFriendRequestHandler extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        System.out.println("Invite Friend Request");
        
        if (user.getDefaultUserInfo().isOwner()) 
        {
            checkInvitedUserWasFriend();
        } 
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.ISNT_ROOM_OWNER);
            send(CMDRP.INVITEFRIEND_RP,toClientData,user);
        }
    }

    private void checkInvitedUserWasFriend()
    {
        int systemUserId = fromClientData.getInt(KJS.SYSTEM_USER_ID);
        TITUser invitedUser = getTITApi().getUserBySystemUserId(systemUserId);
        if(invitedUser==null)
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.USER_NOT_ONLINE);
            send(CMDRP.INVITEFRIEND_RP,toClientData,user);
        }
        else
        {
            if(user.getFriendManager().isFriend(invitedUser))
            {
                checkInviteFriendStatus();
            }
            else
            {
                SFSObject toClientData = new SFSObject();
                toClientData.putBool(KJS.SUCESS,false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.USER_NOT_FRIEND);
                send(CMDRP.INVITEFRIEND_RP,toClientData,user);
            }
        }
    }
    
    private void checkInviteFriendStatus() 
    {
        int systemUserId = fromClientData.getInt(KJS.SYSTEM_USER_ID);
        TITUser invitedUser = getTITApi().getUserBySystemUserId(systemUserId);
        switch (invitedUser.getStatus()) 
        {
            case GETED_PROFILE: 
            case IMPORTANT_REQUESTING: 
                inviteFriend(invitedUser);
                break;
            default:
                SFSObject toClientData = new SFSObject();
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.FRIEND_CANT_JOIN);
                toClientData.putUtfString(KJS.FULL_NAME,
                                    invitedUser.getUserProfile().getFullName());
                this.send(CMDRP.INVITEFRIEND_RP, toClientData,user);
                break;
        }
    }

    private void inviteFriend(TITUser invitedUser) 
    {
        TITInvitation invitation = new TITInvitation(user,invitedUser);
        room.getInvitationManager().add(invitation);
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        
        send(CMDRP.INVITEFRIEND_RP, toClientData,user);
        
        sendWasInvitedUser(invitation,invitedUser);
    }

    private void sendWasInvitedUser(TITInvitation invitation,TITUser invitedUser)
    {
        SFSObject toClientData=new SFSObject();
        toClientData.putUtfString(KJS.FULL_NAME,
                                        user.getUserProfile().getFullName());
        toClientData.putUtfString(KJS.AVATAR_URL,
                                        user.getUserProfile().getAvatarUrl());
        toClientData.putUtfString(KJS.USER_ID,
                                        user.getUserProfile().getUserId());
        toClientData.putInt(KJS.ROOM_ID,room.getDefaultRoomInfo().getId());
        toClientData.putUtfString(KJS.ROOM_NAME,
                                        room.getDefaultRoomInfo().getName());
        toClientData.putUtfString(KJS.TIME,
                                    invitation.getInviteTime().toGMTString());
        String notification = user.getUserProfile().getFullName() + 
                    " m\u1eddi b\u1ea1n v\u00e0o ph\u00f2ng ch\u01a1i game ?";
        toClientData.putUtfString(KJS.NOTIFICATION,notification);
        send(CMDNF.WASINVITED_NF,toClientData, invitedUser);
    }
    
    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case WAITING_INROOM:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        String userId = fromClientData.getUtfString(KJS.USER_ID);
        Integer systemUserId = fromClientData.getInt(KJS.SYSTEM_USER_ID);
        String fullName = fromClientData.getUtfString(KJS.FULL_NAME);
        if (userId == null) 
        {
            return false;
        }
        if (systemUserId == null) 
        {
            return false;
        }
        if (fullName == null) 
        {
            return false;
        }
        return this.checkValidateInfo(systemUserId,fullName);
    }

    private boolean checkValidateInfo(int systemUserId, String fullName) 
    {
        return true;
    }

}

