package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.TITRoomRequestHandler;

public class KickPlayerRequestHandler extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        if (user.getDefaultUserInfo().isOwner()) 
        {
            this.kickUser();
        } else {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool("sucess", false);
            toClientData.putUtfString("system_message", "ISNT_ROOM_OWNER");
            this.send("kick_player_rp", toClientData, this.user);
        }
    }

    private void kickUser() {
        int sytemUserId = this.fromClientData.getInt("system_user_id");
        String fullName = this.fromClientData.getUtfString("full_name");
        TITUser titUser = room.getDefaultRoomInfo().
                                                getUserBySystemId(sytemUserId);
        if (titUser == null) 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool("sucess", false);
            toClientData.putInt("user_id", sytemUserId);
            toClientData.putUtfString("full_name", fullName);
            toClientData.putUtfString("system_message", "USER_NOT_IN_ROOM");
            this.send("kick_player_rp", toClientData, this.user);
        } 
        else 
        {
            this.getTITApi().exitRoom(titUser, this.room);
            SFSObject toClientData = new SFSObject();
            toClientData.putBool("sucess", true);
            toClientData.putInt("user_id", sytemUserId);
            toClientData.putUtfString("full_name", fullName);
            this.send("kick_player_rp", toClientData, this.user);
            titUser.setStatus(TITUserStatus.GETED_PROFILE);
            toClientData = new SFSObject();
            this.send("user_was_kicked_user_nf", toClientData, titUser);
        }
    }

    private boolean checkValidateInfo(int systemUserId, String fullName) {
        return true;
    }

    @Override
    protected void changeUserStatusBegin() {
    }

    @Override
    protected void changeUserStatusEnd() {
    }

    @Override
    protected void changeRoomStatusBegin() {
    }

    @Override
    protected void changeRoomStatusEnd() {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case WAITING_INROOM:
            case SCORING_TOTALPOINT:
                return true;
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() {
        String userId = this.fromClientData.getUtfString("user_id");
        Integer systemUserId = this.fromClientData.getInt("system_user_id");
        String fullName = this.fromClientData.getUtfString("full_name");
        if (userId == null) {
            return false;
        }
        if (systemUserId == null) {
            return false;
        }
        if (fullName == null) {
            return false;
        }
        return this.checkValidateInfo(systemUserId, fullName);
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
            case REFESHING:
                return true;
        }
        return false;
    }

}

