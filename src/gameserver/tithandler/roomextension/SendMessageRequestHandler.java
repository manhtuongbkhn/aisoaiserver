package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.Date;
import gameserver.titconfig.TITFilter;
import gameserver.titsystem.titentities.room.TITMessageInfo;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class SendMessageRequestHandler extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        String messageContent = fromClientData.getUtfString(KJS.MESSAGE_CONTENT);
        messageContent=TITFilter.messageFilter(messageContent);
        String avatarUrl = user.getUserProfile().getAvatarUrl();
        TITMessageInfo messageInfo = this.createMessageInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        send(CMDRP.SENDMESSAGE_RP, toClientData, this.user);
        send(CMDNF.NEWMESSAGE_NF, messageInfo.toSFSObject(),
                                    room.getDefaultRoomInfo().getPlayerArr());
    }

    protected TITMessageInfo createMessageInfo() 
    {
        String messageContent = 
                            fromClientData.getUtfString(KJS.MESSAGE_CONTENT);
        TITMessageInfo messageInfo = new TITMessageInfo();
        messageInfo.setSendUser(user);
        messageInfo.setSendTime(new Date());
        messageInfo.setMessageContent(messageContent);
        room.getMessageManager().add(messageInfo);
        return messageInfo;
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    public void changeUserStatusEnd() 
    {
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (this.user.getStatus()) 
        {
            case WAITING_INROOM: 
            {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() 
    {
        String messageContent = fromClientData.
                                            getUtfString(KJS.MESSAGE_CONTENT);
        if (messageContent != null) 
        {
            return this.checkMessage(messageContent);
        }
        return false;
    }

    private boolean checkMessage(String message) 
    {
        if (message.length() == 0 || message.length() >= 50) 
        {
            return false;
        }
        return true;
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
                return true;
        }
        return false;
    }

}

