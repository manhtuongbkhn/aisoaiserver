package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.user.service.InvitationUserServing;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class InvitationRequestHandler extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        System.out.println("Invitation Request");
        
        if (!user.getDefaultUserInfo().isOwner()) 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.ISNT_ROOM_OWNER);
            send(CMDRP.INVITATION_RP, toClientData,user);
        } 
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,true);
            send(CMDRP.INVITATION_RP,toClientData,user);
            
            try { Thread.sleep(200);} catch (Exception ex) {}
            
            InvitationUserServing userServing=user.getUserServingManager().
                                                    getInvitationUserServing();
            userServing.setRoom(room);
            userServing.onServing();
        }
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case WAITING_INROOM: 
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

