package gameserver.tithandler.roomextension;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceUserRoundContext;
import gameserver.titsystem.titentities.room.gamecontext.SameChoiceUserRoundContext;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.KJS;
import gameserver.titgameinfo.TITGameScriptFactory;

public class GameAnswerRequestHandler
extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        int playType=room.getDefaultRoomInfo().getPlayType();
        int roomUserId=user.getDefaultUserInfo().getRoomUserId();
        JsonObject userAnswer = TITFunction.
                                            covertToJsonObject(fromClientData);
        switch(playType)
        {
            case 1:
            {
                SameChoiceUserRoundContext userRoundContext;
                userRoundContext=room.getSameChoiceGameContext().
                getCurrentRoundContext().getUserRoundContext(roomUserId);
                int gameId = room.getSameChoiceGameContext().
                getCurrentRoundContext().getGameInfo().getGameId();
                JsonArray gameAnswer = room.getSameChoiceGameContext().
                                    getCurrentRoundContext().getGameAnswer();
                JsonArray result = TITGameScriptFactory.
                                checkGameAnswer(gameId,gameAnswer,userAnswer);
                for(int i=0;i<result.size();i++)
                {
                    JsonObject jsonObject=result.get(i).getAsJsonObject();
                    String methodName=jsonObject.
                                            get(KJS.METHOD_NAME).getAsString();
                    switch(methodName)
                    {
                        case KJS.ADD_POINT:
                            int value=jsonObject.get(KJS.VALUE).getAsInt();
                            userRoundContext.addPoint(value);
                            break;
                        case KJS.MUL_POINT:
                            Integer currentPoint=userRoundContext.getPoint();
                            float fValue=jsonObject.get(KJS.VALUE).getAsFloat();
                            if(currentPoint>=0)
                            {
                                Float newpoint=currentPoint*fValue;
                                userRoundContext.setPoint(newpoint.intValue());
                            }
                            else
                            {
                                Float newpoint=currentPoint/fValue;
                                userRoundContext.setPoint(newpoint.intValue());
                            }                               
                            break;
                        case KJS.ADD_TRUE_COUNT:
                             userRoundContext.addTrueCount(1);
                            break;
                        case KJS.ADD_FALSE_COUNT:
                            userRoundContext.addFalseCount(1);
                            break;
                    }
                }
                break;
            }
            case 2:
            {
                ForChoiceUserRoundContext userRoundContext=room.
                    getForChoiceGameContext().getCurrentRoundContext().
                                            getUserRoundContext(roomUserId);
                int gameId=userRoundContext.getGameInfo().getGameId();
                JsonArray gameAnswer=userRoundContext.getGameAnswer();
                JsonArray gamScript=userRoundContext.getGameScript();
                JsonArray result = TITGameScriptFactory.
                                checkGameAnswer(gameId,gameAnswer,userAnswer);
                for(int i=0;i<result.size();i++)
                {
                    JsonObject jsonObject=result.get(i).getAsJsonObject();
                    String methodName=jsonObject.
                                            get(KJS.METHOD_NAME).getAsString();
                    switch(methodName)
                    {
                        case KJS.ADD_POINT:
                            int value=jsonObject.get(KJS.VALUE).getAsInt();
                            userRoundContext.addPoint(value);
                            break;
                        case KJS.MUL_POINT:
                            Integer currentPoint=userRoundContext.getPoint();
                            float fValue=jsonObject.get(KJS.VALUE).getAsFloat();
                            if(currentPoint>=0)
                            {
                                Float newpoint=currentPoint*fValue;
                                userRoundContext.setPoint(newpoint.intValue());
                            }
                            else
                            {
                                Float newpoint=currentPoint/fValue;
                                userRoundContext.setPoint(newpoint.intValue());
                            }                               
                            break;
                        case KJS.ADD_TRUE_COUNT:
                             userRoundContext.addTrueCount(1);
                            break;
                        case KJS.ADD_FALSE_COUNT:
                            userRoundContext.addFalseCount(1);
                            break;
                    }
                }
                break;
            }
        }
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case PLAYING_GAME:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case PLAYING_GAME:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        Integer index = fromClientData.getInt(KJS.INDEX);
        if (index == null) 
        {
            return false;
        }
        return true;
    }

}

