package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.tithandler.TITRoomRequestHandler;

public class ExitRoomRequestHandler
extends TITRoomRequestHandler {
    @Override
    protected void titHandClientRequest() 
    {
        this.getTITApi().exitRoom(this.user, this.room);
        SFSObject toClientData = new SFSObject();
        toClientData.putBool("sucess", true);
        this.send("exit_room_rp", toClientData, this.user);
    }

    @Override
    protected void changeUserStatusBegin() {
        this.user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() {
        this.user.setStatus(TITUserStatus.GETED_PROFILE);
    }

    @Override
    protected void changeRoomStatusBegin() {
    }

    @Override
    protected void changeRoomStatusEnd() {
    }

    @Override
    protected boolean checkUserStatus() {
        switch (this.user.getStatus()) {
            case WAITING_INROOM: {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING: 
            case REFESHING:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() {
        return true;
    }

}

