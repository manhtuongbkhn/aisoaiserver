package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.TITRoomEventHanler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class LeaveRoomEventHandler extends TITRoomEventHanler 
{
    @Override
    protected void titHandleServerEvent() 
    {
        this.trace("Leave Room Event Room--------------------\n");
        if (room.getDefaultRoomInfo().getPlayerCount() > 0) 
        {
            if (user.getDefaultUserInfo().isOwner()) 
            {
                int radom = TITFunction.randInt(room.
                                getDefaultRoomInfo().getOnlinePlayerCount());
                TITUser newOwner = room.getDefaultRoomInfo().
                                                getOnlinePlayerArr().get(radom-1);
                room.getDefaultRoomInfo().setOwner(newOwner);
            }
            reloadRoomInfo();
            reloadPlayerList();
        } 
        else 
        {
            this.getTITApi().removeRoom(this.room);
        }
        user.getDefaultUserInfo().setBackupRoomId(-1);
    }

    private void reloadRoomInfo() 
    {
        SFSObject roomInfo=new SFSObject();
        room.getDefaultRoomInfo().getPublicInfo(roomInfo);
        this.send(CMDNF.ROOMINFO_NF,roomInfo,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }

    private void reloadPlayerList() 
    {
        SFSArray userInfoArray = room.getDefaultRoomInfo().
                                                        getAllUserPublicInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY,userInfoArray);
        this.send(CMDNF.ALLPLAYERINFO_NF,toClientData,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }
}

