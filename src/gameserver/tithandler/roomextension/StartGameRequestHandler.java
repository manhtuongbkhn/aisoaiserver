package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.roomextension.playthread.TITStartGameThread;
import gameserver.titsystem.titentities.room.TITRoomStatus;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.tithandler.roomextension.playthread.TITRoomService;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;

public class StartGameRequestHandler extends TITRoomRequestHandler
{
    private boolean requestSucess = false;

    @Override
    protected void titHandClientRequest() 
    {
        switch(room.getDefaultRoomInfo().getStatus())
        {
            case WAITING:
                checkOwner();
                break;
            case REFESHING:
                SFSObject toClientData = new SFSObject();
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                            StrDefine.ROOM_STATUS_INVALIDATE);
                send(CMDRP.STARTGAME_RP,toClientData,user);
                break;
            default:
                break;
        }
    }
    
    private void checkOwner()
    {
        if (user.getDefaultUserInfo().isOwner())
            checkCountPlayer();
        else
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString
                                (KJS.SYSTEM_MESSAGE,StrDefine.ISNT_ROOM_OWNER);
            send(CMDRP.STARTGAME_RP,toClientData,user);
        }
    }
    
    private void checkCountPlayer() 
    {
        if (room.getDefaultRoomInfo().getPlayerCount() == 
                                    room.getDefaultRoomInfo().getPlayerMax()) 
        {
            this.requestSucess = true;
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS, true);
            this.send(CMDRP.STARTGAME_RP,toClientData,user);
            toClientData = new SFSObject();
            send(CMDNF.READYSTARTGAME_NF,toClientData,
                                    room.getDefaultRoomInfo().getPlayerArr());
            TITRoomService roomService=new TITRoomService(room);
            TITStartGameThread startGameThread = 
                                            new TITStartGameThread(roomService);
            startGameThread.start();
        } 
        else 
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.NOT_ENOUGH_USER);
            this.send(CMDRP.STARTGAME_RP,toClientData,user);
        }
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        user.setStatus(TITUserStatus.IMPORTANT_REQUESTING);
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        if (!this.requestSucess) 
        {
            this.user.setStatus(TITUserStatus.WAITING_INROOM);
        }
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
        if (!this.requestSucess) 
        {
            room.getDefaultRoomInfo().setStatus(TITRoomStatus.WAITING);
        }
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case WAITING_INROOM: 
                return true;
        }
        return false;
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING:
            case REFESHING:
                return true;
        }
        return false;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }
}

