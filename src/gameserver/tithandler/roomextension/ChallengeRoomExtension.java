package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.core.SFSEventType;
import gameserver.tithandler.TITRoomExtension;
import gameserver.titconfig.CMDRQ;
import gameserver.tithandler.zoneextension.BackRoomFinishRequestHandler;
import gameserver.tithandler.zoneextension.ExitRoomFinishRequestHandler;

public class ChallengeRoomExtension extends TITRoomExtension 
{
    @Override
    public void init() 
    {
        initEventHandler();
        initRequestHandler();
    }

    @Override
    protected void initEventHandler() 
    {
        addEventHandler(SFSEventType.USER_JOIN_ROOM,JoinRoomEventHandler.class);
        addEventHandler(SFSEventType.USER_LEAVE_ROOM,
                                                LeaveRoomEventHandler.class);
    }

    @Override
    protected void initRequestHandler() 
    {
        addRequestHandler(CMDRQ.ROOMINFO_RQ,RoomInfoRequestHandler.class);
        addRequestHandler(CMDRQ.ALLPLAYERINFO_RQ,
                                            AllPlayerInfoRequestHandler.class);
        addRequestHandler(CMDRQ.SENDMESSAGE_RQ,SendMessageRequestHandler.class);
        addRequestHandler(CMDRQ.ALLMESSAGEINFO_RQ,
                                            AllMessageInfoRequestHandler.class);
        addRequestHandler(CMDRQ.CHOICEGAME_RQ,
                                            ChoiceGameRequestHandler.class);
        addRequestHandler(CMDRQ.GAMEANSWER_RQ,GameAnswerRequestHandler.class);
        addRequestHandler(CMDRQ.EXITROOMFINISH_RQ,
                                            ExitRoomFinishRequestHandler.class);
    }
}

