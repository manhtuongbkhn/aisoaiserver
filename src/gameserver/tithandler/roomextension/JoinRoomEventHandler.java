package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.TITRoomEventHanler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class JoinRoomEventHandler extends TITRoomEventHanler 
{
    @Override
    protected void titHandleServerEvent() 
    {
        this.trace("Join Room Event ---------------------\n");
        try {
            Thread.sleep(800);
        }
        catch (InterruptedException ex) 
        {
            // empty catch block
        }
        reloadRoomInfo();
        reloadPlayerList();
        user.getDefaultUserInfo().setBackupRoomId
                                            (room.getDefaultRoomInfo().getId());
    }

    public void reloadRoomInfo() 
    {
        SFSObject roomInfo=new SFSObject();
        room.getDefaultRoomInfo().getPublicInfo(roomInfo);
        send(CMDNF.ROOMINFO_NF, roomInfo,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }

    public void reloadPlayerList() 
    {
        SFSArray userInfoArray = room.getDefaultRoomInfo().
                                                        getAllUserPublicInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY, (ISFSArray)userInfoArray);
        send(CMDNF.ALLPLAYERINFO_NF,toClientData,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }

    @Override
    public void changeUserStatusBegin() 
    {
    }

    @Override
    public void changeUserStatusEnd() 
    {
    }

    @Override
    public void changeRoomStatusBegin() 
    {
    }

    @Override
    public void changeRoomStatusEnd() 
    {
    }

    @Override
    public boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    public boolean checkRoomStatus() 
    {
        return true;
    }

    @Override
    public boolean checkRequestData() 
    {
        return true;
    }
}

