package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundStatus;
import gameserver.titconfig.TITFilter;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceUserRoundContext;
import gameserver.titgameinfo.TITGameInfo;
import gameserver.titsystem.titentities.room.gamecontext.TITRoundContext;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;
import gameserver.titconfig.StrDefine;
import gameserver.titgameinfo.TITGameInfoFactory;

public class ChoiceGameRequestHandler
extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        checkChoiceGameUser();
    }
    
    private void checkChoiceGameUser()
    {
        int playType=room.getDefaultRoomInfo().getPlayType();
        int round=room.getGameContext().getCurrentRound();
        int roundCount;
        switch(playType)
        {
            case 1:
                TITUser choiceGameUser;
                roundCount=room.getSameChoiceGameContext().getRoundCount();
                if(round==roundCount)
                    choiceGameUser=null;
                else
                    choiceGameUser = room.getDefaultRoomInfo().
                                                    getPlayerArr().get(round-1);
                if (user.equals(choiceGameUser)) 
                {
                    checkRoundStatus();
                } 
                else 
                {
                    SFSObject toClientData = new SFSObject();
                    toClientData.putBool(KJS.SUCESS, false);
                    toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                        StrDefine.NOT_PERMISSION_CHOICE_GAME);
                    send(CMDRP.CHOICEGAME_RP,toClientData,user);
                }
                break;
            case 2:
                roundCount=room.getForChoiceGameContext().getRoundCount();
                if(round==roundCount)
                {
                    SFSObject toClientData = new SFSObject();
                    toClientData.putBool(KJS.SUCESS, false);
                    toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                        StrDefine.NOT_PERMISSION_CHOICE_GAME);
                    send(CMDRP.CHOICEGAME_RP,toClientData,user);
                }
                else
                    checkRoundStatus();
                break;
        }
    }
    
    private void checkRoundStatus()
    {
        TITRoundContext roundContext=room.getGameContext().
                                                    getCurrentRoundContext();
        if(roundContext.getStatus()==TITRoundStatus.CHOICING_GAME)
            checkGame();
        else          
        {
            SFSObject toClientData = new SFSObject();
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                        StrDefine.NOT_PERMISSION_CHOICE_GAME);
            send(CMDRP.CHOICEGAME_RP,toClientData,user);
        }
            
    }
    
    private void checkGame() 
    {
        Integer gameId = fromClientData.getInt(KJS.GAME_ID);
        String gameName = fromClientData.getUtfString(KJS.GAME_NAME);
        int playType=room.getDefaultRoomInfo().getPlayType();
        int round=room.getGameContext().getCurrentRound();
        ArrayList<Integer> gameIdArr;
        ArrayList<Integer> choicedGameIdArr;
        switch(playType)
        {
            case 1:
                gameIdArr=user.getGameUserInfo().getGameIdArr();
                choicedGameIdArr=room.
                            getSameChoiceGameContext().getChoicedGameIdArr();
                TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
                if(gameIdArr.contains(gameId))
                    checkChoicedGame();
                else
                {
                    SFSObject toClientData = new SFSObject();
                    toClientData.putBool(KJS.SUCESS, false);
                    toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                StrDefine.GAME_NOT_INVALID);
                    send(CMDRP.CHOICEGAME_RP,toClientData,user);
                }
                break;
            case 2:
                gameIdArr=user.getGameUserInfo().getGameIdArr();
                int choiceGameRoomUserId=
                                    user.getDefaultUserInfo().getRoomUserId();
                int wasChoicedGameUserRoomUserId=room.getForChoiceGameContext().
                    getCurrentRoundContext().getWasChoiceGameRoomUserId
                                                        (choiceGameRoomUserId);
                //TITUser wasChoicedGameUser=room.getDefaultRoomInfo().
                            //getUserByRoomUserId(wasChoicedGameUserRoomUserId);
                choicedGameIdArr=room.getForChoiceGameContext().
                            getChoicedGameIdArr(wasChoicedGameUserRoomUserId);
                TITFilter.gameIdFilter(gameIdArr,choicedGameIdArr);
                if(gameIdArr.contains(gameId))
                    checkChoicedGame();
                else
                {
                    SFSObject toClientData = new SFSObject();
                    toClientData.putBool(KJS.SUCESS, false);
                    toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                StrDefine.GAME_NOT_INVALID);
                    send(CMDRP.CHOICEGAME_RP,toClientData,user);
                }
                break;
        }
    }
    
    private void checkChoicedGame()
    {
        int playType=room.getDefaultRoomInfo().getPlayType();
        switch(playType)
        {
            case 1:
                if(!room.getSameChoiceGameContext().
                                    getCurrentRoundContext().isChoicedGame())
                    choiGame();
                else
                {
                    SFSObject toClientData = new SFSObject();
                    toClientData.putBool(KJS.SUCESS, false);
                    toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.CHOICED_GAME);
                    send(CMDRP.CHOICEGAME_RP,toClientData,user);
                }
                break;
            case 2:
                int choiceGameRoomUserId=
                                    user.getDefaultUserInfo().getRoomUserId();
                int wasChoicedGameUserRoomUserId=room.getForChoiceGameContext().
                    getCurrentRoundContext().getWasChoiceGameRoomUserId
                                                        (choiceGameRoomUserId);
                ForChoiceUserRoundContext userRoundContext=room.
                    getForChoiceGameContext().getCurrentRoundContext().
                            getUserRoundContext(wasChoicedGameUserRoomUserId);
                if(!userRoundContext.isChoicedGame())
                {
                    choiGame();
                }
                else
                {
                    SFSObject toClientData = new SFSObject();
                    toClientData.putBool(KJS.SUCESS, false);
                    toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.CHOICED_GAME);
                    send(CMDRP.CHOICEGAME_RP,toClientData,user);
                }
                break;
        }   
    }

    private void choiGame() 
    {
        Integer gameId = fromClientData.getInt(KJS.GAME_ID);
        String gameName = fromClientData.getUtfString(KJS.GAME_NAME);
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS,true);
        send(CMDRP.CHOICEGAME_RP, toClientData,user);
        
        TITGameInfo gameInfo=TITGameInfoFactory.covertGameIdToGameInfo(gameId);
        
        int playType=room.getDefaultRoomInfo().getPlayType();
        switch(playType)
        {
            case 1:
                room.getSameChoiceGameContext().getCurrentRoundContext().
                                                        setGameInfo(gameInfo);
                break;
            case 2:
                int choiceGameRoomUserId=
                                    user.getDefaultUserInfo().getRoomUserId();
                int wasChoicedGameUserRoomUserId=room.getForChoiceGameContext().
                    getCurrentRoundContext().getWasChoiceGameRoomUserId
                                                        (choiceGameRoomUserId);
                ForChoiceUserRoundContext userRoundContext=room.
                    getForChoiceGameContext().getCurrentRoundContext().
                            getUserRoundContext(wasChoicedGameUserRoomUserId);
                userRoundContext.setGameInfo(gameInfo);
                break;
        }
    }

    @Override
    protected void changeUserStatusBegin() 
    {
    }

    @Override
    protected void changeUserStatusEnd() 
    {
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
    }

    @Override
    protected boolean checkUserStatus() 
    {
        switch (user.getStatus()) 
        {
            case PLAYING_GAME:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRoomStatus() 
    {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case PLAYING_GAME:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        Integer gameId = fromClientData.getInt(KJS.GAME_ID);
        String gameName = fromClientData.getUtfString(KJS.GAME_NAME);
        if (gameId == null) 
            return false;
        if (gameName == null) 
            return false;
        return true;
    }
}

