package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import gameserver.tithandler.TITRoomEventHanler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class SpectatorToPlayerEventHandler extends TITRoomEventHanler
{
    @Override
    protected void titHandleServerEvent() throws SFSException 
    {
        reloadRoomInfo();
        reloadPlayerList();
    }
    
    public void reloadRoomInfo() 
    {
        SFSObject roomInfo=new SFSObject();
        room.getDefaultRoomInfo().getPublicInfo(roomInfo);
        send(CMDNF.ROOMINFO_NF, roomInfo,
                                    room.getDefaultRoomInfo().getPlayerArr());
    }

    public void reloadPlayerList() 
    {
        SFSArray userInfoArray = room.getDefaultRoomInfo().
                                                        getAllUserPublicInfo();
        SFSObject toClientData = new SFSObject();
        toClientData.putSFSArray(KJS.ARRAY, (ISFSArray)userInfoArray);
        send(CMDNF.ALLPLAYERINFO_NF,toClientData,room.getDefaultRoomInfo().
                                                                getPlayerArr());
    }
    
    @Override
    protected boolean checkUserStatus() 
    {
        return true;
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

    @Override
    protected void changeUserStatusBegin() 
    {
        
    }

    @Override
    protected void changeUserStatusEnd() 
    {
        
    }
    
    @Override
    protected boolean checkRoomStatus() 
    {
        return true;
    }

    @Override
    protected void changeRoomStatusBegin() 
    {
        
    }

    @Override
    protected void changeRoomStatusEnd() 
    {
        
    }
    
}
