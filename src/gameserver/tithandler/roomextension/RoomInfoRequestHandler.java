package gameserver.tithandler.roomextension;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUserStatus;
import gameserver.tithandler.TITRoomRequestHandler;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.CMDRP;
import gameserver.titconfig.KJS;

public class RoomInfoRequestHandler extends TITRoomRequestHandler 
{
    @Override
    protected void titHandClientRequest() 
    {
        SFSObject toClientData = new SFSObject();
        toClientData.putBool(KJS.SUCESS, true);
        send(CMDRP.ROOMINFO_RP, toClientData,user);
        SFSObject roomInfo=new SFSObject();
        room.getDefaultRoomInfo().getPublicInfo(roomInfo);
        send(CMDNF.ROOMINFO_NF, roomInfo,user);
    }

    @Override
    protected void changeUserStatusBegin() {
    }

    @Override
    protected void changeUserStatusEnd() {
    }

    @Override
    protected void changeRoomStatusBegin() {
    }

    @Override
    protected void changeRoomStatusEnd() {
    }

    @Override
    protected boolean checkUserStatus() {
        TITUserStatus userStatus = this.user.getStatus();
        switch (userStatus) {
            case WAITING_INROOM: {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean checkRoomStatus() {
        switch (room.getDefaultRoomInfo().getStatus()) 
        {
            case WAITING: 
            case REFESHING:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected boolean checkRequestData() 
    {
        return true;
    }

}

