/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonObject
 */
package gameserver.titfacebook;

import com.google.gson.JsonObject;

public interface ITITFacebook {
    public JsonObject checkAccessToken(JsonObject var1);

    public String getAccessToken();

    public JsonObject getUserInfo(JsonObject var1);

    public JsonObject getFriends();
}

