package gameserver.titfacebook;

import gameserver.titfacebook.ITITFacebook;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.User;
import java.util.List;
import gameserver.titconfig.TITFilter;
import gameserver.titconfig.FacebookConfig;
import gameserver.titconfig.KJS;

public class TITFacebook implements ITITFacebook 
{
    private FacebookClient facebookClient;
    private String accessToken;

    @Override
    public String getAccessToken() 
    {
        return this.accessToken;
    }

    public TITFacebook(String accessToken) 
    {
        this.accessToken = accessToken;
        String appSecret = FacebookConfig.getAppSecret();
        this.facebookClient = new DefaultFacebookClient(accessToken,appSecret,
                                                            Version.VERSION_2_4);
    }

    @Override
    public JsonObject checkAccessToken(JsonObject jsonData) 
    {
        this.getUserInfo(jsonData);
        if (jsonData.get(KJS.SUCESS).getAsBoolean()) 
        {
            jsonData.addProperty(KJS.RESULT,true);
        } 
        else 
        {
            jsonData.addProperty(KJS.RESULT,false);
        }
        return jsonData;
    }

    @Override
    public JsonObject getUserInfo(JsonObject jsonData) 
    {
        try 
        {
            String fields = "id,name,first_name,last_name,email,birthday,gender,link,picture";
            User user = facebookClient.fetchObject("me",User.class,Parameter.with((String)"fields",fields));
            String facebookId = user.getId();
            String fullName = user.getName();
            String firstName = user.getFirstName();
            String lastName = user.getLastName();
            fullName = TITFilter.nameFilter(fullName, firstName, lastName);
            String email = user.getEmail();
            String birthDay = user.getBirthday();
            String gender = user.getGender();
            String facebookUrl = user.getLink();
            String avatarUrl = user.getPicture().getUrl();
            if (facebookId == null) 
            {
                jsonData.addProperty(KJS.SUCESS,false);
                return jsonData;
            }
            jsonData.addProperty(KJS.FACEBOOK_ID, facebookId);
            if (fullName == null) 
            {
                fullName = "";
            }
            jsonData.addProperty(KJS.FULL_NAME, fullName);
            if (email == null) 
            {
                email = "";
            }
            jsonData.addProperty(KJS.EMAIL, email);
            if (birthDay == null) 
            {
                birthDay = "";
            }
            jsonData.addProperty(KJS.BIRTH_DAY,birthDay);
            if (gender == null) {
                gender = "";
            }
            jsonData.addProperty(KJS.GENDER,gender);
            if (facebookUrl == null) 
            {
                facebookUrl = "";
            }
            jsonData.addProperty(KJS.FACEBOOK_URL, facebookUrl);
            if (avatarUrl == null)
            {
                avatarUrl = "";
            }
            jsonData.addProperty(KJS.AVATAR_URL, avatarUrl);
            
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }
    
    @Override
    public JsonObject getFriends() 
    {
        JsonObject jsonObj = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        String fields = "id,name,first_name,last_name,email,birthday,gender,link,picture";
        int limit = FacebookConfig.getLimitFriends();
        try 
        {
            Connection friends = facebookClient.fetchConnection("me/friends",
            User.class, new Parameter[]{Parameter.with("limit",limit),
                                            Parameter.with("fields", fields)});
            List listFriends = friends.getData();
            for (int i = 0; i < listFriends.size(); ++i) 
            {
                User user = (User)listFriends.get(i);
                JsonObject tmp = new JsonObject();
                String facebookId = user.getId();
                String fullName = user.getName();
                String firstName = user.getFirstName();
                String lastName = user.getLastName();
                fullName = TITFilter.nameFilter(fullName, firstName, lastName);
                String email = user.getEmail();
                String birthDay = user.getBirthday();
                String gender = user.getGender();
                String facebookUrl = user.getLink();
                String avatarUrl = user.getPicture().getUrl();
                
                if (facebookId == null) 
                {
                    jsonObj.addProperty(KJS.SUCESS,false);
                    return jsonObj;
                }
                tmp.addProperty(KJS.FACEBOOK_ID, facebookId);
                if (fullName == null) 
                {
                    fullName = "";
                }
                tmp.addProperty(KJS.FULL_NAME, fullName);
                if (email == null) 
                {
                    email = "";
                }
                tmp.addProperty(KJS.EMAIL, email);
                if (birthDay == null) 
                {
                    birthDay = "";
                }
                tmp.addProperty(KJS.BIRTH_DAY,birthDay);
                if (gender == null) 
                {
                    gender = "";
                }
                tmp.addProperty(KJS.GENDER,gender);
                if (facebookUrl == null) 
                {
                    facebookUrl = "";
                }
                tmp.addProperty(KJS.FACEBOOK_URL, facebookUrl);
                if (avatarUrl == null)
                {
                    avatarUrl = "";
                }
                tmp.addProperty(KJS.AVATAR_URL, avatarUrl);
                
                jsonArray.add(tmp);
            }
            
            jsonObj.add(KJS.ARRAY,jsonArray);
            jsonObj.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonObj.addProperty(KJS.SUCESS,false);
            jsonObj.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonObj;
    }
}

