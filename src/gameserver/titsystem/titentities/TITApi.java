package gameserver.titsystem.titentities;

import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.SFSApi;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;
import com.smartfoxserver.v2.exceptions.SFSRoomException;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.ServerConfig;
import gameserver.titgameinfo.TITRoomNameFactory;
import gameserver.tithandler.zoneextension.ZoneExtension;

public class TITApi 
{
    private SFSApi sfsApi;
    public TITApi(SFSApi sfsApi) 
    {
        this.sfsApi = sfsApi;
    }

    public void removeRoom(TITRoom room) 
    {
        String roomName = room.getDefaultRoomInfo().getName();
        sfsApi.removeRoom((Room)room.getSFSRoom(), true, true);
        String strNameId;
        Integer nameId;
        switch (room.getDefaultRoomInfo().getType()) 
        {
            case 1: 
                strNameId = roomName.substring
                                        ("NRoom".length(),roomName.length());
                nameId = new Integer(strNameId);
                TITRoomNameFactory.removeNormalRoomNameId(nameId);
                break;
            case 2:
                strNameId = roomName.substring
                                        ("CRoom".length(),roomName.length());
                nameId = new Integer(strNameId);
                TITRoomNameFactory.removeChallengeRoomNameId(nameId);
                break;
        }
    }

    public boolean joinRoom(TITUser user, TITRoom room, String roomPass) 
    {
        SFSUser sfsUser = user.getSFSUser();
        SFSRoom sfsRoom = room.getSFSRoom();
        try 
        {
            this.getSFSApi().joinRoom(sfsUser,sfsRoom,roomPass,
                                                        false,null,true,true);
            return true;
        }
        catch (SFSJoinRoomException ex) 
        {
            return false;
        }
    }

    public void exitRoom(TITUser user, TITRoom room) 
    {
        SFSUser sfsUser = user.getSFSUser();
        SFSRoom sfsRoom = room.getSFSRoom();
        getSFSApi().leaveRoom(sfsUser,sfsRoom, true, true);
    }

    public TITRoom createRoom(int roomType, String roomPass,
                                                    int playerMax,TITUser user) 
    {
        TITZone zone = user.getDefaultUserInfo().getZone();
        CreateRoomSettings settings = new CreateRoomSettings();
        settings.setMaxUsers(playerMax);
        if (!roomPass.equals(""))
            settings.setPassword(roomPass);
        settings.setGame(true);
        settings.setMaxSpectators(playerMax);
        Integer roomNameId;
        String nameRoom;
        switch (roomType) 
        {
            case 1: 
                roomNameId = TITRoomNameFactory.getNewNornalRoomNameId();
                nameRoom = "NRoom" + roomNameId.toString();
                settings.setName(nameRoom);
                settings.setGroupId("NORMAL_ROOM_GROUP");
                break;
            case 2:
                roomNameId = TITRoomNameFactory.getNewChallengeRoomNameId();
                nameRoom = "CRoom" + roomNameId.toString();
                settings.setName(nameRoom);
                settings.setGroupId("CHALLENGE_ROOM_GROUP");
                break;
        }
        settings.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);
        settings.setExtension(this.createExtension(roomType));
        try {
            SFSRoom sfsRoom = (SFSRoom)getSFSApi().createRoom(zone.getSFSZone(), 
                        settings, (User)user.getSFSUser(), true,null,true,true);
            TITRoom titRoom = new TITRoom(sfsRoom);
            titRoom.initDefaultRoomInfo();
            titRoom.getDefaultRoomInfo().setOwner(user);
            titRoom.setAutoRemoveMode();
            setRoomFlag(titRoom);
            return titRoom;
        }
        catch (SFSCreateRoomException ex) 
        {
            return null;
        }
    }

    public void logout(TITUser user) 
    {
        sfsApi.logout(user.getSFSUser());
    }

    public void disconnect(TITUser user) 
    {
        sfsApi.disconnectUser(user.getSFSUser());
        sfsApi.disconnect(user.getDefaultUserInfo().getSession());
    }

    public boolean playerToSpectator(TITUser user, TITRoom room) 
    {
        SFSUser sfsUser = user.getSFSUser();
        SFSRoom sfsRoom = room.getSFSRoom();
        try {
            this.sfsApi.playerToSpectator(sfsUser,sfsRoom,true,true);
            return true;
        }
        catch (SFSRoomException ex) {
            return false;
        }
    }

    public boolean spectatorToPlayer(TITUser user, TITRoom room) 
    {
        SFSUser sfsUser = user.getSFSUser();
        SFSRoom sfsRoom = room.getSFSRoom();
        try 
        {
            sfsApi.spectatorToPlayer(sfsUser,sfsRoom, true, true);
            return true;
        }
        catch (SFSRoomException ex) 
        {
            return false;
        }
    }

    private CreateRoomSettings.RoomExtensionSettings createExtension(int roomType) 
    {
        CreateRoomSettings.RoomExtensionSettings extension;
        switch (roomType) 
        {
            case 1:
                extension=new CreateRoomSettings.RoomExtensionSettings("zone",
                                            ServerConfig.NORMALEXTENSION_CLASS);
                break;
            case 2:
            default:
                extension=new CreateRoomSettings.RoomExtensionSettings("zone",
                                        ServerConfig.CHALLENGEEXTENSION_CLASS);
                break;
        }
        return extension;
    }

    public TITUser getUserBySystemUserId(int systemUserId) 
    {
        SFSUser sfsUser = (SFSUser) sfsApi.getUserById(systemUserId);
        if(sfsUser==null)
            return null;
        else
            return new TITUser(sfsUser);
    }

    private void setRoomFlag(TITRoom room) 
    {
        
    }

    private SFSApi getSFSApi() 
    {
        return sfsApi;
    }
    
    public static TITApi getDefaultApi()
    {
        return new TITApi((SFSApi)ZoneExtension.
                                            getDefautZoneExtension().getApi());
    }
}

