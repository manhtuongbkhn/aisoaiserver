package gameserver.titsystem.titentities;

import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.room.TITRoom;
import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.SFSZone;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import gameserver.titconfig.ServerConfig;
import gameserver.tithandler.zoneextension.ZoneExtension;

public class TITZone 
{
    private SFSZone sfsZone;

    public TITZone(SFSZone sfsZone) 
    {
        this.sfsZone = sfsZone;
    }

    public ArrayList<TITRoom> getRoomArr(String groupId) 
    {
        ArrayList roomArr = new ArrayList();
        List roomList = sfsZone.getRoomListFromGroup(groupId);

        for(int i = 0; i < roomList.size(); ++i) 
        {
            SFSRoom sfsRoom = (SFSRoom)roomList.get(i);
            TITRoom titRoom = new TITRoom(sfsRoom);
            roomArr.add(titRoom);
        }

        return roomArr;
    }

    public ArrayList<TITUser> getUserArr() 
    {
        ArrayList userArr = new ArrayList();
        Iterator userIterator = sfsZone.getUserList().iterator();

        while(userIterator.hasNext()) 
        {
            SFSUser sfsUser = (SFSUser)userIterator.next();
            TITUser user = new TITUser(sfsUser);
            userArr.add(user);
        }

        return userArr;
    }

    public SFSArray getSendClientRoomInfoSFFArr() 
    {
        SFSArray sfsArray = new SFSArray();
        ArrayList roomArr = this.getRoomArr(ServerConfig.NORMALROOM_GROUP);

        for(int i = 0; i < roomArr.size(); ++i) 
        {
            TITRoom room = (TITRoom)roomArr.get(i);
            SFSObject roomInfo=new SFSObject();
            room.getDefaultRoomInfo().getSendClientInfo(roomInfo);
            sfsArray.addSFSObject(roomInfo);
        }

        return sfsArray;
    }

    public SFSZone getSFSZone() 
    {
        return this.sfsZone;
    }

    public TITRoom getRoomById(int roomId) 
    {
        SFSRoom sfsRoom = (SFSRoom)sfsZone.getRoomById(roomId);
        if(sfsRoom==null)
            return null;
        else
            return new TITRoom(sfsRoom);
    }

    public TITRoom getRoomByName(String roomName) 
    {
        SFSRoom sfsRoom = (SFSRoom)this.sfsZone.getRoomByName(roomName);
        return sfsRoom != null?new TITRoom(sfsRoom):null;
    }
    
    public static TITZone getDefaultZone()
    {
        return new TITZone((SFSZone)ZoneExtension.
                                    getDefautZoneExtension().getParentZone());
    }
}
