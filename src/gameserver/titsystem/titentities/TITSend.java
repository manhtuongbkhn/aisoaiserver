
package gameserver.titsystem.titentities;

import gameserver.titconfig.TITFunction;
import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.TITZoneExtension;
import gameserver.titconfig.KJS;
import gameserver.tithandler.zoneextension.ZoneExtension;

public class TITSend 
{
    private TITZoneExtension zoneExtension;
    
    private TITSend(TITZoneExtension zoneExtension)
    {
        this.zoneExtension=zoneExtension;
    }
    
    public void send(String cmd,SFSObject dataToClient,
                                                    ArrayList<TITUser> userArr) 
    {
        for (int i = 0; i < userArr.size(); ++i) 
        {
            TITUser titUser = userArr.get(i);
            send(cmd, dataToClient, titUser);
        }
    }

    public void send(String cmd, SFSObject toClientData, TITUser user) 
    {
        SFSObject sfsObject = new SFSObject();
        sfsObject.putUtfString(KJS.DATA,toClientData.toJson());
        zoneExtension.send(cmd,sfsObject,user.getSFSUser());
    }

    public void send(String cmd,JsonObject toClientData,
                                                    ArrayList<TITUser> userArr) 
    {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        send(cmd, sfsObject, userArr);
    }

    public void send(String cmd, JsonObject toClientData, TITUser user) 
    {
        SFSObject sfsObject = TITFunction.covertToSFSObject(toClientData);
        send(cmd, sfsObject, user);
    }
    
    public static TITSend getDefaultSend()
    {
        return new TITSend(ZoneExtension.getDefautZoneExtension());
    }
}
