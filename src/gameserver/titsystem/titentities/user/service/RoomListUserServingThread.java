package gameserver.titsystem.titentities.user.service;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class RoomListUserServingThread extends TITUserServingThread
{
    public RoomListUserServingThread(TITUser user) 
    {
        super(user);
    }
    
    @Override
    public void run()
    {
        serving=true;
        stopFlag=false;
        while(!stopFlag) 
        {
            SFSArray sfsArray = user.getDefaultUserInfo().getZone().
                                                getSendClientRoomInfoSFFArr();
            SFSObject toClientData = new SFSObject();
            toClientData.putSFSArray(KJS.ARRAY,sfsArray);
            send.send(CMDNF.RELOADROOMLIST_NF,toClientData,user);
            try 
            {
                Thread.sleep(ServerConfig.RELOAD_ROOMLIST_MILLIS);
            }
            catch (InterruptedException ex) {}
        }
        serving=false;
    }
}
