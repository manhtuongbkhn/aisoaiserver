package gameserver.titsystem.titentities.user.service;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.user.TITFriend;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class InvitationUserServingThread extends TITUserServingThread
{
    private TITRoom room;
    
    public InvitationUserServingThread(TITUser user,TITRoom room) 
    {
        super(user);
        this.room=room;
    }
    
    @Override
    public void run()
    {
        serving=true;
        stopFlag=false;
        while (!stopFlag) 
        {
            ArrayList<TITFriend> friendArr = user.getFriendManager().
                                                        getCanInviteFriendArr();
            insideRoomUserFilter(friendArr);
            invitationUserFilter(friendArr);
            
            SFSArray sfsArray = new SFSArray();
            for (int i = 0; i < friendArr.size(); ++i) 
            {
                TITFriend titFriend = friendArr.get(i);
                SFSObject sfsObject = titFriend.getPublicInfo();
                sfsArray.addSFSObject((ISFSObject)sfsObject);
            }
            
            SFSObject toClientData = new SFSObject();
            toClientData.putSFSArray(KJS.ARRAY,sfsArray);
            send.send(CMDNF.RELOADINVITATIONFRIENDLIST_NF, toClientData,user);
            try 
            {
                Thread.sleep(ServerConfig.RELOAD_INVITATION_FRIENDLIST_MILLIS);
            }
            catch (InterruptedException ex) {}
        }
        serving=false;
    }
    
     private ArrayList<TITFriend> insideRoomUserFilter
                                                (ArrayList<TITFriend> friendArr) 
    {
        for (int i = 0; i < friendArr.size();i++) 
        {
            TITFriend titFriend = friendArr.get(i);
            TITUser titUser = titFriend.getUser();
            if (room.getDefaultRoomInfo().checkUserInRoom(titUser))
                friendArr.remove(titUser);
        }
        return friendArr;
    }

    private ArrayList<TITFriend> invitationUserFilter(ArrayList<TITFriend> friendArr) 
    {
        for (int i = 0; i < friendArr.size();i++) 
        {
            TITFriend friend = friendArr.get(i);
            TITUser titUser = friend.getUser();
            if (room.getInvitationManager().isUserWasInvited(titUser))
                friendArr.remove(friend);
        }
        return friendArr;
    }
}
