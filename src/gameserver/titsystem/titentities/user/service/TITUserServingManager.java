package gameserver.titsystem.titentities.user.service;

import gameserver.titsystem.titentities.user.TITUser;

public class TITUserServingManager 
{
    private RoomListUserServing roomListUserServing;
    private FriendListUserServing friendListUserServing;
    private InvitationUserServing invitationUserServing;
    private TopPlayerUserServing topPlayerUserServing;
    
    public TITUserServingManager(TITUser user)
    {
        roomListUserServing=new RoomListUserServing(user);
        friendListUserServing=new FriendListUserServing(user);
        invitationUserServing=new InvitationUserServing(user);
        topPlayerUserServing=new TopPlayerUserServing(user);
    }

    public RoomListUserServing getRoomListUserServing() 
    {
        return roomListUserServing;
    }

    public FriendListUserServing getFriendListUserServing() 
    {
        return friendListUserServing;
    }

    public InvitationUserServing getInvitationUserServing() 
    {
        return invitationUserServing;
    }

    public TopPlayerUserServing getTopPlayerUserServing() 
    {
        return topPlayerUserServing;
    }
    
    public void offAllServing()
    {
        roomListUserServing.offServing();
        friendListUserServing.offServing();
        invitationUserServing.offServing();
        topPlayerUserServing.offServing();
    }
}

