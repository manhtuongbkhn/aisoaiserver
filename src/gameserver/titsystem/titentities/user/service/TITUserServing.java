package gameserver.titsystem.titentities.user.service;

import com.google.gson.JsonObject;
import gameserver.titsystem.titentities.user.TITUser;

abstract public class TITUserServing 
{
    protected TITUser user;
    protected JsonObject option;
    
    public TITUserServing(TITUser user)
    {
        this.user=user;
    }
    
    abstract public void onServing();
    
    abstract public void offServing();

    public JsonObject getOption() 
    {
        return option;
    }

    public void setOption(JsonObject option) 
    {
        this.option = option;
    }
    
    
}
