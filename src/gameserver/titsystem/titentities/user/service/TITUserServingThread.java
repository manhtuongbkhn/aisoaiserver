package gameserver.titsystem.titentities.user.service;

import gameserver.titsystem.titentities.TITApi;
import gameserver.titsystem.titentities.TITSend;
import gameserver.titsystem.titentities.user.TITUser;

public class TITUserServingThread extends Thread
{
    protected TITUser user;
    protected TITApi api;
    protected TITSend send;
    
    protected boolean stopFlag;
    protected boolean serving;
    
    public TITUserServingThread(TITUser user)
    {
        this.user=user;
        this.api=TITApi.getDefaultApi();
        this.send=TITSend.getDefaultSend();
    }

    public boolean isStop() 
    {
        return stopFlag;
    }

    public void setStop(boolean stop) 
    {
        this.stopFlag=stop;
    }
    
    public boolean isServing()
    {
        return serving;
    }
}
