package gameserver.titsystem.titentities.user.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import gameserver.titsystem.titservice.TopPlayerService;
import gameserver.titsystem.titentities.TITSend;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;

public class TopPlayerUserServing extends TITUserServing
{
    public TopPlayerUserServing(TITUser user) 
    {
        super(user);
    }

    @Override
    public void onServing() 
    {
        JsonArray topPlayerJsonArray=TopPlayerService.getTopPlayerJsonArray();
        JsonObject toClientData=new JsonObject();
        toClientData.add(KJS.ARRAY,topPlayerJsonArray);
        TITSend send=TITSend.getDefaultSend();
        send.send(CMDNF.RELOADTOPPLAYER_NF,toClientData,user);
        TopPlayerService.register(user);
    }

    @Override
    public void offServing() 
    {
        TopPlayerService.unregister(user);
    }
}
