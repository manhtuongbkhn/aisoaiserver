package gameserver.titsystem.titentities.user.service;

import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;

public class InvitationUserServing extends TITUserServing
{
    protected TITUserServingThread thread;
    private TITRoom room;
    
    public InvitationUserServing(TITUser user) 
    {
        super(user);
    }

    @Override
    public void onServing() 
    {
        if(thread==null)
        {
            thread=new InvitationUserServingThread(user,room);
            thread.start();
        }
        else
        {
            if(!thread.isServing())
            {
                thread=new InvitationUserServingThread(user,room);
                thread.start();
            }
            else
                thread.setStop(false);
        }
    }

    @Override
    public void offServing() 
    {
        if(thread!=null)
            thread.setStop(true);
    }

    public void setRoom(TITRoom room) 
    {
        this.room = room;
    }
}
