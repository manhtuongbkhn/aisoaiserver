package gameserver.titsystem.titentities.user.service;

import gameserver.titsystem.titentities.user.TITUser;

public class FriendListUserServing extends TITUserServing
{
    protected TITUserServingThread thread;
        
    public FriendListUserServing(TITUser user) 
    {
        super(user);
    } 

    @Override
    public void onServing() 
    {
        if(thread==null)
        {
            thread=new FriendListUserServingThread(user);
            thread.start();
        }
        else
        {
            if(!thread.isServing())
            {
                thread=new FriendListUserServingThread(user);
                thread.start();
            }
            else
                thread.setStop(false);
        }
    }

    @Override
    public void offServing() 
    {
        if(thread!=null)
            thread.setStop(true);
    }
}
