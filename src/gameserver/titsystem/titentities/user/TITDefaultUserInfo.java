package gameserver.titsystem.titentities.user;

import com.smartfoxserver.bitswarm.sessions.Session;
import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.SFSZone;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titsystem.titentities.TITZone;

public class TITDefaultUserInfo 
{
    private int roomUserId;
    private Integer backupRoomId;
    private SFSUser sfsUser;
    
    public TITDefaultUserInfo(SFSUser sfsUser)
    {
        this.sfsUser=sfsUser;
    }

    public int getRoomUserId() 
    {
        return roomUserId;
    }

    public void setRoomUserId(int roomUserId) 
    {
        this.roomUserId = roomUserId;
    }
    
    public boolean isOnline() 
    {
        return sfsUser.isConnected();
    }
    
    public void setBackupRoomId(Integer roomId) 
    {
        this.backupRoomId=roomId;
    }

    public Integer getBackupRoomId() 
    {
        return backupRoomId;
    }
    
    public TITRoom getRoom() 
    {
        SFSRoom sfsRoom = (SFSRoom)sfsUser.getLastJoinedRoom();
        if (sfsRoom == null) 
            return null;
        return new TITRoom(sfsRoom);
    }

    public boolean inRoom() 
    {
        
        if (getRoom()== null) 
            return false;
        if (getRoom().getSFSRoom() == null) 
            return false;
        return true;
    }

    /*
    public ArrayList<TITRoom> getJoinedRoom() 
    {
        List roomList = sfsUser.getJoinedRooms();
        ArrayList<TITRoom> roomArr = new ArrayList<TITRoom>();
        for (int i=0;i<roomList.size();i++) 
        {
            SFSRoom sfsRoom = (SFSRoom)roomList.get(i);
            TITRoom room = new TITRoom(sfsRoom);
            roomArr.add(room);
        }
        return roomArr;
    }
    */
    
    public int getSystemUserId() 
    {
        return sfsUser.getId();
    }
    
    public boolean isOwner() 
    {
        TITRoom room=getRoom();
        if(room==null)
        {
            return false;
        }
        else
        {
            TITUser user=new TITUser(sfsUser);
            return room.getDefaultRoomInfo().isOwner(user);
        }   
    }
    
    public boolean isPlayer() 
    {
        TITRoom room = getRoom();
        if (room == null)
            return false;
        SFSRoom sfsRooom = room.getSFSRoom();
        return sfsUser.isPlayer(sfsRooom);
    }

    public boolean isSpectator() 
    {
        TITRoom room = getRoom();
        if (room == null)
            return false;
        SFSRoom sfsRooom = room.getSFSRoom();
        return sfsUser.isSpectator(sfsRooom);
    }
    
    public TITZone getZone() 
    {
        SFSZone sfsZone = (SFSZone)sfsUser.getZone();
        return new TITZone(sfsZone);
    }

    public Session getSession() 
    {
        return (Session)sfsUser.getSession();
    }
}
