package gameserver.titsystem.titentities.user;

import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import gameserver.titsystem.titentities.user.service.TITUserServingManager;
import gameserver.titconfig.KJS;

public class TITUser 
{
    private SFSUser sfsUser;

    public TITUser(SFSUser sfsUser) 
    {
        this.sfsUser = sfsUser;
    }

    public void initUserProfile()
    {
        TITUserProfile userProfile=new TITUserProfile();
        sfsUser.setProperty(KJS.USER_PROFILE,userProfile);
    }
    
    public TITUserProfile getUserProfile()
    {
        return (TITUserProfile)sfsUser.getProperty(KJS.USER_PROFILE);
    }
    
    public void destroyUserProfile()
    {
        sfsUser.removeProperty(KJS.USER_PROFILE);
    }
    
    public void initGameUserInfo()
    {
        TITGameUserInfo userGameInfo=new TITGameUserInfo();
        sfsUser.setProperty(KJS.GAME_USERINFO,userGameInfo);
    }
    
    public TITGameUserInfo getGameUserInfo()
    {
        return (TITGameUserInfo)sfsUser.getProperty(KJS.GAME_USERINFO);
    }
    
    public void destroyGameUserInfo()
    {
        sfsUser.removeProperty(KJS.GAME_USERINFO);
    }
    
    public void initDefaultUserInfo()
    {
        TITDefaultUserInfo defaultUserInfo=new TITDefaultUserInfo(sfsUser);
        sfsUser.setProperty(KJS.DEFAULT_USERINFO,defaultUserInfo);
    }
    
    public TITDefaultUserInfo getDefaultUserInfo()
    {
        return (TITDefaultUserInfo)sfsUser.getProperty(KJS.DEFAULT_USERINFO);
    }
    
    public void destroyDefaultUserInfo()
    {
        sfsUser.removeProperty(KJS.DEFAULT_USERINFO);
    }
    
    public void initUserServingManager() 
    {
        TITUserServingManager servingManager = new TITUserServingManager(this);
        sfsUser.setProperty(KJS.USER_SERVING_MANAGER,servingManager);
    }

    public TITUserServingManager getUserServingManager() 
    {
        return (TITUserServingManager)sfsUser.getProperty
                                                (KJS.USER_SERVING_MANAGER);
    }
    
    public void destroyServingManager()
    {
        sfsUser.removeProperty(KJS.USER_SERVING_MANAGER);
    }
    
    public void initFriendManager() 
    {
        TITFriendManager friendManager = new TITFriendManager();
        sfsUser.setProperty(KJS.FRIEND_MANAGER,friendManager);
    }

    public TITFriendManager getFriendManager()
    {
        return (TITFriendManager)sfsUser.getProperty(KJS.FRIEND_MANAGER);
    }

    public void destroyFriendManager()
    {
        sfsUser.removeProperty(KJS.FRIEND_MANAGER);
    }
    
    public void initChallengeInfo()
    {
        TITChallengeUserInfo challengeUserInfo=new TITChallengeUserInfo();
        sfsUser.setProperty(KJS.CHALLENGE_USER_INFO,challengeUserInfo);
    }
    
    public TITChallengeUserInfo getChallengeUserInfo()
    {
        return (TITChallengeUserInfo) 
                                sfsUser.getProperty(KJS.CHALLENGE_USER_INFO);
    }
    
    public void destroyChallengeUserInfo()
    {
        sfsUser.removeProperty(KJS.CHALLENGE_USER_INFO);
    }
    
    public SFSObject getPublicInfo() 
    {
        SFSObject userInfo = new SFSObject();
        userInfo.putBool(KJS.IS_OWNER,getDefaultUserInfo().isOwner());
        userInfo.putInt(KJS.SYSTEM_USER_ID,getDefaultUserInfo().
                                                            getSystemUserId());
        getUserProfile().getPublicInfo(userInfo);
        getGameUserInfo().getPublicInfo(userInfo);
        return userInfo;
    }
    
    public SFSUser getSFSUser() 
    {
        return sfsUser;
    }
    
    @Override
    public boolean equals(Object object)
    {
        try
        {
            TITUser otherUser=(TITUser) object;
            if (otherUser == null)
                return false;
            boolean b1 = getDefaultUserInfo().getSystemUserId()==otherUser.
                                        getDefaultUserInfo().getSystemUserId();
            boolean b2 = getUserProfile().getUserId().equals
                                    (otherUser.getUserProfile().getUserId());
            boolean b3 = getUserProfile().getFacebookId().equals
                                (otherUser.getUserProfile().getFacebookId());
            return b1 && b2 && b3;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    
    public boolean setStatus(TITUserStatus userStatus) 
    {
        String userStatusStr = userStatus.toString();
        SFSUserVariable uv = new SFSUserVariable(KJS.USER_STATUS,userStatusStr);
        return this.setVariable(uv);
    }

    public TITUserStatus getStatus() 
    {
        SFSUserVariable uv = (SFSUserVariable)
                                        sfsUser.getVariable(KJS.USER_STATUS);
        if (uv == null)
            return TITUserStatus.NULL;
        String userStatusStr = uv.getStringValue();
        return TITUserStatus.valueOf(userStatusStr);
    }
    
    private boolean setVariable(SFSUserVariable uv) 
    {
        try 
        {
            sfsUser.setVariable(uv);
            return true;
        }
        catch (SFSVariableException ex) 
        {
            return false;
        }
    }
}

