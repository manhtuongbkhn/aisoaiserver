package gameserver.titsystem.titentities.user;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;

public class TITFriendManager 
{
    private NotSyncTITFriendManager notSyncTITFriendManager;
    
    public TITFriendManager()
    {
        notSyncTITFriendManager=new NotSyncTITFriendManager();
    }

    public void addFriend(TITFriend friend) 
    {
        this.notSyncTITFriendManagerMethod("addFriend",friend);
    }

    public void online(TITUser friendUser) 
    {
        notSyncTITFriendManagerMethod("online",friendUser);
    }

    public void offline(TITUser friendUser) 
    {
        notSyncTITFriendManagerMethod("offline",friendUser);
    }

    public boolean isFriend(TITUser user)
    {
        return (boolean)notSyncTITFriendManagerMethod("isFriend",user);
    }
    
    public ArrayList<TITFriend> getFriendArr() 
    {
        return (ArrayList)notSyncTITFriendManagerMethod("getFriendArr",null);
    }

    public SFSArray getFriendSFSArr() 
    {
        return (SFSArray)notSyncTITFriendManagerMethod("getFriendSFSArr", null);
    }

    public ArrayList<TITFriend> getOnlineFriendArr() 
    {
        return (ArrayList)notSyncTITFriendManagerMethod
                                                    ("getOnlineFriendArr",null);
    }

    public ArrayList<TITFriend> getCanInviteFriendArr() 
    {
        return (ArrayList)notSyncTITFriendManagerMethod
                                                ("getCanInviteFriendArr", null);
    }

    public void destroy(TITUser myUser)
    {
        notSyncTITFriendManagerMethod("destroy",myUser);
    }
    
    private synchronized Object notSyncTITFriendManagerMethod
                                            (String methodName,Object param1) 
    {
        TITUser myUser,friendUser;
        switch (methodName) 
        {
            case "addFriend": 
                TITFriend friend=(TITFriend) param1;
                notSyncTITFriendManager.addFriend(friend);
                break;
            case "online":
                friendUser=(TITUser) param1;
                notSyncTITFriendManager.online(friendUser);
                break;
            case "offline":
                friendUser=(TITUser) param1;
                notSyncTITFriendManager.offline(friendUser);
                break;
            case "isFriend":
                TITUser user=(TITUser) param1;
                return notSyncTITFriendManager.isFriend(user);
            case "getFriendArr":
                return notSyncTITFriendManager.getFriendArr();
            case "getFriendSFSArr":
                return notSyncTITFriendManager.getFriendSFSArr();
            case "getOnlineFriendArr":
                return notSyncTITFriendManager.getOnlineFriendArr();
            case "getCanInviteFriendArr":
                return notSyncTITFriendManager.getCanInviteFriendArr();
            case "destroy":
                myUser=(TITUser) param1;
                notSyncTITFriendManager.destroy(myUser);
                break;
        }
        return null;
    }
}

class NotSyncTITFriendManager
{
    private ArrayList<TITFriend> friendArr ;
    
    public NotSyncTITFriendManager()
    {
        friendArr=new ArrayList<TITFriend>();
    }
    
    protected void addFriend(TITFriend friend) 
    {
        friendArr.add(friend);
    }

    protected void online(TITUser friendUser) 
    {
        String userId=friendUser.getUserProfile().getUserId();
        TITFriend friend=getFriendByUserId(userId);
        friend.online(friendUser);
    }

    protected void offline(TITUser friendUser) 
    {
        String userId=friendUser.getUserProfile().getUserId();
        TITFriend friend=getFriendByUserId(userId);
        friend.offline();
    }
    
    protected boolean isFriend(TITUser user)
    {
        String userId=user.getUserProfile().getUserId();
        TITFriend friend=getFriendByUserId(userId);
        if(friend==null)
            return false;
        else
            return true;
    }
    
    protected TITFriend getFriendByUserId(String userId)
    {
        for(int i=0;i<friendArr.size();i++)
        {
            TITFriend friend=friendArr.get(i);
            if(friend.getUserId().equals(userId))
                return friend;
        }
        return null;
    }
    
    protected int getFriendCount()
    {
        return friendArr.size();
    }
    
    protected ArrayList<TITFriend> getFriendArr() 
    {
        return (ArrayList<TITFriend>)friendArr.clone();
    }

    protected SFSArray getFriendSFSArr() 
    {
        SFSArray sfsArray = new SFSArray();
        for (int i = 0; i < friendArr.size();i++) 
        {
            TITFriend titFriend = friendArr.get(i);
            SFSObject sfsObject = titFriend.getPublicInfo();
            sfsArray.addSFSObject((ISFSObject)sfsObject);
        }
        return sfsArray;
    }

    protected ArrayList<TITFriend> getOnlineFriendArr() 
    {
        ArrayList<TITFriend> onlineFriendArr = new ArrayList<TITFriend>();
        for (int i = 0; i < friendArr.size(); ++i) 
        {
            TITFriend titFriend = friendArr.get(i);
            if (titFriend.isOnline()) 
                onlineFriendArr.add(titFriend);
        }
        return (ArrayList<TITFriend>)onlineFriendArr.clone();
    }

    protected ArrayList<TITFriend> getCanInviteFriendArr() 
    {
        ArrayList<TITFriend> canInviteFriendArr = new ArrayList<TITFriend>();
        for (int i=0; i < friendArr.size();i++) 
        {
            TITFriend titFriend = friendArr.get(i);
            if (titFriend.getStatus().equals(TITUserStatus.GETED_PROFILE)) 
                canInviteFriendArr.add(titFriend);
        }
        return (ArrayList<TITFriend>)canInviteFriendArr.clone();
    }
    
    protected void destroy(TITUser myUser)
    {
        for(int i=0;i<friendArr.size();i++)
        {
            TITFriend friend=friendArr.get(i);
            if(friend.isOnline())
            {
                TITUser friendUser=friend.getUser();
                TITFriendManager friendManager=friendUser.getFriendManager();
                friendManager.offline(myUser);
            }
        }
        friendArr=null;
    }
}
