package gameserver.titsystem.titentities.user;

public class TITChallengeUserInfo 
{
    private int playerCount;
    private int playType;

    public int getPlayerCount() 
    {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) 
    {
        this.playerCount = playerCount;
    }

    public int getPlayType() 
    {
        return playType;
    }

    public void setPlayType(int playType) 
    {
        this.playType = playType;
    }
}
