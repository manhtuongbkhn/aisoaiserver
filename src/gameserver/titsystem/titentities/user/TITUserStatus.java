package gameserver.titsystem.titentities.user;

public enum TITUserStatus 
{
    NULL,
    OFFLINE,
    LOGGED,
    GETED_PROFILE,
    WAITING_INQUEUE,
    WAITING_INROOM,
    PLAYING_GAME,
    SCORING_TOTALPOINT,
    IMPORTANT_REQUESTING;
    

    private TITUserStatus() 
    {
        
    }
}

