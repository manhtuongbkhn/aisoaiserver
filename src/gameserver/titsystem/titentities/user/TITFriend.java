package gameserver.titsystem.titentities.user;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.KJS;

public class TITFriend
{
    private NotSyncTITFriend notSyncTITFriend;

    public TITFriend(JsonObject friendInfoJs) 
    {
        notSyncTITFriend=new NotSyncTITFriend(friendInfoJs);
    }   
    
    public void online(TITUser friendUser)
    {
        notSyncTITFriendMethod("online",friendUser);
    }
    
    public void offline()
    {
        notSyncTITFriendMethod("offline",null);
    }
    
    public boolean isOnline()
    {
        return (boolean) notSyncTITFriendMethod("isOnline",null);
    }
    
    public String getUserId()
    {
        return (String) notSyncTITFriendMethod("getUserId",null);
    }
    
    public SFSObject getPublicInfo()
    {
        return (SFSObject) notSyncTITFriendMethod("getPublicInfo",null);
    }
    
    public TITUserStatus getStatus()
    {
        return (TITUserStatus) notSyncTITFriendMethod("getStatus",null);
    }
    
    public TITUser getUser()
    {
        return (TITUser) notSyncTITFriendMethod("getUser",null);
    }
    
    private synchronized Object notSyncTITFriendMethod
                                            (String methodName,Object param1)
    {
        switch(methodName)
        {
            case "online":
                TITUser friendUser=(TITUser) param1;
                notSyncTITFriend.online(friendUser);
                break;
            case "offline":
                notSyncTITFriend.offline();
                break;
            case "isOnline":
                return notSyncTITFriend.isOnline();
            case "getUserId":
                return notSyncTITFriend.getUserId();
            case "getPublicInfo":
                return notSyncTITFriend.getPublicInfo();
            case "getStatus":
                return notSyncTITFriend.getStatus();
            case "getUser":
                return notSyncTITFriend.getUser();
        }
        return null;
    }
}

class NotSyncTITFriend 
{
    private Object data;

    protected NotSyncTITFriend(JsonObject friendInfoJs)
    {
        data=new FriendStaticData(friendInfoJs);
    }

    protected boolean online(TITUser friendUser)
    {
        if(friendUser.getDefaultUserInfo().isOnline())
        {
            data=friendUser;
            return true;
        }
        else
            return false;
    }
    
    protected void offline()
    {
        TITUser friend=(TITUser) data;
        JsonObject friendInfoJs=new JsonObject();
        friend.getUserProfile().getData(friendInfoJs);
        friend.getGameUserInfo().getData(friendInfoJs);
        data=new FriendStaticData(friendInfoJs);
    }
    
    protected boolean isOnline() 
    {
        if (data instanceof FriendStaticData) 
            return false;
        if (data instanceof TITUser)
        {
            return true;
        }
        return false;
    }

    protected TITUserStatus getStatus() 
    {
        if (isOnline())
            return getUser().getStatus();
        return TITUserStatus.OFFLINE;
    }

    protected int getSystemUserId() 
    {
        if (isOnline())
            return getUser().getDefaultUserInfo().getSystemUserId();
        return -1;
    }

    protected String getUserId() 
    {
        if (isOnline()) 
            return getUser().getUserProfile().getUserId();
        return getFriendStaticData().getUserProfile().getUserId();
    }

    protected String getFullName() 
    {
        if (isOnline()) 
            return getUser().getUserProfile().getFullName();
        return getFriendStaticData().getUserProfile().getFullName();
    }

    protected String getAvatarUrl() 
    {
        if (isOnline()) 
            return getUser().getUserProfile().getAvatarUrl();
        return getFriendStaticData().getUserProfile().getAvatarUrl();
    }

    protected String getGender() 
    {
        if (isOnline())
            return getUser().getUserProfile().getGender();
        return getFriendStaticData().getUserProfile().getGender();
    }

    protected String getFacebookId() 
    {
        if (isOnline())
            return getUser().getUserProfile().getFacebookId();
        return getFriendStaticData().getUserProfile().getFacebookId();
    }

    protected String getFacebookUrl() 
    {
        if (isOnline())
            return getUser().getUserProfile().getFacebookUrl();
        return getFriendStaticData().getUserProfile().getFacebookUrl();
    }
    
    protected int getRank()
    {
        if(isOnline())
            return getUser().getGameUserInfo().getRank();
        return getFriendStaticData().getGameUserInfo().getRank();
    }
    
    protected int getRankPoint()
    {
        if(isOnline())
            return getUser().getGameUserInfo().getRankPoint();
        return getFriendStaticData().getGameUserInfo().getRankPoint();
    }
    
    protected int getMatchCount()
    {
        if(isOnline())
            return getUser().getGameUserInfo().getMatchCount();
        return getFriendStaticData().getGameUserInfo().getMatchCount();
    }
    
    protected int getWinPercent()
    {
        if(isOnline())
            return getUser().getGameUserInfo().getWinPercent();
        return getFriendStaticData().getGameUserInfo().getWinPercent();
    }
    
    protected SFSArray getCompetence()
    {
        if(isOnline())
            return getUser().getGameUserInfo().getCompetenceSFSArr();
        SFSArray sfsArray=getFriendStaticData().
                                    getGameUserInfo().getCompetenceSFSArr();
        return sfsArray;
    }
    
    protected SFSObject getPublicInfo() 
    {
        int status;
        SFSObject sfsObject = new SFSObject();
        sfsObject.putInt(KJS.SYSTEM_USER_ID, getSystemUserId());
        sfsObject.putUtfString(KJS.USER_ID,getUserId());
        sfsObject.putUtfString(KJS.FACEBOOK_ID,getFacebookId());
        sfsObject.putUtfString(KJS.FULL_NAME,getFullName());
        sfsObject.putUtfString(KJS.AVATAR_URL,getAvatarUrl());
        sfsObject.putUtfString(KJS.FACEBOOK_URL,getFacebookUrl());
        sfsObject.putUtfString(KJS.GENDER,getGender());
        sfsObject.putInt(KJS.RANK,getRank());
        sfsObject.putInt(KJS.RANK_POINT,getRankPoint());
        sfsObject.putInt(KJS.MATCH_COUNT,getMatchCount());
        sfsObject.putInt(KJS.WIN_PERCENT,getWinPercent());
        sfsObject.putSFSArray(KJS.COMPETENCE,getCompetence());
        
        switch (getStatus()) 
        {
            case OFFLINE: 
            case NULL:
                status = 0;
                break;
            case LOGGED: 
            case GETED_PROFILE:
                status = 1;
                break;
            default:
                status = 2;
        }
        sfsObject.putInt(KJS.USER_STATUS,status);
        return sfsObject;
    }

    protected TITUser getUser() 
    {
        if(isOnline())
        {
            return (TITUser) data;
        }
        else
            return null;
    }

    protected FriendStaticData getFriendStaticData() 
    {
        if(isOnline())
            return null;
        else
            return (FriendStaticData) data;
    }
}

class FriendStaticData
{
    private TITUserProfile userProfile;
    private TITGameUserInfo gameUserInfo;
    
    public FriendStaticData(TITUserProfile userProfile,
                                                TITGameUserInfo gameUserInfo)
    {
        this.userProfile=userProfile;
        this.gameUserInfo=gameUserInfo;
    }
    
    public FriendStaticData(JsonObject jsonObject)
    {
        userProfile=new TITUserProfile();
        gameUserInfo=new TITGameUserInfo();
        userProfile.setData(jsonObject);
        gameUserInfo.setData(jsonObject);
    }

    public TITUserProfile getUserProfile() 
    {
        return userProfile;
    }

    public void setUserProfile(TITUserProfile userProfile) 
    {
        this.userProfile = userProfile;
    }

    public TITGameUserInfo getGameUserInfo() 
    {
        return gameUserInfo;
    }

    public void setGameUserInfo(TITGameUserInfo gameUserInfo) 
    {
        this.gameUserInfo = gameUserInfo;
    }
}

