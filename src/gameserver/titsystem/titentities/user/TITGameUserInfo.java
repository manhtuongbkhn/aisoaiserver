package gameserver.titsystem.titentities.user;

import gameserver.titsystem.titservice.challenge.RankFuction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class TITGameUserInfo 
{
    private int normalMatchCount;
    private int winNormalMatchCount;
    private int rankMatchCount;
    private int winRankMatchCount;
    private int experiencePoint;
    private int rankPoint;
    private int afkMatchCount;
    
    private ArrayList<Integer> competenceArr=new ArrayList<Integer>();
    private ArrayList<GameMaturity> gameMaturityArr=
                                            new ArrayList<GameMaturity>();
    
    public void setData(JsonObject jsonData)
    {
        normalMatchCount=jsonData.get(KJS.NMATCH_COUNT).getAsInt();
        winNormalMatchCount=jsonData.get(KJS.WIN_NMATCH_COUNT).getAsInt();
        rankMatchCount=jsonData.get(KJS.RMATCH_COUNT).getAsInt();
        winRankMatchCount=jsonData.get(KJS.WIN_RMATCH_COUNT).getAsInt();
        experiencePoint=jsonData.get(KJS.EXPERIENCE_POINT).getAsInt();
        rankPoint=jsonData.get(KJS.RANK_POINT).getAsInt();
        afkMatchCount=jsonData.get(KJS.AFKMATCH_COUNT).getAsInt();
        
        competenceArr.add(jsonData.get(KJS.SPEED).getAsInt());
        competenceArr.add(jsonData.get(KJS.OBSERVE).getAsInt());
        competenceArr.add(jsonData.get(KJS.CALCULATE).getAsInt());
        competenceArr.add(jsonData.get(KJS.MEMORY).getAsInt());
        competenceArr.add(jsonData.get(KJS.LOGIC).getAsInt());
        competenceArr.add(jsonData.get(KJS.REFLEX).getAsInt());
        
        JsonArray jsonArray=jsonData.get(KJS.GAME_MATURITY_ARR).getAsJsonArray();
        for(int i=0;i<jsonArray.size();i++)
        {
            JsonObject jsonObject=jsonArray.get(i).getAsJsonObject();
            int gameId=jsonObject.get(KJS.GAME_ID).getAsInt();
            int gameMaturityPoint=jsonObject.get(KJS.GAME_MATURITY).getAsInt();
            GameMaturity gameMaturity=new GameMaturity(gameId,gameMaturityPoint);
            gameMaturityArr.add(gameMaturity);
        }
    }
    
    public int getRank() 
    {
        return RankFuction.getRank(rankPoint);
    }
    
    public int getRankPoint() 
    {
        return rankPoint;
    }

    public void setRankPoint(int rankPoint) 
    {
        this.rankPoint = rankPoint;
    }
    
    
   public ArrayList<Integer> getCompetence() 
    {
        return competenceArr;
    }

    public SFSArray getCompetenceSFSArr() 
    {
        return TITFunction.covertToIntSFSArray(competenceArr);
    }
    
    public JsonArray getCompetenceJsonArr()
    {
        return TITFunction.covertToIntJsonArray(competenceArr);
    }

    public ArrayList<Integer> getGameIdArr() 
    {
        ArrayList<Integer> gameIdArr=new ArrayList<Integer>();
        for(int i=0;i<gameMaturityArr.size();i++)
        {
            int gameId=gameMaturityArr.get(i).getGameId();
            gameIdArr.add(gameId);
        }
        return gameIdArr;
    }

    public SFSArray getGameIdSFSArr() 
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<gameMaturityArr.size();i++)
        {
            int gameId=gameMaturityArr.get(i).getGameId();
            sfsArray.addInt(gameId);
        }
        return sfsArray;
    }

    public int getNormalMatchCount() 
    {
        return normalMatchCount;
    }

    public void setNormalMatchCount(int normalMatchCount) 
    {
        this.normalMatchCount = normalMatchCount;
    }

    public int getRankMatchCount() 
    {
        return rankMatchCount;
    }

    public void setRankMatchCount(int rankMatchCount) 
    {
        this.rankMatchCount = rankMatchCount;
    }
    
    public int getMatchCount() 
    {
        return normalMatchCount+rankMatchCount;
    }

    public int getWinNormalMatchCount() 
    {
        return winNormalMatchCount;
    }

    public void setWinNormalMatchCount(int winNormalMatchCount) 
    {
        this.winNormalMatchCount = winNormalMatchCount;
    }

    public int getWinRankMatchCount() 
    {
        return winRankMatchCount;
    }

    public void setWinRankMatchCount(int winRankMatchCount) 
    {
        this.winRankMatchCount = winRankMatchCount;
    }
    
    public int getWinMatchCount()
    {
        return winNormalMatchCount+winRankMatchCount;
    }
    
    public int getWinPercent() 
    {
        Integer matchCount=getMatchCount();
        Integer winMatchCount=getWinMatchCount();
        if(matchCount==0)
            return 0;
        else
        {
            Float winPercent=winMatchCount.floatValue()*100f
                                                    /matchCount.floatValue();
            return winPercent.intValue();
        }
    }

    public int getAfkMatchCount() 
    {
        return afkMatchCount;
    }

    public void setAfkMatchCount(int afkMatchCount) 
    {
        this.afkMatchCount = afkMatchCount;
    } 
    
    public JsonObject getData(JsonObject jsonData)
    {
        jsonData.addProperty(KJS.NMATCH_COUNT,normalMatchCount);
        jsonData.addProperty(KJS.WIN_NMATCH_COUNT,winNormalMatchCount);
        jsonData.addProperty(KJS.RMATCH_COUNT,rankMatchCount);
        jsonData.addProperty(KJS.WIN_RMATCH_COUNT,winRankMatchCount);
        jsonData.addProperty(KJS.EXPERIENCE_POINT,experiencePoint);
        jsonData.addProperty(KJS.RANK_POINT,rankPoint);
        jsonData.addProperty(KJS.AFKMATCH_COUNT,afkMatchCount);
        
        jsonData.addProperty(KJS.SPEED,competenceArr.get(0));
        jsonData.addProperty(KJS.OBSERVE,competenceArr.get(1));
        jsonData.addProperty(KJS.CALCULATE,competenceArr.get(2));
        jsonData.addProperty(KJS.MEMORY,competenceArr.get(3));
        jsonData.addProperty(KJS.LOGIC,competenceArr.get(4));
        jsonData.addProperty(KJS.REFLEX,competenceArr.get(5));
        
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<gameMaturityArr.size();i++)
        {
            GameMaturity gameMaturityObj=gameMaturityArr.get(0);
            int gameId=gameMaturityObj.getGameId();
            int gameMaturity=gameMaturityObj.getMaturity();
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.GAME_ID,gameId);
            jsonObject.addProperty(KJS.GAME_MATURITY,gameMaturity);
            jsonArray.add(jsonObject);
        }
        jsonData.add(KJS.GAME_MATURITY_ARR,jsonArray);
        return jsonData;
    }
    
    public SFSObject getPublicInfo(SFSObject userInfo)
    {
        userInfo.putInt(KJS.RANK,getRank());
        userInfo.putInt(KJS.RANK_POINT,getRankPoint());
        userInfo.putInt(KJS.MATCH_COUNT,getMatchCount());
        userInfo.putInt(KJS.WIN_PERCENT,getWinPercent());
        userInfo.putSFSArray(KJS.COMPETENCE,getCompetenceSFSArr());
        return userInfo;
    }
    
    public JsonObject getPublicInfo(JsonObject userInfo)
    {
        userInfo.addProperty(KJS.RANK,getRank());
        userInfo.addProperty(KJS.RANK_POINT,getRankPoint());
        userInfo.addProperty(KJS.MATCH_COUNT,getMatchCount());
        userInfo.addProperty(KJS.WIN_PERCENT,getWinPercent());
        userInfo.add(KJS.COMPETENCE,getCompetenceJsonArr());
        return userInfo;
    }
}

class GameMaturity
{
    private int gameId;
    private int maturity;

    public GameMaturity(int iGameId,int iMaturityPoint)
    {
        this.gameId=iGameId;
        this.maturity=iMaturityPoint;
    }
    
    public int getGameId() 
    {
        return gameId;
    }

    public void setGameId(int gameId) 
    {
        this.gameId = gameId;
    }

    public int getMaturity() 
    {
        return maturity;
    }

    public void setMaturity(int maturityPoint) 
    {
        this.maturity = maturityPoint;
    }  
}