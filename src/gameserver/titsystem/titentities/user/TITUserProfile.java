package gameserver.titsystem.titentities.user;

import com.google.gson.JsonObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titconfig.KJS;

public class TITUserProfile 
{
    private String userId;
    private String facebookId;
    private String fullName;
    private String email;
    private String birthDay;
    private String gender;
    private String facebookUrl;
    private String avatarUrl;
    private String registerDate;

    public void setData(JsonObject jsonData)
    {
        userId= jsonData.get(KJS.USER_ID).getAsString();
        facebookId = jsonData.get(KJS.FACEBOOK_ID).getAsString();
        fullName = jsonData.get(KJS.FULL_NAME).getAsString();
        email = jsonData.get(KJS.EMAIL).getAsString();
        birthDay = jsonData.get(KJS.BIRTH_DAY).getAsString();
        gender = jsonData.get(KJS.GENDER).getAsString();
        facebookUrl = jsonData.get(KJS.FACEBOOK_URL).getAsString();
        avatarUrl = jsonData.get(KJS.AVATAR_URL).getAsString();
        registerDate = jsonData.get(KJS.REGISTER_DATE).getAsString();
        
    }
    
    public String getUserId() 
    {
        return userId;
    }

    public void setUserId(String userId) 
    {
        
        this.userId = userId;
    }

    public String getFacebookId() 
    {
        return facebookId;
    }

    public void setFacebookId(String facebookId) 
    {
        this.facebookId = facebookId;
    }

    public String getFullName() 
    {
        return fullName;
    }

    public void setFullName(String fullName) 
    {
        this.fullName = fullName;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getBirthDay() 
    {
        return birthDay;
    }

    public void setBirthDay(String birthDay) 
    {
        this.birthDay = birthDay;
    }

    public String getGender() 
    {
        return gender;
    }

    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getFacebookUrl() 
    {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) 
    {
        this.facebookUrl = facebookUrl;
    }

    public String getAvatarUrl() 
    {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) 
    {
        this.avatarUrl = avatarUrl;
    }

    public String getRegisterDate() 
    {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) 
    {
        this.registerDate = registerDate;
    }
    
    public JsonObject getData(JsonObject jsonData)
    {
        jsonData.addProperty(KJS.USER_ID,userId);
        jsonData.addProperty(KJS.FACEBOOK_ID,facebookId);
        jsonData.addProperty(KJS.FULL_NAME,fullName);
        jsonData.addProperty(KJS.EMAIL,email);
        jsonData.addProperty(KJS.BIRTH_DAY,birthDay);
        jsonData.addProperty(KJS.GENDER,gender);
        jsonData.addProperty(KJS.FACEBOOK_URL,facebookUrl);
        jsonData.addProperty(KJS.AVATAR_URL,avatarUrl);
        jsonData.addProperty(KJS.REGISTER_DATE,registerDate);
        return jsonData;
    }
    
    public SFSObject getPublicInfo(SFSObject userInfo)
    {
        userInfo.putUtfString(KJS.USER_ID,getUserId());
        userInfo.putUtfString(KJS.FACEBOOK_ID,getFacebookId());
        userInfo.putUtfString(KJS.FACEBOOK_URL,getFacebookUrl());
        userInfo.putUtfString(KJS.AVATAR_URL,getAvatarUrl());
        userInfo.putUtfString(KJS.FULL_NAME,getFullName());
        userInfo.putUtfString(KJS.GENDER,getGender());
        return userInfo;
    }
    
    public JsonObject getPublicInfo(JsonObject userInfo)
    {
        userInfo.addProperty(KJS.USER_ID,getUserId());
        userInfo.addProperty(KJS.FACEBOOK_ID,getFacebookId());
        userInfo.addProperty(KJS.FACEBOOK_URL,getFacebookUrl());
        userInfo.addProperty(KJS.AVATAR_URL,getAvatarUrl());
        userInfo.addProperty(KJS.FULL_NAME,getFullName());
        userInfo.addProperty(KJS.GENDER,getGender());
        return userInfo;
    }
}
