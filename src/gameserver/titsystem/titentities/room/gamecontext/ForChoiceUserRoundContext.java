package gameserver.titsystem.titentities.room.gamecontext;

import com.google.gson.JsonArray;
import gameserver.titgameinfo.TITGameInfo;

public class ForChoiceUserRoundContext extends TITUserRoundGameContext
{
    private int maxPoint;
    private JsonArray gameScript;
    private JsonArray gameAnswer;
    private TITGameInfo gameInfo;
    
    public ForChoiceUserRoundContext()
    {
        point=0;
        trueCount=0;
        falseCount=0;
    }

    public boolean isChoicedGame()
    {
        if(gameInfo==null)
            return false;
        else
            return true;
    }

    public int getMaxPoint() 
    {
        return maxPoint;
    }

    public void setMaxPoint(int maxPoint) 
    {
        this.maxPoint = maxPoint;
    }
    
    
    public JsonArray getGameScript() 
    {
        return gameScript;
    }

    public void setGameScript(JsonArray gameScript) 
    {
        this.gameScript = gameScript;
    }

    public JsonArray getGameAnswer() 
    {
        return gameAnswer;
    }

    public void setGameAnswer(JsonArray gameAnswer) 
    {
        this.gameAnswer = gameAnswer;
    }

    public TITGameInfo getGameInfo() 
    {
        return gameInfo;
    }

    public void setGameInfo(TITGameInfo gameInfo) 
    {
        this.gameInfo = gameInfo;
    }
    
    
}
