package gameserver.titsystem.titentities.room.gamecontext;

import com.google.gson.JsonArray;
import java.util.ArrayList;
import gameserver.titgameinfo.TITGameInfo;

public class SameChoiceRoundContext extends TITRoundContext
{
    private int maxPoint;
    private JsonArray gameScript;
    private JsonArray gameAnswer;
    private TITGameInfo gameInfo;
    
    public SameChoiceRoundContext(int index,int playerCount) 
    {
        this.index=index;
        userRoundContextArr=new ArrayList<SameChoiceUserRoundContext>();
        for(int i=1;i<=playerCount;i++)
        {
            SameChoiceUserRoundContext userRoundGameContext=
                                                new SameChoiceUserRoundContext();
            userRoundContextArr.add(userRoundGameContext);
        }
    }
    
    public boolean isChoicedGame()
    {
        if(gameInfo==null)
            return false;
        else
            return true;
    }
    
    public SameChoiceUserRoundContext getUserRoundContext(int roomUserId)
    {
        return (SameChoiceUserRoundContext)
                                    userRoundContextArr.get(roomUserId-1);
    }

    public int getMaxPoint() 
    {
        return maxPoint;
    }

    public void setMaxPoint(int maxPoint)
    {
        this.maxPoint = maxPoint;
    }
    
    public TITGameInfo getGameInfo() 
    {
        return gameInfo;
    }

    public void setGameInfo(TITGameInfo gameInfo) 
    {
        this.gameInfo = gameInfo;
    }  
    
    public JsonArray getGameScript() 
    {
        return gameScript;
    }

    public void setGameScript(JsonArray gameScript) 
    {
        this.gameScript = gameScript;
    }

    public JsonArray getGameAnswer() 
    {
        return gameAnswer;
    }

    public void setGameAnswer(JsonArray gameAnswer)
    {
        this.gameAnswer = gameAnswer;
    }
}
