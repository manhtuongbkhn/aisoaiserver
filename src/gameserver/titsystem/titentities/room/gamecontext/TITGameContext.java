package gameserver.titsystem.titentities.room.gamecontext;

import java.util.ArrayList;

public class TITGameContext 
{
    protected int playerCount;
    protected int currentRound;
    protected ArrayList roundContextArr;
    
    public TITRoundContext getCurrentRoundContext()
    {
        return (TITRoundContext)roundContextArr.get(currentRound-1);
    }
    
    public int getTotalPoint(int roomUserId)
    {
        int currentRound=getCurrentRound();
        int totalPoint=0;
        for(int i=1;i<=currentRound;i++)
        {
            TITRoundContext roundContext=(TITRoundContext)
                                                    roundContextArr.get(i-1);
            TITUserRoundGameContext userRoundGameContext=
                            roundContext.getUserRoundGameContext(roomUserId);
            int covertPoint=userRoundGameContext.getConvertPoint();
            totalPoint=totalPoint+covertPoint;
        }
        return totalPoint;
    }
    
    public ArrayList<Integer> getRoundPointArr(int roomUserId)
    {
        ArrayList<Integer> roundPointArr=new ArrayList<Integer>();
        int roundCount=getCurrentRound();
        for(int i=1;i<=roundCount;i++)
        {
            TITRoundContext roundContext=(TITRoundContext)
                                                    roundContextArr.get(i-1);
            TITUserRoundGameContext userRoundGameContext=
                            roundContext.getUserRoundGameContext(roomUserId);
            int covertPoint=userRoundGameContext.getConvertPoint();
            roundPointArr.add(covertPoint);
        }
        return roundPointArr;
    }
    
    public int getPlayerCount() 
    {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) 
    {
        this.playerCount = playerCount;
    }
    
    public int getCurrentRound() 
    {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) 
    {
        this.currentRound = currentRound;
    }
    
    public int getRoundCount()
    {
        return roundContextArr.size();
    }
}
