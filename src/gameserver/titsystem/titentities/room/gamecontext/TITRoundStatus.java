package gameserver.titsystem.titentities.room.gamecontext;

public enum TITRoundStatus 
{
    NULL,
    CHOICING_GAME,
    SHOWING_GAME_INFO,
    PLAYING_GAME,
    SCORING_POINT;
    
    private TITRoundStatus() 
    {
        
    }
}

