package gameserver.titsystem.titentities.room.gamecontext;

import java.util.ArrayList;
import gameserver.titgameinfo.TITGameInfo;

public class ForChoiceGameContext extends TITGameContext
{
    public ForChoiceGameContext(int playerCount)
    {
        roundContextArr=new ArrayList<ForChoiceRoundContext>();
        this.playerCount=playerCount;
        int rountCount=playerCount+1;
        for(int i=1;i<=rountCount;i++)
        {
            ForChoiceRoundContext roundContext=
                                    new ForChoiceRoundContext(i,playerCount);
            roundContextArr.add(roundContext);
        }
    }
    
    public ForChoiceRoundContext getCurrentRoundContext()
    {
        return (ForChoiceRoundContext)roundContextArr.get(currentRound-1);
    }
    
    /*
    public int getTotalPoint(int roomUserId)
    {
        int currentRound=getCurrentRound();
        int totalPoint=0;
        for(int i=1;i<=currentRound;i++)
        {
            ForChoiceRoundContext roundContext=(ForChoiceRoundContext)
                                                    roundContextArr.get(i-1);
            ForChoiceUserRoundContext userRoundGameContext=roundContext.
                                            getUserRoundContext(roomUserId);
            int roundPoint=userRoundGameContext.getPoint();
            totalPoint=totalPoint+roundPoint;
        }
        return totalPoint;
    }
    
    public ArrayList<Integer> getRoundPointArr(int roomUserId)
    {
        ArrayList<Integer> roundPointArr=new ArrayList<Integer>();
        int roundCount=getCurrentRound();
        for(int i=1;i<=roundCount;i++)
        {
            ForChoiceRoundContext roundContext=(ForChoiceRoundContext)
                                                    roundContextArr.get(i-1);
            ForChoiceUserRoundContext userRoundContext=roundContext.
                                            getUserRoundContext(roomUserId);
            int roundPoint=userRoundContext.getPoint();
            roundPointArr.add(roundPoint);
        }
        return roundPointArr;
    }
    */
    
    public ArrayList<Integer> getChoicedGameIdArr(int roomUserId)
    {
        ArrayList<Integer> gameIdArr=new ArrayList<Integer>();
        int currentRound=getCurrentRound();
        for(int i=1;i<=currentRound-1;i++)
        {
            ForChoiceRoundContext roundContext=(ForChoiceRoundContext)
                                                    roundContextArr.get(i-1);
            ForChoiceUserRoundContext userRoundContext=roundContext.
                                                getUserRoundContext(roomUserId);
            int gameId=userRoundContext.getGameInfo().getGameId();
            gameIdArr.add(gameId);
        }
        return gameIdArr;
    }
    
    public void setLastRoundGameInfo(TITGameInfo gameInfo)
    {
        int roundCount=getRoundCount();
        ForChoiceRoundContext roundContext=(ForChoiceRoundContext)
                                            roundContextArr.get(roundCount-1);
        int playerCount=getPlayerCount();
        for(int i=1;i<=playerCount;i++)
        {
            ForChoiceUserRoundContext userRoundContext=roundContext.
                                                        getUserRoundContext(i);
            userRoundContext.setGameInfo(gameInfo);
        }
    }
    
    public TITGameInfo getLastRoundGameInfo()
    {
        int roundCount=getRoundCount();
        ForChoiceRoundContext roundContext=(ForChoiceRoundContext)
                                            roundContextArr.get(roundCount-1);
        ForChoiceUserRoundContext userRoundContext=roundContext.
                                                        getUserRoundContext(1);
        return  userRoundContext.getGameInfo();
    }
}
