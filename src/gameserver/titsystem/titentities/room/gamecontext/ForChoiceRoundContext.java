package gameserver.titsystem.titentities.room.gamecontext;

import java.util.ArrayList;

public class ForChoiceRoundContext extends TITRoundContext
{
    public ForChoiceRoundContext(int index,int playerCount) 
    {
        this.index=index;
        userRoundContextArr=new ArrayList<ForChoiceUserRoundContext>();
        for(int i=1;i<=playerCount;i++)
        {
            ForChoiceUserRoundContext userRoundGameContext=
                                                new ForChoiceUserRoundContext();
            userRoundContextArr.add(userRoundGameContext);
        }
    }
    
    public ForChoiceUserRoundContext getUserRoundContext(int roomUserId)
    {
        return (ForChoiceUserRoundContext)
                                    userRoundContextArr.get(roomUserId-1);
    }
    
    public boolean isAllUserChoicedGame()
    {
        for(int i=1;i<=userRoundContextArr.size();i++)
        {
            ForChoiceUserRoundContext userRoundContext=getUserRoundContext(i);
            if(userRoundContext.getGameInfo()==null)
                return false;
        }
        return true;
    }
    
    public int getWasChoiceGameRoomUserId(int choiceGameRoomUserId)
    {
        int wasChoicedGameUserRoomUserId=choiceGameRoomUserId+index;
        int playerCount=getUserCount();
        if(wasChoicedGameUserRoomUserId>playerCount)
            wasChoicedGameUserRoomUserId=
                                    wasChoicedGameUserRoomUserId-playerCount;
        return wasChoicedGameUserRoomUserId;
    }
}
