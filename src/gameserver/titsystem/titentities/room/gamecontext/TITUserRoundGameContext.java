package gameserver.titsystem.titentities.room.gamecontext;

public class TITUserRoundGameContext 
{
    protected int point;
    protected int convertPoint;
    protected int trueCount;
    protected int falseCount;
    
    public int getPoint() 
    {
        return point;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public void addPoint(int changePoint)
    {
        point=point+changePoint;
    }
    
    public int getTrueCount() 
    {
        return trueCount;
    }

    public void addTrueCount(int changeTrueCount)
    {
        trueCount=trueCount+changeTrueCount;
    }
    
    public void setTrueCount(int trueCount) 
    {
        this.trueCount = trueCount;
    }

    public int getFalseCount()
    {
        return falseCount;
    }

    public void setFalseCount(int falseCount) 
    {
        this.falseCount = falseCount;
    }
    
    public void addFalseCount(int changeFalseCount)
    {
        falseCount=falseCount+changeFalseCount;
    }

    public int getConvertPoint() 
    {
        return convertPoint;
    }

    public void setConvertPoint(int convertPoint) 
    {
        this.convertPoint = convertPoint;
    }
}
