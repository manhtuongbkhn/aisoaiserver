package gameserver.titsystem.titentities.room.gamecontext;

import java.util.ArrayList;

public class TITRoundContext 
{
    protected int index;
    protected TITRoundStatus status;
    protected int gameOverTime;
    protected int hardLevel;
    protected ArrayList userRoundContextArr;

    public int getIndex() 
    {
        return index;
    }
    
    public TITUserRoundGameContext getUserRoundGameContext(int roomUserId)
    {
        return (TITUserRoundGameContext) 
                                    userRoundContextArr.get(roomUserId-1);
    }
    
    public TITRoundStatus getStatus() 
    {
        if(status==null)
            return TITRoundStatus.NULL;
        return status;
    }

    public void setStatus(TITRoundStatus status) 
    {
        this.status = status;
    }
    
    public int getGameOverTime() 
    {
        return 60;
    }

    public void setGameOverTime(int gameOverTime) 
    {
        this.gameOverTime = gameOverTime;
    }

    public int getHardLevel() 
    {
        return 1;
    }

    public void setHardLevel(int hardLevel) 
    {
        this.hardLevel = hardLevel;
    } 
    
    protected int getUserCount()
    {
        return userRoundContextArr.size();
    }
}
