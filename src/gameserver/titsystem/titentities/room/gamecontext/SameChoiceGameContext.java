package gameserver.titsystem.titentities.room.gamecontext;

import java.util.ArrayList;

public class SameChoiceGameContext extends TITGameContext
{   
    public SameChoiceGameContext(int playerCount)
    {
        roundContextArr=new ArrayList<SameChoiceRoundContext>();
        this.playerCount=playerCount;
        int rountCount=playerCount+1;
        for(int i=1;i<=rountCount;i++)
        {
            SameChoiceRoundContext roundContext=
                                    new SameChoiceRoundContext(i,playerCount);
            roundContextArr.add(roundContext);
        }
    }

    public SameChoiceRoundContext getCurrentRoundContext()
    {
        return (SameChoiceRoundContext)roundContextArr.get(currentRound-1);
    }
    
    /*
    public int getTotalPoint(int roomUserId)
    {
        int currentRound=getCurrentRound();
        int totalPoint=0;
        for(int i=1;i<=currentRound;i++)
        {
            SameChoiceRoundContext roundContext=(SameChoiceRoundContext)
                                                    roundContextArr.get(i-1);
            SameChoiceUserRoundContext userRoundGameContext=roundContext.
                                            getUserRoundContext(roomUserId);
            int roundPoint=userRoundGameContext.getPoint();
            totalPoint=totalPoint+roundPoint;
        }
        return totalPoint;
    }
    
    public ArrayList<Integer> getRoundPointArr(int roomUserId)
    {
        ArrayList<Integer> roundPointArr=new ArrayList<Integer>();
        int roundCount=getCurrentRound();
        for(int i=1;i<=roundCount;i++)
        {
            SameChoiceRoundContext roundContext=(SameChoiceRoundContext)
                                                    roundContextArr.get(i-1);
            SameChoiceUserRoundContext userRoundContext=roundContext.
                                            getUserRoundContext(roomUserId);
            int roundPoint=userRoundContext.getPoint();
            roundPointArr.add(roundPoint);
        }
        return roundPointArr;
    }
    */
    
    public ArrayList<Integer> getChoicedGameIdArr()
    {
        ArrayList<Integer> gameIdArr=new ArrayList<Integer>();
        int currentRound=getCurrentRound();
        for(int i=1;i<=currentRound-1;i++)
        {
            SameChoiceRoundContext roundContext=(SameChoiceRoundContext)
                                                    roundContextArr.get(i-1);
            int gameId=roundContext.getGameInfo().getGameId();
            gameIdArr.add(gameId);
        }
        return gameIdArr;
    }
}
