package gameserver.titsystem.titentities.room;

import java.util.ArrayList;
import gameserver.titsystem.titentities.user.TITUser;

public class TITInvitationManager 
{
    private NotSyncTITInvitationManager notSyncTITInvitationManager;

    public TITInvitationManager()
    {
        notSyncTITInvitationManager=new NotSyncTITInvitationManager();
    }
    
    public void add(TITInvitation invitation) 
    {
        notSyncTITInvitationManagerMethod("add",invitation);
    }

    public boolean isUserWasInvited(TITUser user) 
    {
        return (boolean)notSyncTITInvitationManagerMethod
                                                    ("isUserWasInvited",user);
    }

    public boolean removeByWasInvitedUser(TITUser wasInvitedUser)
    {
        return (boolean)notSyncTITInvitationManagerMethod
                                    ("removeByWasInvitedUser",wasInvitedUser);
    }
    
    public void reset()
    {
        notSyncTITInvitationManagerMethod("reset",null);
    }
    
    public void destroy()
    {
        notSyncTITInvitationManagerMethod("destroy",null);
    }
    
    private synchronized Object notSyncTITInvitationManagerMethod
                                            (String methodName, Object param1) 
    {
        TITUser user;
        switch (methodName) 
        {
            case "add":
                TITInvitation invitation=(TITInvitation) param1;
                notSyncTITInvitationManager.add(invitation);
                break;
            case "isUserWasInvited": 
                user=(TITUser) param1;
                return notSyncTITInvitationManager.isUserWasInvited(user);
            case "removeByWasInvitedUser":
                user=(TITUser) param1;
                return notSyncTITInvitationManager.removeByWasInvitedUser(user);
            case "reset":
                notSyncTITInvitationManager.reset();
                break;
            case "destroy":
                notSyncTITInvitationManager=null;
                break;
        }
        return null;
    }
}

class NotSyncTITInvitationManager
{
    private ArrayList<TITInvitation> invitationArr;
    
    public NotSyncTITInvitationManager()
    {
        invitationArr=new ArrayList<TITInvitation>();
    }
    
    protected void add(TITInvitation invitation)
    {
        invitationArr.add(invitation);
    }
    
    public void reset()
    {
        invitationArr.clear();
    }
    
    protected boolean removeByWasInvitedUser(TITUser user)
    {
        for (int i = 0; i < invitationArr.size(); ++i) 
        {
            TITInvitation invitation = invitationArr.get(i);
            String wasInvitedUserId=invitation.getWasInvitedUserId();
            if(wasInvitedUserId.equals(user.getUserProfile().getUserId()))
                return invitationArr.remove(invitation);
        }
        return false;
    }
    
    protected boolean isUserWasInvited(TITUser user)
    {
        for (int i = 0; i < invitationArr.size(); ++i) 
        {
            TITInvitation invitation = invitationArr.get(i);
            String wasInvitedUserId=invitation.getWasInvitedUserId();
            if(wasInvitedUserId.equals(user.getUserProfile().getUserId()))
                return true;
        }
        return false;
    }
}

