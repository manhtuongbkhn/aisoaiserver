package gameserver.titsystem.titentities.room;

import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.Date;
import gameserver.titconfig.TITFunction;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.KJS;

public class TITMessageInfo 
{    
    private String senderFullName;
    private String senderUserId;
    private int senderSystemUserId;
    private String senderAvatarUrl;
    
    private Date sendTime;
    private String messageContent;

    public void setSendUser(TITUser senderUser) 
    {
        senderFullName=senderUser.getUserProfile().getFullName();
        senderUserId=senderUser.getUserProfile().getUserId();
        senderSystemUserId=senderUser.getDefaultUserInfo().getSystemUserId();
        senderAvatarUrl=senderUser.getUserProfile().getAvatarUrl();
    }

    public Date getSendTime() 
    {
        return this.sendTime;
    }

    public void setSendTime(Date sendTime) 
    {
        this.sendTime = sendTime;
    }

    public String getMessageContent() 
    {
        return this.messageContent;
    }

    public void setMessageContent(String messageContent) 
    {
        this.messageContent = messageContent;
    }

    public SFSObject toSFSObject() 
    {
        SFSObject sfsObject = new SFSObject();
        sfsObject.putUtfString(KJS.USER_ID,senderUserId);
        sfsObject.putInt(KJS.SYSTEM_USER_ID,senderSystemUserId);
        sfsObject.putUtfString(KJS.FULL_NAME,senderFullName);
        sfsObject.putUtfString(KJS.SEND_TIME,TITFunction.covertDate(sendTime));
        sfsObject.putUtfString(KJS.MESSAGE_CONTENT,messageContent);
        sfsObject.putUtfString(KJS.AVATAR_URL,senderAvatarUrl);
        return sfsObject;
    }
}

