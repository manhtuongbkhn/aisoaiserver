package gameserver.titsystem.titentities.room;

import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import java.util.List;
import gameserver.titsystem.titentities.room.TITRoomStatus;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;

public class TITDefaultRoomInfo 
{
    private SFSRoom sfsRoom;
    private String invite;
    private int avatar;
    private int playType;
    private TITRoomStatus roomStatus;
    ArrayList<TITUser> backupUserArr;
    
    public TITDefaultRoomInfo(SFSRoom sfsRoom)
    {
        this.sfsRoom=sfsRoom;
    }

    public int getId()
    {
        return sfsRoom.getId();
    }
    
    public String getInvite() 
    {
        return invite;
    }

    public void setInvite(String invite) 
    {
        this.invite = invite;
    }
    
    public void setName(String name)
    {
        sfsRoom.setName(name);
    }
    
    public String getName() 
    {
        return sfsRoom.getName();
    }

    public int getAvatar() 
    {
        return avatar;
    }

    public void setAvatar(int avatar) 
    {
        this.avatar = avatar;
    }    
    
    public void setPass(String pass)
    {
        sfsRoom.setPassword(pass);
    }
    
    public String getPass() 
    {
        if (sfsRoom.getPassword()==null) 
        {
            return "";
        }
        return this.sfsRoom.getPassword();
    }
    
    public boolean isLock()
    {
        if(getPass()=="")
            return false;
        else
            return true;
    }
    
    public int getPlayerMax() 
    {
        return sfsRoom.getMaxUsers();
    }
    
    
    private int getUserCount() 
    {
        return getUserArr().size();
    }
    
    public int getOnlinePlayerCount()
    {
        return sfsRoom.getUserList().size();
    }
    
    public int getPlayerCount() 
    {
        return getPlayerArr().size();
    }
    
    public int getSpectatorCount()
    {
        return getSpectatorArr().size();
    }
    
    public Integer getType() 
    {
        String groupId;
        switch (groupId = this.sfsRoom.getGroupId()) 
        {
            case ServerConfig.NORMALROOM_GROUP: 
                return 1;
            case ServerConfig.CHALLENGEROOM_GROUP:
                return 2;
        }
        return 0;
    }
    
    public boolean isOwner(TITUser user)
    {
        if(user==null)
            return false;
        SFSUser sfsOwner=(SFSUser) sfsRoom.getOwner();
        if(sfsOwner==null)
            return false;
        else
        {
           TITUser titOwner=new TITUser(sfsOwner);
           return titOwner.equals(user);
        }
    }
    
    public void setOwner(TITUser user)
    {
        if(user==null)
        {
            sfsRoom.setOwner(null);
        }
        else
        {
            SFSUser sfsUser=user.getSFSUser();
            sfsRoom.setOwner(sfsUser);
        }
    }
    
    public TITUser getOwner() 
    {
        SFSUser sfsUser = (SFSUser)sfsRoom.getOwner();
        if (sfsUser == null) 
            return null;
        TITUser titUser = new TITUser(sfsUser);
        return titUser;
    }
    
    public void setStatus(TITRoomStatus roomStatus) 
    {
        this.roomStatus=roomStatus;
    }

    public TITRoomStatus getStatus() 
    {
        if(roomStatus==null)
            return TITRoomStatus.NULL;
        return roomStatus;
    }

    public int getPlayType() 
    {
        return playType;
    }

    public void setPlayType(int playType) 
    {
        this.playType = playType;
    }
    
    
    public void backupUserArr()
    {
        this.backupUserArr=getUserArr();
    }
    
    public void clearBackupUserArr()
    {
        backupUserArr=null;
    }
    
    private ArrayList<TITUser> getBackupUserArr() 
    {
        return backupUserArr;
    }
    
    /*
    private ArrayList<TITUser> getBackupPlayerArr() 
    {
        ArrayList<TITUser> playerArr = new ArrayList<TITUser>();
        for (int i = 0; i < backupUserArr.size();i++) 
        {
            TITUser user = backupUserArr.get(i);
            if (user.getDefaultUserInfo().isPlayer()) 
                playerArr.add(user);
        }
        return playerArr;
    }

    private ArrayList<TITUser> getBackupSpectatorArr() 
    {
        ArrayList<TITUser> spectatorArr = new ArrayList<TITUser>();
        for (int i = 0; i < backupUserArr.size(); ++i) 
        {
            TITUser user = backupUserArr.get(i);
            if (user.getDefaultUserInfo().isSpectator()) 
                spectatorArr.add(user);
        }
        return spectatorArr;
    }
    */
    
    private ArrayList<TITUser> getUserArr()
    {
        switch (getStatus()) 
        {
            case NULL: 
            case WAITING:
            case REFESHING:
                ArrayList<TITUser> userArr = new ArrayList<TITUser>();
                List sfsUserArr = sfsRoom.getUserList();
                for (int i=0;i<sfsUserArr.size();i++) 
                {
                    SFSUser sfsUser = (SFSUser)sfsUserArr.get(i);
                    TITUser titUser = new TITUser(sfsUser);
                    if (titUser.getDefaultUserInfo().isOwner()) 
                        userArr.add(0, titUser);
                    else
                        userArr.add(titUser);
                }
                return userArr;
            case PLAYING_GAME:
            case SCORING_TOTALPOINT:
                return backupUserArr;
            default:
                return backupUserArr;
        }
    }
    
    public ArrayList<TITUser> getOnlinePlayerArr()
    {
        ArrayList<TITUser> playerArr = new ArrayList<TITUser>();
        List sfsuserList = sfsRoom.getUserList();
        for (int i = 0; i < sfsuserList.size();i++) 
        {
            SFSUser sfsUser = (SFSUser)sfsuserList.get(i);
            TITUser titUser = new TITUser(sfsUser);
            if (titUser.getDefaultUserInfo().isOwner()) 
                playerArr.add(0, titUser);
            else
                playerArr.add(titUser);
        }
        return playerArr;
    }
    
    public ArrayList<TITUser> getPlayerArr() 
    {
        switch (this.getStatus()) 
        {
            case NULL: 
            case WAITING:
            case REFESHING:
                return getOnlinePlayerArr();
            case PLAYING_GAME:
            case SCORING_TOTALPOINT:    
                return backupUserArr;
            default:
                return backupUserArr;
        }
    }
    
    public ArrayList<TITUser> getSpectatorArr() 
    {   
        switch (this.getStatus()) 
        {
            case NULL: 
            case WAITING:
            case PLAYING_GAME:
            case SCORING_TOTALPOINT:    
                return new ArrayList<TITUser>();
            case REFESHING:
                 ArrayList<TITUser> spectatorArr = new ArrayList<TITUser>();
                List sfsuserList = this.sfsRoom.getSpectatorsList();
                for (int i = 0; i < sfsuserList.size(); ++i) 
                {
                    SFSUser sfsUser = (SFSUser)sfsuserList.get(i);
                    TITUser titUser = new TITUser(sfsUser);
                    if (titUser.getDefaultUserInfo().isOwner()) 
                        spectatorArr.add(0, titUser);
                    else
                        spectatorArr.add(titUser);
                }
                return spectatorArr;
            default:
                return new ArrayList<>();
        }
    }
    
    public TITUser getUserBySystemId(int systeUserId) 
    {
        SFSUser sfsUser = (SFSUser)sfsRoom.getUserById(systeUserId);
        if (sfsUser == null)
            return null;
        return new TITUser(sfsUser);
    }
    
    public TITUser getUserByRoomUserId(int roomUserId)
    {
        for(int i=0;i<getPlayerArr().size();i++)
        {
            TITUser user=getPlayerArr().get(i);
            if(user.getDefaultUserInfo().getRoomUserId()==roomUserId)
                return user;
        }
        return null;
    }
    
    public boolean checkUserInRoom(TITUser user) 
    {
        return sfsRoom.containsUser(user.getSFSUser());
    }
    
    public SFSObject getPublicInfo(SFSObject roomInfo) 
    {
        roomInfo.putInt(KJS.ROOM_ID,getId());
        roomInfo.putUtfString(KJS.ROOM_NAME, getName());
        roomInfo.putUtfString(KJS.ROOM_PASS,getPass());
        roomInfo.putBool(KJS.ROOM_LOCK, isLock());
        roomInfo.putUtfString(KJS.ROOM_INVITE,getInvite());
        roomInfo.putInt(KJS.ROOM_TYPE,getType());
        roomInfo.putInt(KJS.ROOM_AVATAR,getAvatar());
        roomInfo.putUtfString(KJS.ROOM_STATUS,getStatus().toString());
        roomInfo.putInt(KJS.PLAYER_COUNT,getPlayerCount());
        roomInfo.putInt(KJS.PLAYER_MAX,getPlayerMax());
        TITUser owner = getOwner();
        int ownerSystemUserId=-1;
        String ownerUserId=null;
        if(owner==null)
        {
            ownerSystemUserId=-1;
            ownerUserId="xxxxxxxx";
        }
        else
        {
            ownerSystemUserId=owner.getDefaultUserInfo().getSystemUserId();
            ownerUserId=owner.getUserProfile().getUserId();
        }
        roomInfo.putInt(KJS.SYSTEM_USER_ID,ownerSystemUserId);
        roomInfo.putUtfString(KJS.USER_ID, ownerUserId);
        return roomInfo;
    }
    
    public SFSObject getSendClientInfo(SFSObject roomInfo) 
    {
        roomInfo.putInt(KJS.ROOM_ID,getId());
        roomInfo.putUtfString(KJS.ROOM_NAME,getName());
        roomInfo.putBool(KJS.ROOM_LOCK,isLock());
        roomInfo.putUtfString(KJS.ROOM_INVITE,getInvite());
        roomInfo.putInt(KJS.ROOM_TYPE,getType());
        roomInfo.putInt(KJS.ROOM_AVATAR,getAvatar());
        switch (this.getStatus()) 
        {
            case NULL: 
            case WAITING:
                roomInfo.putUtfString(KJS.ROOM_STATUS,"WAITING");
                break;
            case PLAYING_GAME: 
                roomInfo.putUtfString(KJS.ROOM_STATUS,"PLAYING");
                break;
            case SCORING_TOTALPOINT:
            case REFESHING:    
                roomInfo.putUtfString(KJS.ROOM_STATUS,"SCORING");
                break;
            default:
                roomInfo.putUtfString(KJS.ROOM_STATUS,"PLAYING");
                break;
        }
        roomInfo.putInt(KJS.PLAYER_COUNT,getPlayerCount());
        roomInfo.putInt(KJS.PLAYER_MAX,getPlayerMax());
        TITUser owner = getOwner();
        int ownerSystemUserId=-1;
        String ownerUserId=null;
        if(owner==null)
        {
            ownerSystemUserId=-1;
            ownerUserId="xxxxxxxx";
        }
        else
        {
            ownerSystemUserId=owner.getDefaultUserInfo().getSystemUserId();
            ownerUserId=owner.getUserProfile().getUserId();
        }
        if(owner==null)
        {
            ownerSystemUserId=-1;
            ownerUserId="xxxxxxxx";
        }
        roomInfo.putInt(KJS.SYSTEM_USER_ID,ownerSystemUserId);
        roomInfo.putUtfString(KJS.USER_ID,ownerUserId);
        return roomInfo;
    }
    
    public SFSArray getAllUserPublicInfo() 
    {
        ArrayList<TITUser> arrUser = getPlayerArr();
        SFSArray sfsArray = new SFSArray();
        for (int i = 0; i < arrUser.size(); ++i) 
        {
            TITUser titUser = arrUser.get(i);
            SFSObject sfsObject = titUser.getPublicInfo();
            sfsArray.addSFSObject(sfsObject);
        }
        return sfsArray;
    }
}
