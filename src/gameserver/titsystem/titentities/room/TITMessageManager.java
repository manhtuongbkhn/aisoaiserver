package gameserver.titsystem.titentities.room;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;

public class TITMessageManager 
{
    private NotSyncTITMessageManager notSyncTITMessageManager;
    
    public TITMessageManager()
    {
        notSyncTITMessageManager=new NotSyncTITMessageManager();
    }
    
    public void add(TITMessageInfo messageInfo) 
    {
        notSyncMessageArrMethod("add", messageInfo);
    }
    
    public void reset()
    {
        notSyncMessageArrMethod("reset",null);
    }
    
    public void destroy()
    {
        notSyncMessageArrMethod("destroy",null);
    }
    
    public ArrayList<TITMessageInfo> getMessageArr() 
    {
        return (ArrayList)notSyncMessageArrMethod("getMessageArr", null);
    }

    public ArrayList<TITMessageInfo> getMessageArr20() 
    {
        return (ArrayList)notSyncMessageArrMethod("getMessageArr20", null);
    }

    public SFSArray getMessageSFSArr() 
    {
        return (SFSArray)notSyncMessageArrMethod("getMessageSFSArr", null);
    }

    public SFSArray getMessageSFSArr20() 
    {
        return (SFSArray)notSyncMessageArrMethod("getMessageSFSArr20", null);
    }

    private synchronized Object notSyncMessageArrMethod
                                            (String methodName,Object param1) 
    {
        switch (methodName) 
        {
            case "add": 
                TITMessageInfo messageInfo = (TITMessageInfo)param1;
                notSyncTITMessageManager.add(messageInfo);
                break;
            case "reset":
                notSyncTITMessageManager.reset();
                break;
            case "destroy":
                notSyncTITMessageManager=null;
                break;
            case "getMessageArr":
                return notSyncTITMessageManager.getMessageArr();
            case "getMessageArr20":
                return notSyncTITMessageManager.getMessageArr20();
            case "getMessageSFSArr":
                return notSyncTITMessageManager.getSFSMessageArr();
            case "getMessageSFSArr20":
                return notSyncTITMessageManager.getSFSMessageArr20();
        }
        return null;
    }
}

class NotSyncTITMessageManager
{
    private ArrayList<TITMessageInfo> messageArr;
    
    public NotSyncTITMessageManager()
    {
        messageArr=new ArrayList<TITMessageInfo>();
    }
    
    protected void add(TITMessageInfo messageInfo) 
    {
        int size = messageArr.size();
        if (size <= 20) 
            messageArr.add(messageInfo);
        else 
        {
            messageArr.add(messageInfo);
            messageArr.remove(0);
        }
    }
    
    protected void reset()
    {
        messageArr.clear();
    }
    
    protected ArrayList<TITMessageInfo> getMessageArr() 
    {
        return (ArrayList) messageArr.clone();
    }

    protected ArrayList<TITMessageInfo> getMessageArr20() 
    {
        ArrayList<TITMessageInfo> subMessageArr = new ArrayList<TITMessageInfo>();
        int end = this.messageArr.size() - 1;
        int start = end - 20 + 1;
        if (start < 0) 
            start = 0;
        for (int i = start; i <= end; ++i) 
        {
            TITMessageInfo messageInfo = messageArr.get(i);
            subMessageArr.add(messageInfo);
        }
        return (ArrayList)subMessageArr.clone();
    }

    protected SFSArray getSFSMessageArr() 
    {
        SFSArray messageSFSArray = new SFSArray();
        ArrayList cloneMessageArr = (ArrayList)messageArr.clone();
        for (int i = 0; i < cloneMessageArr.size(); i++) 
        {
            TITMessageInfo messageInfo = (TITMessageInfo)cloneMessageArr.get(i);
            SFSObject messageSFSObject = messageInfo.toSFSObject();
            messageSFSArray.addSFSObject((ISFSObject)messageSFSObject);
        }
        return messageSFSArray;
    }

    protected SFSArray getSFSMessageArr20() 
    {
        SFSArray messageSFSArray = new SFSArray();
        ArrayList<TITMessageInfo> subCloneMessageArr = getMessageArr20();
        for (int i = 0; i < subCloneMessageArr.size();i++) 
        {
            TITMessageInfo messageInfo = subCloneMessageArr.get(i);
            SFSObject messageSFSObject = messageInfo.toSFSObject();
            messageSFSArray.addSFSObject((ISFSObject)messageSFSObject);
        }
        return messageSFSArray;
    }
}
