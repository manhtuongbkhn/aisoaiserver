package gameserver.titsystem.titentities.room;

import gameserver.titsystem.titentities.room.gamecontext.SameChoiceGameContext;
import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import gameserver.titsystem.titentities.room.gamecontext.ForChoiceGameContext;
import gameserver.titsystem.titentities.room.gamecontext.TITGameContext;
import gameserver.titconfig.KJS;

public class TITRoom 
{
    private SFSRoom sfsRoom;

    public TITRoom(SFSRoom sfsRoom) 
    {
        this.sfsRoom = sfsRoom;
    }

    public void initDefaultRoomInfo()
    {
        TITDefaultRoomInfo defaultRoomInfo=new TITDefaultRoomInfo(sfsRoom);
        sfsRoom.setProperty(KJS.DEFAULT_ROOMINFO,defaultRoomInfo);
    }   
    
    public TITDefaultRoomInfo getDefaultRoomInfo()
    {
        return (TITDefaultRoomInfo) sfsRoom.getProperty(KJS.DEFAULT_ROOMINFO);
    }
    
    public void destroyDefaultRoomInfo()
    {
        sfsRoom.removeProperty(KJS.DEFAULT_ROOMINFO);
    }   
    
    public void initGameContext()
    {
        int playeType=getDefaultRoomInfo().getPlayType();
        int playerCount=getDefaultRoomInfo().getPlayerCount();
        TITGameContext gameContext;
        switch(playeType)
        {
            case 1:
                gameContext=new SameChoiceGameContext(playerCount);
                break;
            case 2:
            default:    
                gameContext=new ForChoiceGameContext(playerCount);
                break;
        }
        sfsRoom.setProperty(KJS.GAME_CONTEXT,gameContext);
    }
    
    public SameChoiceGameContext getSameChoiceGameContext()
    {
        if(getDefaultRoomInfo().getPlayType()==1)
            return (SameChoiceGameContext) getGameContext();
        else
            return null;
    }
    
    public ForChoiceGameContext getForChoiceGameContext()
    {
        if(getDefaultRoomInfo().getPlayType()==2)
            return (ForChoiceGameContext) getGameContext();
        else
            return null;
    }
    
    public TITGameContext getGameContext()
    {
        return (TITGameContext) sfsRoom.getProperty(KJS.GAME_CONTEXT);
    }
    
    public void destroyGameContext()
    {
        sfsRoom.removeProperty(KJS.GAME_CONTEXT);
    }
    
    public void initMessageManager() 
    {
        TITMessageManager messageManager = new TITMessageManager();
        sfsRoom.setProperty(KJS.MESSAGE_MANAGER,messageManager);
    }

    public TITMessageManager getMessageManager() 
    {
        return (TITMessageManager)sfsRoom.getProperty(KJS.MESSAGE_MANAGER);
    }
    
    
    public void initInvitationManager() 
    {
        TITInvitationManager invitationManager = new TITInvitationManager();
        sfsRoom.setProperty(KJS.INVITATION_MANAGER,invitationManager);
    }

    public TITInvitationManager getInvitationManager() 
    {
        return (TITInvitationManager)
                                    sfsRoom.getProperty(KJS.INVITATION_MANAGER);
    }
    
    public void destroyMessageManager()
    {
        getMessageManager().destroy();
        sfsRoom.removeProperty(KJS.MESSAGE_MANAGER);
    }
    
    public void destroyInvitationManager()
    {
        getInvitationManager().destroy();
        sfsRoom.removeProperty(KJS.INVITATION_MANAGER);
    }
    
    private boolean setVariable(SFSRoomVariable rv) 
    {
        try 
        {
            sfsRoom.setVariable((RoomVariable)rv);
            return true;
        }
        catch (SFSVariableException ex) 
        {
            return false;
        }
    }

    public void setAutoRemoveMode() 
    {
        this.sfsRoom.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);
    }

    public boolean setFlag(boolean flag) 
    {
        SFSRoomVariable rv = new SFSRoomVariable(KJS.ROOM_FLAG,flag);
        return this.setVariable(rv);
    }

    public boolean getFlag() 
    {
        SFSRoomVariable rv = (SFSRoomVariable)sfsRoom.getVariable(KJS.ROOM_FLAG);
        if (rv == null)
            return false;
        return true;
    }

    public SFSRoom getSFSRoom() 
    {
        return this.sfsRoom;
    }

}

