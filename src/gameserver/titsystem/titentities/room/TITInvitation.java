package gameserver.titsystem.titentities.room;

import java.util.Date;
import gameserver.titsystem.titentities.user.TITUser;

public class TITInvitation 
{
    private String inviteUserId;
    private String wasInvitedUserId;
    private Date inviteTime;
    private boolean answer;

    public TITInvitation() 
    {
    }

    public TITInvitation(TITUser inviteUser, TITUser wasInvitedUser) 
    {
        this.inviteUserId = inviteUser.getUserProfile().getUserId();
        this.wasInvitedUserId = wasInvitedUser.getUserProfile().getUserId();
        this.inviteTime = new Date();
        this.answer = false;
    }
    
    public Date getInviteTime() 
    {
        return this.inviteTime;
    }
    
    public boolean isAnswer() 
    {
        return this.answer;
    }

    public void setAnswer(boolean answer) 
    {
        this.answer = answer;
    }
    
    public String getInviteUserId() 
    {
        return inviteUserId;
    }

    public String getWasInvitedUserId() 
    {
        return wasInvitedUserId;
    }
}

