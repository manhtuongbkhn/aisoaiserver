package gameserver.titsystem.titservice.roomlist;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;
import gameserver.titsystem.titentities.TITSend;
import gameserver.titsystem.titentities.TITZone;
import gameserver.titsystem.titentities.user.TITUser;
import java.util.ArrayList;

public class RoomListService 
{
    private static ArrayList<RoomListUserToken> tokenArr=
                                            new ArrayList<RoomListUserToken>();
    
    public static void register(RoomListUserToken token)
    {
        syncTokenArrMethod("add",token);
    }
    
    public static void unregister(RoomListUserToken token)
    {
        syncTokenArrMethod("remove",token);
    }
    
    private static Object syncTokenArrMethod(String methodName,Object param1)
    {
        RoomListUserToken token;
        switch(methodName)
        {
            case "add":
                token=(RoomListUserToken) param1;
                return tokenArr.add(token);
            case "remove":
                token=(RoomListUserToken) param1;
                return tokenArr.remove(token);
            case "clone":
                return tokenArr.clone();
        }
        return null;
    }
    
    public void start()
    {
        while(true)
        {
            reloadRoomList();
            try {Thread.sleep(ServerConfig.RELOAD_ROOMLIST_MILLIS);} 
            catch (InterruptedException ex) {}
        }
    }
    
    private void reloadRoomList()
    {
        TITSend send=TITSend.getDefaultSend();
        TITZone zone=TITZone.getDefaultZone();
        SFSObject toClientData;
        SFSArray sfsArray=zone.getSendClientRoomInfoSFFArr();
        ArrayList<RoomListUserToken> cloneTokenArr=(ArrayList<RoomListUserToken>)
                                            syncTokenArrMethod("clone",null);
        for(int i=0;i<tokenArr.size();i++)
        {
            RoomListUserToken token=cloneTokenArr.get(i);
            TITUser user=token.getUser();
            RoomListOption option=token.getOption();
            if(option.isOption())
            {
             toClientData = new SFSObject();
                toClientData.putSFSArray(KJS.ARRAY,sfsArray);
                send.send(CMDNF.RELOADROOMLIST_NF,toClientData,user);
            }
        }
    }
    
    private void sendRoomList()
    {
        
    }
    
}
