package gameserver.titsystem.titservice.roomlist;

import com.google.gson.JsonObject;
import gameserver.titconfig.KJS;

public class RoomListOption 
{
    private boolean option;
    private String name;
    private int playerCount;
    private int playType;
    private int lock;
    
    public RoomListOption(JsonObject jsonObject)
    {
        if(jsonObject==null)
        {
            option=false;
        }
        else
        {
            option=true;
            if(jsonObject.get(KJS.ROOM_NAME)==null)
                name="";
            else
                name=jsonObject.get(KJS.ROOM_NAME).getAsString();
            
            if(jsonObject.get(KJS.PLAYER_COUNT)==null)
                playerCount=-1;
            else
                playerCount=jsonObject.get(KJS.PLAYER_COUNT).getAsInt();
            
            if(jsonObject.get(KJS.PLAY_TYPE)==null)
                playType=-1;
            else
                playType=jsonObject.get(KJS.PLAY_TYPE).getAsInt();
            
            if(jsonObject.get(KJS.PLAY_TYPE)==null)
                playType=-1;
            else
                playType=jsonObject.get(KJS.PLAY_TYPE).getAsInt();
            
            if(jsonObject.get(KJS.ROOM_LOCK)==null)
                lock=-1;
            else
                if(jsonObject.get(KJS.ROOM_LOCK).getAsBoolean())
                    lock=1;
                if(jsonObject.get(KJS.ROOM_LOCK).getAsBoolean())
                    lock=0;
        }
    }

    public boolean isOption() 
    {
        return option;
    }

    public String getName() 
    {
        return name;
    }

    public int getPlayerCount() 
    {
        return playerCount;
    }

    public int getPlayType() 
    {
        return playType;
    }

    public int getLock() 
    {
        return lock;
    }
}
