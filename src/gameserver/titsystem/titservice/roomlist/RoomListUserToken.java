package gameserver.titsystem.titservice.roomlist;

import gameserver.titsystem.titentities.user.TITUser;

public class RoomListUserToken 
{
    private TITUser user;
    private RoomListOption option;
    
    public RoomListUserToken(TITUser user,RoomListOption option)
    {
        this.user=user;
        this.option=option;
    }

    public TITUser getUser() 
    {
        return user;
    }

    public void setUser(TITUser user) 
    {
        this.user = user;
    }

    public RoomListOption getOption() 
    {
        return option;
    }

    public void setOption(RoomListOption option) 
    {
        this.option = option;
    }   
}
