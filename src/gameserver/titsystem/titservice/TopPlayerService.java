package gameserver.titsystem.titservice;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import gameserver.titsystem.titentities.TITSend;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;
import gameserver.titdatabase.TITDatabase;

public class TopPlayerService 
{
    private static JsonArray topPlayerJsonArray=new JsonArray();
    private static ArrayList<TITUser> userArr=new ArrayList<TITUser>();
    
    public static void register(TITUser user)
    {
        syncUserArrMethod("add",user);
    }
    
    public static void unregister(TITUser user)
    {
        syncUserArrMethod("remove",user);
    }
    
    public static void start()
    {
        while(true)
        {
            //System.out.println("Top Player Runing");
            topPlayerJsonArray=ranking();
            reloadTopPlayerNotify();
            try {Thread.sleep(ServerConfig.RELOAD_TOPPLAYER_TIME_MILLIS);} 
            catch (InterruptedException ex) {}
        }
    }
    
    private static JsonArray ranking()
    {
        TITDatabase database=TITDatabase.getInstance();
        JsonArray newTopPlayerJsonArray=new JsonArray();
        database.getTopPlayer(newTopPlayerJsonArray);
        for(int i=0;i<newTopPlayerJsonArray.size();i++)
        {
            JsonObject userInfoJson=
                                newTopPlayerJsonArray.get(i).getAsJsonObject();
            userInfoJson.addProperty(KJS.RANK_POSTION,i+1);
        }
        return newTopPlayerJsonArray;
    }
    
    private static void reloadTopPlayerNotify()
    {
        Thread thread=new Thread()
        {
            @Override
            public void run()
            {
                TITSend send=TITSend.getDefaultSend();
                JsonObject toClientData=new JsonObject();
                toClientData.add(KJS.ARRAY,topPlayerJsonArray);
                ArrayList<TITUser> receiveUserArrr=(ArrayList<TITUser>)
                                                syncUserArrMethod("clone",null);
                send.send(CMDNF.RELOADTOPPLAYER_NF,toClientData,receiveUserArrr);
            }
        };
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
    }
    
    private static Object syncUserArrMethod(String methodName,Object param1)
    {
        TITUser user;
        switch(methodName)
        {
            case "add":
                user=(TITUser) param1;
                return userArr.add(user);
            case "remove":
                user=(TITUser) param1;
                return userArr.remove(user);
            case "clone":
                return userArr.clone();
        }
        return null;
    }

    public static JsonArray getTopPlayerJsonArray() 
    {
        return topPlayerJsonArray;
    }
}
