package gameserver.titsystem.titservice.challenge;

import java.util.Date;
import gameserver.titsystem.titentities.user.TITUser;

public class PlayerToken 
{
    private Date joinTime;
    private TITUser user;
    
    public PlayerToken(Date joinTime,TITUser user) 
    {
        this.joinTime = joinTime;
        this.user=user;
    }

    public Date getJoinTime() 
    {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) 
    {
        this.joinTime = joinTime;
    }
    
    public int getRank() 
    {
        return user.getGameUserInfo().getRank();
    }

    public TITUser getUser() 
    {
        return user;
    }
    
    public boolean equals(Object object)
    {
        try
        {
            PlayerToken otherToken=(PlayerToken) object;
            return user.equals(otherToken.getUser());
        }
        catch(Exception ex)
        {
            return false;
        }
    }
}

