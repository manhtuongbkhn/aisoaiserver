package gameserver.titsystem.titservice.challenge;

import java.util.ArrayList;

public class TokenQueue 
{
    private ArrayList<ArrayList<PlayerToken>> tokenQueue;

    public TokenQueue(int rankCount) 
    {
        tokenQueue=new ArrayList<ArrayList<PlayerToken>>();
        for (int i = 0; i < rankCount; ++i) 
        {
            ArrayList tokenArr = new ArrayList();
            tokenQueue.add(tokenArr);
        }
    }

    public int getTokenCount() 
    {
        int count = 0;
        for (int i = 0; i < tokenQueue.size(); ++i) 
        {
            ArrayList<PlayerToken> tokenArr = tokenQueue.get(i);
            count += tokenArr.size();
        }
        return count;
    }

    public PlayerToken getToken(int index) 
    {
        ++index;
        int rowCount = tokenQueue.size();
        for (int row = 0; row < rowCount; ++row) 
        {
            ArrayList<PlayerToken> tokenArr = tokenQueue.get(row);
            int arrSize = tokenArr.size();
            if (index > arrSize) 
            {
                index -= arrSize;
                continue;
            }
            return tokenArr.get(index - 1);
        }
        return null;
    }

    public void addToken(PlayerToken token) 
    {
        int rank = token.getRank();
        ArrayList<PlayerToken> tokenArr = tokenQueue.get(rank);
        tokenArr.add(token);
    }

    public boolean removeToken(PlayerToken token) 
    {
        int rank = token.getRank();
        ArrayList<PlayerToken> tokenArr = tokenQueue.get(rank);
        return tokenArr.remove(token);
    }

    public void clear() 
    {
        for (int i = 0; i < tokenQueue.size();i++) 
        {
            ArrayList<PlayerToken> tokenArr = this.tokenQueue.get(i);
            tokenArr.clear();
        }
    }
}

