package gameserver.titsystem.titservice.challenge;

import java.util.ArrayList;
import java.util.Date;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.TITZoneExtension;

public class ChallengeService 
{
    private static ArrayList<ChallengeControl> controlArr;

    public static void init(TITZoneExtension zoneExtension) 
    {
        controlArr = new ArrayList();
        for (int playType = 1; playType <= 2; ++playType) 
        {
            for (int playerCount = 1; playerCount <= 4; ++playerCount) 
            {
                ChallengeControl control = 
                                    new ChallengeControl(playerCount,playType);
                controlArr.add(control);
            }
        }
        controlArr.get(0).init();
        controlArr.get(1).init();
    }

    public static int registerUser
                                (TITUser user, int playType, int playerCount) 
    {
        int index = (playType - 1) * 4 + playerCount - 1;
        ChallengeControl control = controlArr.get(index);
        PlayerToken token = new PlayerToken(new Date(), user);
        control.joinQueue(token);
        return control.getCurrentHandTime();
    }
                                    
    public static boolean unregisterUser
                                    (TITUser user,int playType,int playerCount)
    {
        int index = (playType - 1) * 4 + playerCount - 1;
        ChallengeControl control = controlArr.get(index);
        PlayerToken token = new PlayerToken(new Date(), user);
        return control.leaveQueue(token);
    }
}

