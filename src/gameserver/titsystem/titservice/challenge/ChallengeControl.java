
package gameserver.titsystem.titservice.challenge;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.tithandler.roomextension.playthread.ChallengeBeginRoomThread;
import gameserver.titsystem.titentities.TITApi;
import gameserver.titsystem.titentities.TITSend;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;
import gameserver.tithandler.roomextension.playthread.TITRoomService;
import gameserver.titconfig.CMDNF;
import gameserver.titconfig.KJS;
import gameserver.titconfig.ServerConfig;
import gameserver.titconfig.StrDefine;

public class ChallengeControl 
{
    private NotSyncHandlingTokenQueue notSyncHandlingTokenQueue;
    private Thread handlingThread;
    private int currentHandTimeMillis;
    
    public ChallengeControl(int playerCount,int playType) 
    {
        notSyncHandlingTokenQueue=
                            new NotSyncHandlingTokenQueue(playerCount,playType);
        currentHandTimeMillis=ServerConfig.CHALLENGE_HAND_TIME_MILLIS;
    }

    public void init() 
    {
        handlingThread = new Thread()
        {
            @Override
            public void run() 
            {
                int delayTime=ServerConfig.CHALLENGE_DELAY_TIME_MILLIS;
                int tokenMax=ServerConfig.CHALLENGE_TOKEN_MAX;
                do 
                {
                    int handTime=ServerConfig.CHALLENGE_HAND_TIME_MILLIS;
                    boolean sleepEnd = true;
                    for (int i=handTime;i >= 0;i=i-delayTime) 
                    {
                        try {Thread.sleep(delayTime);}catch (Exception ex) {}
                        if (getTokenCount()>=tokenMax)
                        {
                            currentHandTimeMillis=handTime-i;
                            handling();
                            sleepEnd = false;
                            break;
                        }
                    }
                    if (sleepEnd)
                    {
                        currentHandTimeMillis=handTime;
                        handling();
                    }
                } while (true);
            }
        };
        handlingThread.start();
    }


    public void joinQueue(PlayerToken token) 
    {
        notSyncHandlingTokenQueueMethod("joinQueue", token);
    }

    public boolean leaveQueue(PlayerToken token) 
    {
        return (boolean) notSyncHandlingTokenQueueMethod("leaveQueue", token);
    }

    public int getTokenCount() 
    {
        return (int)notSyncHandlingTokenQueueMethod("getTokenCount",null);
    }
    
    public void handling()
    {
        notSyncHandlingTokenQueueMethod("handling",null);
    }

    public int getCurrentHandTime()
    {
        return currentHandTimeMillis/1000;
    }
    
    private synchronized Object notSyncHandlingTokenQueueMethod
                                            (String methodName, Object param1) 
    {
        PlayerToken token;
        switch (methodName) 
        {
            case "joinQueue": 
                token = (PlayerToken)param1;
                notSyncHandlingTokenQueue.joinQueue(token);
                break;
            case "leaveQueue":
                token = (PlayerToken)param1;
                return notSyncHandlingTokenQueue.leaveQueue(token);
            case "getTokenCount":
                return notSyncHandlingTokenQueue.getTokenCount();
            case "handling":
                notSyncHandlingTokenQueue.handling();
                break;
        }
        return null;
    }
}

class NotSyncHandlingTokenQueue
{
    private int playerCount;
    private int playType;
    private TokenQueue tokenQueue = new TokenQueue(10);
    
    public NotSyncHandlingTokenQueue(int playerCount,int playType)
    {
        this.playerCount=playerCount;
        this.playType=playType;
    }
    
    protected void handling() 
    {
        while (tokenQueue.getTokenCount() >= playerCount) 
        {
            TITRoom room;
            switch (playerCount) 
            {
                case 1:
                    room = create1PlayerRoom();
                    break;
                case 2:
                    room = create2PlayerRoom();
                    break;
                case 3:
                    room = create3PlayerRoom();
                    break;
                default:
                    room = create4PlayerRoom();
            }
            room.getDefaultRoomInfo().setOwner(null);
            room.getDefaultRoomInfo().setPlayType(playType);
            room.getDefaultRoomInfo().setInvite
                            ("Ch\u00fac tr\u1eadn \u0111\u1ea5u vui v\u1ebb!");
            room.getDefaultRoomInfo().setAvatar(3);
            TITRoomService roomService=new TITRoomService(room);
            ChallengeBeginRoomThread beginRoomThread = 
                                    new ChallengeBeginRoomThread(roomService);
            beginRoomThread.start();
        }
    }

    protected void joinQueue(PlayerToken challengeToken)
    {
        tokenQueue.addToken(challengeToken);
    }
    
    protected boolean leaveQueue(PlayerToken challengeToken)
    {
        return tokenQueue.removeToken(challengeToken);
    }
    
    protected int getTokenCount()
    {
        return tokenQueue.getTokenCount();
    }
    
    private TITRoom create1PlayerRoom() 
    {
        SFSObject toClientData = new SFSObject();
        PlayerToken token1 = (PlayerToken) tokenQueue.getToken(0);
        TITUser user1 = token1.getUser();
        TITRoom room = getApi().createRoom(2, "", 1, user1);
        if (room == null) 
        {
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,StrDefine.SYSTEM_ERROR);
        }
        toClientData.putBool(KJS.SUCESS, true);
        tokenQueue.removeToken(token1);
        send(CMDNF.CHALLENGE_NF, toClientData, user1);
        return room;
    }

    private TITRoom create2PlayerRoom() 
    {
        SFSObject toClientData = new SFSObject();
        PlayerToken token1 = tokenQueue.getToken(0);
        PlayerToken token2 = tokenQueue.getToken(1);
        TITUser user1 = token1.getUser();
        TITUser user2 = token2.getUser();
        TITRoom room = getApi().createRoom(2, "", 2, user1);
        if (room == null) 
        {
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                        StrDefine.SYSTEM_ERROR);
        } 
        else 
        {
            boolean sucess = getApi().joinRoom(user2, room, "");
            if (sucess) 
            {
                toClientData.putBool(KJS.SUCESS, true);
            } 
            else 
            {
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                        StrDefine.SYSTEM_ERROR);
            }
        }
        tokenQueue.removeToken(token1);
        tokenQueue.removeToken(token2);
        send(CMDNF.CHALLENGE_NF, toClientData, user1);
        send(CMDNF.CHALLENGE_NF, toClientData, user2);
        return room;
    }

    private TITRoom create3PlayerRoom() 
    {
        SFSObject toClientData = new SFSObject();
        PlayerToken token1 = tokenQueue.getToken(0);
        PlayerToken token2 = tokenQueue.getToken(1);
        PlayerToken token3 = tokenQueue.getToken(2);
        TITUser user1 = token1.getUser();
        TITUser user2 = token2.getUser();
        TITUser user3 = token3.getUser();
        TITRoom room = getApi().createRoom(2, "", 3, user1);
        if (room == null) 
        {
            toClientData.putBool(KJS.SUCESS, false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                        StrDefine.SYSTEM_ERROR);
        } 
        else 
        {
            boolean sucess1 = getApi().joinRoom(user2, room, "");
            boolean sucess2 = getApi().joinRoom(user3, room, "");
            if (sucess1 && sucess2) 
            {
                toClientData.putBool(KJS.SUCESS, true);
            } 
            else 
            {
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                    StrDefine.SYSTEM_ERROR);
            }
        }
        tokenQueue.removeToken(token1);
        tokenQueue.removeToken(token2);
        tokenQueue.removeToken(token3);
        send(CMDNF.CHALLENGE_NF,toClientData, user1);
        send(CMDNF.CHALLENGE_NF,toClientData, user2);
        send(CMDNF.CHALLENGE_NF,toClientData, user3);
        return room;
    }

    private TITRoom create4PlayerRoom() 
    {
        SFSObject toClientData = new SFSObject();
        PlayerToken token1 = tokenQueue.getToken(0);
        PlayerToken token2 = tokenQueue.getToken(1);
        PlayerToken token3 = tokenQueue.getToken(2);
        PlayerToken token4 = tokenQueue.getToken(3);
        TITUser user1 = token1.getUser();
        TITUser user2 = token2.getUser();
        TITUser user3 = token3.getUser();
        TITUser user4 = token4.getUser();
        TITRoom room = this.getApi().createRoom(2, "", 4, user1);
        if (room == null) 
        {
            toClientData.putBool(KJS.SUCESS,false);
            toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                        StrDefine.SYSTEM_ERROR);
        } 
        else 
        {
            boolean sucess1 = getApi().joinRoom(user2, room, "");
            boolean sucess2 = getApi().joinRoom(user3, room, "");
            boolean sucess3 = getApi().joinRoom(user4, room, "");
            if (sucess1 && sucess2 && sucess3) 
            {
                toClientData.putBool(KJS.SUCESS, true);
            } 
            else 
            {
                toClientData.putBool(KJS.SUCESS, false);
                toClientData.putUtfString(KJS.SYSTEM_MESSAGE,
                                                        StrDefine.SYSTEM_ERROR);
            }
        }
        tokenQueue.removeToken(token1);
        tokenQueue.removeToken(token2);
        tokenQueue.removeToken(token3);
        tokenQueue.removeToken(token4);
        send(CMDNF.CHALLENGE_NF, toClientData, user1);
        send(CMDNF.CHALLENGE_NF, toClientData, user2);
        send(CMDNF.CHALLENGE_NF, toClientData, user3);
        send(CMDNF.CHALLENGE_NF, toClientData, user4);
        return room;
    }

    public int getPlayerCount() 
    {
        return this.playerCount;
    }

    public void setPlayerCount(int playerCount) 
    {
        this.playerCount = playerCount;
    }

    public int getPlayType() 
    {
        return this.playType;
    }

    public void setPlayType(int playType) 
    {
        this.playType = playType;
    }

    public TITApi getApi() 
    {
        return TITApi.getDefaultApi();
    }

    public void send(String CMD, SFSObject data, TITUser user) 
    {
        TITSend send=TITSend.getDefaultSend();
        send.send(CMD, data, user);
    }

    public void trace(String message) 
    {
        System.out.println(message);
    }
    
    
}