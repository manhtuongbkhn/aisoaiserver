/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonObject
 */
package gameserver.titdatabase;

import com.google.gson.JsonObject;
import gameserver.titdatabase.LoginHistoryTable;
import gameserver.titdatabase.UserProfileTable;

public interface ITITDatabse {
    public UserProfileTable getUserProfileTb();

    public LoginHistoryTable getLoginHistoryTb();

    public JsonObject checkIdFacebookExist(JsonObject var1);

    public JsonObject checkAccessTokenExist(JsonObject var1);

    public JsonObject getNewIdUser(JsonObject var1);

    public JsonObject updateProfile(JsonObject var1);
}

