package gameserver.titdatabase;

import com.google.gson.JsonObject;
import java.sql.Connection;

public abstract class TITTable 
{
    protected Connection connection;

    public TITTable(Connection connection) 
    {
        this.connection = connection;
    }

    public abstract JsonObject insertRecord(JsonObject jsonData);

    public abstract JsonObject getRecord(JsonObject jsonData);
}

