package gameserver.titdatabase;

import gameserver.titconfig.TITDatabaseConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import gameserver.titconfig.KJS;

public class LoginHistoryTable extends TITTable 
{
    public LoginHistoryTable(Connection connection) 
    {
        super(connection);
    }

    @Override
    public JsonObject insertRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.LOGIN_HISTORY_INSERT_RECORD;
        try 
        {
            String userId = jsonData.get(KJS.USER_ID).getAsString();
            String loginTime = jsonData.get(KJS.LOGIN_TIME).getAsString();
            String logoutTime = jsonData.get(KJS.LOGOUT_TIME).getAsString();
            String accessToken = jsonData.get(KJS.ACCESS_TOKEN).getAsString();
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, userId);
            stt.setString(2, loginTime);
            stt.setString(3, logoutTime);
            stt.setString(4, accessToken);
            stt.execute();
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (SQLException ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }

    @Override
    public JsonObject getRecord(JsonObject input) 
    {
        String sql = TITDatabaseConfig.LOGIN_HISTORY_GET_RECORD;
        JsonObject jsonObj = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        String idUser = input.get(KJS.USER_ID).getAsString();
        try {
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, idUser);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) {
                rs.beforeFirst();
                while (rs.next()) 
                {
                    JsonObject tmp = new JsonObject();
                    tmp.addProperty(KJS.USER_ID,
                                                rs.getString(KJS.USER_ID));
                    tmp.addProperty(KJS.LOGIN_TIME,
                                            rs.getString(KJS.LOGIN_TIME));
                    tmp.addProperty(KJS.LOGOUT_TIME,
                                            rs.getString(KJS.LOGOUT_TIME));
                    tmp.addProperty(KJS.ACCESS_TOKEN,
                                            rs.getString(KJS.ACCESS_TOKEN));
                    jsonArray.add((JsonElement)tmp);
                }
                jsonObj.add(KJS.DATA,jsonArray);
            }
            jsonObj.addProperty(KJS.SUCESS,true);
        }
        catch (SQLException ex) 
        {
            jsonObj.addProperty(KJS.SUCESS,false);
            jsonObj.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonObj;
    }

    public JsonObject getRecordAndAccessToken(JsonObject input) 
    {
        String sql = TITDatabaseConfig.LOGIN_HISTORY_GET_RECORD_ACCESSTOKEN;
        JsonObject jsonObj = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        String idUser = input.get(KJS.USER_ID).getAsString();
        String accessToken = input.get(KJS.ACCESS_TOKEN).getAsString();
        try 
        {
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, idUser);
            stt.setString(2, accessToken);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) 
            {
                rs.beforeFirst();
                while (rs.next()) 
                {
                    JsonObject tmp = new JsonObject();
                    tmp.addProperty(KJS.USER_ID, rs.getString(KJS.USER_ID));
                    tmp.addProperty(KJS.LOGIN_TIME,
                                                rs.getString(KJS.LOGIN_TIME));
                    tmp.addProperty(KJS.LOGOUT_TIME,
                                                rs.getString(KJS.LOGOUT_TIME));
                    tmp.addProperty(KJS.ACCESS_TOKEN,
                                            rs.getString(KJS.ACCESS_TOKEN));
                    jsonArray.add((JsonElement)tmp);
                }
            }
            jsonObj.add(KJS.DATA,jsonArray);
            jsonObj.addProperty(KJS.SUCESS,true);
        }
        catch (SQLException ex) 
        {
            jsonObj.addProperty(KJS.SUCESS,false);
            jsonObj.addProperty(KJS.EXCEPTION,ex.toString());
        }
        return jsonObj;
    }
}

