package gameserver.titdatabase;

import gameserver.titconfig.TITDatabaseConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import gameserver.titconfig.KJS;

class UserProfileTable extends TITTable 
{
    public UserProfileTable(Connection connection) 
    {
        super(connection);
    }

    @Override
    public JsonObject insertRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_INSERT_SQL;
        try 
        {
            String idUser = jsonData.get(KJS.USER_ID).getAsString();
            String idFacebook = jsonData.get(KJS.FACEBOOK_ID).getAsString();
            String fullName = jsonData.get(KJS.FULL_NAME).getAsString();
            String email = jsonData.get(KJS.EMAIL).getAsString();
            String birthday = jsonData.get(KJS.BIRTH_DAY).getAsString();
            String gender = jsonData.get(KJS.GENDER).getAsString();
            String urlFacebook = jsonData.get(KJS.FACEBOOK_URL).getAsString();
            String urlAvatar = jsonData.get(KJS.AVATAR_URL).getAsString();
            boolean online = jsonData.get(KJS.ONLINE).getAsBoolean();
            int systemUserId = jsonData.get(KJS.SYSTEM_USER_ID).getAsInt();
            String dateRegister = jsonData.get(KJS.REGISTER_DATE).getAsString();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, idUser);
            stt.setString(2, idFacebook);
            stt.setString(3, fullName);
            stt.setString(4, email);
            stt.setString(5, birthday);
            stt.setString(6, gender);
            stt.setString(7, urlFacebook);
            stt.setString(8, urlAvatar);
            stt.setBoolean(9, online);
            stt.setInt(10, systemUserId);
            stt.setString(11, dateRegister);
            stt.execute();
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION,ex.toString());
        }
        return jsonData;
    }

    @Override
    public JsonObject getRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_GET_SQL;
        try 
        {
            jsonData.addProperty(KJS.RESULT,false);
            String userId = jsonData.get(KJS.USER_ID).getAsString();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, userId);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) {
                rs.beforeFirst();
                while (rs.next()) 
                {
                    jsonData.addProperty(KJS.USER_ID, rs.getString(KJS.USER_ID));
                    jsonData.addProperty(KJS.FACEBOOK_ID, 
                                                rs.getString(KJS.FACEBOOK_ID));
                    jsonData.addProperty(KJS.FULL_NAME,
                                                rs.getString(KJS.FULL_NAME));
                    jsonData.addProperty(KJS.EMAIL, rs.getString(KJS.EMAIL));
                    jsonData.addProperty(KJS.BIRTH_DAY,
                                                rs.getString(KJS.BIRTH_DAY));
                    jsonData.addProperty(KJS.GENDER, rs.getString(KJS.GENDER));
                    jsonData.addProperty(KJS.FACEBOOK_URL,
                                                rs.getString(KJS.FACEBOOK_URL));
                    jsonData.addProperty(KJS.AVATAR_URL,
                                                rs.getString(KJS.AVATAR_URL));
                    jsonData.addProperty(KJS.ONLINE,rs.getBoolean(KJS.ONLINE));
                    jsonData.addProperty(KJS.SYSTEM_USER_ID,
                                                rs.getInt(KJS.SYSTEM_USER_ID));
                    jsonData.addProperty(KJS.REGISTER_DATE,
                                            rs.getString(KJS.REGISTER_DATE));
                    jsonData.addProperty(KJS.RESULT,false);
                }
            }
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }

    public JsonObject getRecordWithIdFb(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_GET_BYFB_SQL;
        try 
        {
            jsonData.addProperty(KJS.RESULT,false);
            String facebookId = jsonData.get(KJS.FACEBOOK_ID).getAsString();
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, facebookId);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) 
            {
                rs.beforeFirst();
                while (rs.next())
                {
                    jsonData.addProperty(KJS.USER_ID, rs.getString(KJS.USER_ID));
                    jsonData.addProperty(KJS.FACEBOOK_ID, 
                                                rs.getString(KJS.FACEBOOK_ID));
                    jsonData.addProperty(KJS.FULL_NAME, 
                                                rs.getString(KJS.FULL_NAME));
                    jsonData.addProperty(KJS.EMAIL, rs.getString(KJS.EMAIL));
                    jsonData.addProperty(KJS.BIRTH_DAY,
                                                rs.getString(KJS.BIRTH_DAY));
                    jsonData.addProperty(KJS.GENDER, rs.getString(KJS.GENDER));
                    jsonData.addProperty(KJS.FACEBOOK_URL,
                                            rs.getString(KJS.FACEBOOK_URL));
                    jsonData.addProperty(KJS.AVATAR_URL,
                                                rs.getString(KJS.AVATAR_URL));
                    jsonData.addProperty(KJS.ONLINE,rs.getBoolean(KJS.ONLINE));
                    jsonData.addProperty(KJS.SYSTEM_USER_ID,
                                                rs.getInt(KJS.SYSTEM_USER_ID));
                    jsonData.addProperty(KJS.REGISTER_DATE,
                                            rs.getString(KJS.REGISTER_DATE));
                    jsonData.addProperty(KJS.RESULT,true);
                }
            }
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }

    public JsonObject getMaxUserId(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_GET_MAXUSERID_SQL;
        try 
        {
            PreparedStatement stt = this.connection.prepareStatement(sql);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) 
            {
                rs.beforeFirst();
                while (rs.next()) 
                {
                    jsonData.addProperty(KJS.RESULT, rs.getString(KJS.USER_ID));
                }
            }
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (SQLException ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }

    public JsonObject updateFieldsOnline(JsonObject input) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_UPDATE_ONLINE;
        JsonObject jsonObj = new JsonObject();
       
        try 
        {
            String userId = input.get(KJS.USER_ID).getAsString();
            boolean online = input.get(KJS.ONLINE).getAsBoolean();
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setBoolean(1, online);
            stt.setString(2, userId);
            stt.execute();
            jsonObj.addProperty(KJS.SUCESS,true);
        }
        catch (SQLException ex) 
        {
            jsonObj.addProperty(KJS.SUCESS,false);
            jsonObj.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonObj;
    }

    public JsonObject updateFieldsSystemUserId(JsonObject input) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_UPDATE_SYSTEMUSERID;
        JsonObject jsonObj = new JsonObject();
        try 
        {
            String userId = input.get(KJS.USER_ID).getAsString();
            int systemUserId = input.get(KJS.SYSTEM_USER_ID).getAsInt();
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setInt(1, systemUserId);
            stt.setString(2, userId);
            stt.execute();
            jsonObj.addProperty(KJS.SUCESS,true);
        }
        catch (SQLException ex) 
        {
            jsonObj.addProperty(KJS.SUCESS,false);
            jsonObj.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonObj;
    }

    public JsonObject updateRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_PROFILE_UPDATE_RECORD;
        try 
        {
            String userId = jsonData.get(KJS.USER_ID).getAsString();
            String facebookId = jsonData.get(KJS.FACEBOOK_ID).getAsString();
            String fullName = jsonData.get(KJS.FULL_NAME).getAsString();
            String email = jsonData.get(KJS.EMAIL).getAsString();
            String birthDay = jsonData.get(KJS.BIRTH_DAY).getAsString();
            String gender = jsonData.get(KJS.GENDER).getAsString();
            String facebook_url = jsonData.get(KJS.FACEBOOK_URL).getAsString();
            String avatar_url = jsonData.get(KJS.AVATAR_URL).getAsString();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, facebookId);
            stt.setString(2, fullName);
            stt.setString(3, email);
            stt.setString(4, birthDay);
            stt.setString(5, gender);
            stt.setString(6, facebook_url);
            stt.setString(7, avatar_url);
            stt.setString(8, userId);
            stt.execute();
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
        }
        return jsonData;
    }
    
    public JsonObject getTop10Player(JsonObject jsonData)
    {
        String sql=TITDatabaseConfig.TOP_USER_GET_SQL;
        JsonArray jsonArray=new JsonArray();
        JsonObject userInfoJson;
        try 
        {
            PreparedStatement stt = this.connection.prepareStatement(sql);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) 
            {
                rs.beforeFirst();
                while (rs.next())
                {
                    userInfoJson=new JsonObject();
                    userInfoJson.addProperty(KJS.USER_ID, rs.getString(KJS.USER_ID));
                    userInfoJson.addProperty(KJS.FACEBOOK_ID, 
                                                rs.getString(KJS.FACEBOOK_ID));
                    userInfoJson.addProperty(KJS.FULL_NAME, 
                                                rs.getString(KJS.FULL_NAME));
                    userInfoJson.addProperty(KJS.EMAIL, rs.getString(KJS.EMAIL));
                    userInfoJson.addProperty(KJS.BIRTH_DAY,
                                                rs.getString(KJS.BIRTH_DAY));
                    userInfoJson.addProperty(KJS.GENDER, rs.getString(KJS.GENDER));
                    userInfoJson.addProperty(KJS.FACEBOOK_URL,
                                            rs.getString(KJS.FACEBOOK_URL));
                    userInfoJson.addProperty(KJS.AVATAR_URL,
                                                rs.getString(KJS.AVATAR_URL));
                    userInfoJson.addProperty(KJS.ONLINE,rs.getBoolean(KJS.ONLINE));
                    userInfoJson.addProperty(KJS.SYSTEM_USER_ID,
                                                rs.getInt(KJS.SYSTEM_USER_ID));
                    userInfoJson.addProperty(KJS.REGISTER_DATE,
                                            rs.getString(KJS.REGISTER_DATE));
                    userInfoJson.addProperty(KJS.RESULT,true);
                    jsonArray.add(userInfoJson);
                }
            }
            jsonData.add(KJS.ARRAY,jsonArray);
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }
}

