package gameserver.titdatabase;

import gameserver.titconfig.TITDatabaseConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import gameserver.titconfig.KJS;

public class UserGameMaturityTable extends TITTable
{

    public UserGameMaturityTable(Connection connection) 
    {
        super(connection);
    }

    @Override
    public JsonObject insertRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_GAME_MATURITY_INSERT_SQL;
        try 
        {
            String userId=jsonData.get(KJS.USER_ID).getAsString();
            int gameId=jsonData.get(KJS.GAME_ID).getAsInt();
            int gameMaturity=jsonData.get
                                    (KJS.GAME_MATURITY).getAsInt();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1,userId);
            stt.setInt(2,gameId);
            stt.setInt(3,gameMaturity);
            stt.execute();
            jsonData.addProperty(KJS.SUCESS,true);
        } catch (SQLException ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION,ex.toString());
        }
        return jsonData;
    }

    @Override
    public JsonObject getRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_GAME_MATURITY_GET_SQL;
        try 
        {
            JsonArray  jsonArray=new JsonArray();
            JsonObject tmp;
            jsonData.addProperty(KJS.RESULT,false);
            String userId = jsonData.get(KJS.USER_ID).getAsString();
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, userId);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) 
            {
                rs.beforeFirst();
                while (rs.next()) 
                {
                    tmp=new JsonObject();
                    tmp.addProperty(KJS.USER_ID,rs.getString(KJS.USER_ID));
                    tmp.addProperty(KJS.GAME_ID,rs.getInt(KJS.GAME_ID));
                    tmp.addProperty(KJS.GAME_MATURITY,
                                            rs.getInt(KJS.GAME_MATURITY));
                    jsonArray.add(tmp);
                }
            }
            jsonData.add(KJS.GAME_MATURITY_ARR,jsonArray);
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }
    
    public JsonObject updateRecord(JsonObject jsonData)
    {
        
        String sql = TITDatabaseConfig.USER_GAME_MATURITY_UPDATE_SQL;
        try 
        {
            String userId=jsonData.get(KJS.USER_ID).getAsString();
            int gameId=jsonData.get(KJS.GAME_ID).getAsInt();
            int gameMaturity=jsonData.get(KJS.GAME_MATURITY).getAsInt();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setInt(1,gameMaturity);
            stt.setString(2,userId);
            stt.setInt(3,gameId);
            stt.execute();
            
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
        }
        return jsonData;
    }
}
