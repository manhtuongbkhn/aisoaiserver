package gameserver.titdatabase;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import gameserver.titsystem.titentities.user.TITGameUserInfo;
import gameserver.titsystem.titentities.user.TITUserProfile;
import gameserver.titconfig.KJS;
import gameserver.titgameinfo.TITGameInfoFactory;

public class TITDatabase implements ITITDatabse
{
    private Connection connection;
    private static TITDatabase titDatabase;
    private UserProfileTable userProfileTb;
    private GameUserInfoTable gameUserInfoTb;
    private UserGameMaturityTable userGameMaturityTb;
    private LoginHistoryTable loginHistoryTb;

    private TITDatabase(Connection connection) 
    {
        this.connection = connection;
        userProfileTb = new UserProfileTable(connection);
        gameUserInfoTb=new GameUserInfoTable(connection);
        userGameMaturityTb=new UserGameMaturityTable(connection);
        loginHistoryTb = new LoginHistoryTable(connection);
    }

    public static TITDatabase getInstance(Connection connection) 
    {
        if(titDatabase == null) 
            titDatabase = new TITDatabase(connection);

        return titDatabase;
    }

    public static TITDatabase getInstance() 
    {
        return titDatabase;
    }

    public UserProfileTable getUserProfileTb() 
    {
        return userProfileTb;
    }

    public LoginHistoryTable getLoginHistoryTb() 
    {
        return loginHistoryTb;
    }

    public JsonObject checkIdFacebookExist(JsonObject jsonData) 
    {
        userProfileTb.getRecordWithIdFb(jsonData);
        return jsonData;
    }

    public JsonObject checkAccessTokenExist(JsonObject jsonData) 
    {
        JsonObject jsonObj = loginHistoryTb.getRecordAndAccessToken(jsonData);
        if(jsonObj.get(KJS.SUCESS).getAsBoolean()) 
        {
            JsonArray exception = jsonObj.getAsJsonArray(KJS.DATA);
            if(exception.size() != 0) 
                jsonData.addProperty(KJS.RESULT,true);
            else 
                jsonData.addProperty(KJS.RESULT,false);

            jsonData.addProperty(KJS.SUCESS,true);
        } 
        else 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            String exception1 = jsonObj.get(KJS.EXCEPTION).getAsString();
            jsonData.addProperty(KJS.EXCEPTION, exception1);
        }

        return jsonData;
    }

    public JsonObject getNewIdUser(JsonObject jsonData) 
    {
        userProfileTb.getMaxUserId(jsonData);
        if(jsonData.get(KJS.SUCESS).getAsBoolean()) 
        {
            String maxIdUserStr = jsonData.get(KJS.RESULT).getAsString();
            Integer maxIdUserInt = new Integer(maxIdUserStr);
            Integer newIdUserInt = Integer.valueOf(maxIdUserInt.intValue() + 1);
            String newIdUserStr = newIdUserInt.toString();
            int len = newIdUserStr.length();

            for(int i = 0; i < 8 - len; ++i) 
            {
                newIdUserStr = "0".concat(newIdUserStr);
            }

            jsonData.addProperty(KJS.USER_ID, newIdUserStr);
            jsonData.addProperty(KJS.SUCESS,true);
        } 
        else 
            jsonData.addProperty(KJS.SUCESS,false);
        return jsonData;
    }

    public JsonObject registerUser(JsonObject jsonData) 
    {
        getNewIdUser(jsonData);
        if(jsonData.get(KJS.SUCESS).getAsBoolean()) 
        {
            insertUserProfile(jsonData);
            if(jsonData.get(KJS.SUCESS).getAsBoolean()) 
            {
                insertGameUserInfo(jsonData);
                if(jsonData.get(KJS.SUCESS).getAsBoolean())
                {
                    insertUserGameMaturity(jsonData);
                    if(jsonData.get(KJS.SUCESS).getAsBoolean())
                    {
                        return insertLoginHistory(jsonData);
                    }
                }       
            }
        }
        return jsonData;
    }
    
    private JsonObject insertUserProfile(JsonObject jsonData)
    {
        boolean online = true;
        Date registerDate = new Date();
        String registerDateStr = registerDate.toLocaleString();
        jsonData.addProperty(KJS.ONLINE,online);
        jsonData.addProperty(KJS.SYSTEM_USER_ID,-1);
        jsonData.addProperty(KJS.REGISTER_DATE, registerDateStr);
        userProfileTb.insertRecord(jsonData);
        return jsonData;
    }
    
    private JsonObject insertGameUserInfo(JsonObject jsonData)
    {
        jsonData.addProperty(KJS.NMATCH_COUNT,0);
        jsonData.addProperty(KJS.WIN_NMATCH_COUNT,0);
        jsonData.addProperty(KJS.RMATCH_COUNT,0);
        jsonData.addProperty(KJS.WIN_RMATCH_COUNT,0);
        jsonData.addProperty(KJS.EXPERIENCE_POINT,0);
        jsonData.addProperty(KJS.RANK_POINT,0);
        jsonData.addProperty(KJS.AFKMATCH_COUNT,0);
        
        jsonData.addProperty(KJS.SPEED,20);
        jsonData.addProperty(KJS.OBSERVE,20);
        jsonData.addProperty(KJS.CALCULATE,20);
        jsonData.addProperty(KJS.MEMORY,20);
        jsonData.addProperty(KJS.LOGIC,20);
        jsonData.addProperty(KJS.REFLEX,20);
        gameUserInfoTb.insertRecord(jsonData);
        return jsonData;
    }
    
    private JsonObject insertUserGameMaturity(JsonObject jsonData)
    {
        JsonArray gameMaturityArr=new JsonArray();
        ArrayList<Integer> freeGameIdArr=TITGameInfoFactory.getFreeGameIdArr();
        String userId=jsonData.get(KJS.USER_ID).getAsString();
        
        for(int i=0;i<freeGameIdArr.size();i++)
        {
            int gameId=freeGameIdArr.get(i);
            JsonObject input=new JsonObject();
            input.addProperty(KJS.USER_ID,userId);
            input.addProperty(KJS.GAME_ID,gameId);
            input.addProperty(KJS.GAME_MATURITY,0);
            userGameMaturityTb.insertRecord(input);
            if(!input.get(KJS.SUCESS).getAsBoolean())
            {
                jsonData.addProperty(KJS.SUCESS,false);
                break;
            }
            gameMaturityArr.add(input);
        }
        jsonData.addProperty(KJS.SUCESS,true);
        jsonData.add(KJS.GAME_MATURITY_ARR,gameMaturityArr);
        return jsonData;
    }
    
    private JsonObject insertLoginHistory(JsonObject jsonData)
    {
        Date timeLogin = new Date();
        jsonData.addProperty(KJS.LOGIN_TIME, timeLogin.toLocaleString());
        jsonData.addProperty(KJS.LOGOUT_TIME,"");
        loginHistoryTb.insertRecord(jsonData);
        return jsonData;
    }
    
    public JsonObject updateProfile(JsonObject jsonData) 
    {
        
        return userProfileTb.updateRecord(jsonData);
    }

    public JsonObject updateOnline(JsonObject input) 
    {
        return userProfileTb.updateFieldsOnline(input);
    }

    public JsonObject updateSystemUserId(JsonObject input) 
    {
        return userProfileTb.updateFieldsSystemUserId(input);
    }
    
    public JsonObject getFriendInfoByFacebookId(JsonObject jsonData)
    {
        JsonObject input=new JsonObject();
        String facebookId=jsonData.get(KJS.FACEBOOK_ID).getAsString();
        input.addProperty(KJS.FACEBOOK_ID,facebookId);
        input=userProfileTb.getRecordWithIdFb(input);
        if(input.get(KJS.SUCESS).getAsBoolean()&&
                                        input.get(KJS.RESULT).getAsBoolean())
        {
            boolean online=input.get(KJS.ONLINE).getAsBoolean();
            int systemUserId=input.get(KJS.SYSTEM_USER_ID).getAsInt();
            String userId=input.get(KJS.USER_ID).getAsString();
            String registerDate=input.get(KJS.REGISTER_DATE).getAsString();
            
            jsonData.addProperty(KJS.ONLINE,online);
            jsonData.addProperty(KJS.SYSTEM_USER_ID,systemUserId);
            jsonData.addProperty(KJS.USER_ID,userId);
            jsonData.addProperty(KJS.REGISTER_DATE,registerDate);
            jsonData.addProperty(KJS.SUCESS,true);
            jsonData.addProperty(KJS.RESULT,true);
            getUserGameInfo(jsonData);
        }
        else
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.RESULT,false);
        }
        return jsonData;
    }
    
    public JsonObject getUserProfileByFacebookId(JsonObject jsonData)
    {
        return userProfileTb.getRecordWithIdFb(jsonData);
    }
    
    
    public JsonObject getUserGameInfo(JsonObject jsonData)
    {
        gameUserInfoTb.getRecord(jsonData);
        if(jsonData.get(KJS.SUCESS).getAsBoolean()&&
                                    jsonData.get(KJS.RESULT).getAsBoolean())
        {
            JsonObject input=new JsonObject();
            String userId=jsonData.get(KJS.USER_ID).getAsString();
            input.addProperty(KJS.USER_ID,userId);          
            userGameMaturityTb.getRecord(input);
            if(input.get(KJS.SUCESS).getAsBoolean())
            {
                JsonArray gameMaturityJsonArr=
                                input.get(KJS.GAME_MATURITY_ARR).getAsJsonArray();
                jsonData.add(KJS.GAME_MATURITY_ARR,gameMaturityJsonArr);
                jsonData.addProperty(KJS.SUCESS,true);
                jsonData.addProperty(KJS.RESULT,true);
            }
            else
                jsonData.addProperty(KJS.SUCESS,false);
        }
        return jsonData;
    }
    
    public JsonArray getTopPlayer(JsonArray jsonArray)
    {
        JsonObject jsonData=new JsonObject();
        TITDatabase.getInstance().userProfileTb.getTop10Player(jsonData);
        
        JsonArray newJsonArray=jsonData.get(KJS.ARRAY).getAsJsonArray();
        for(int i=0;i<newJsonArray.size();i++)
        {
            JsonObject userInfoJson=newJsonArray.get(i).getAsJsonObject();
            getUserGameInfo(userInfoJson);
            if(userInfoJson.get(KJS.SUCESS).getAsBoolean()&&
                                userInfoJson.get(KJS.RESULT).getAsBoolean())
            {
                TITUserProfile userProfile=new TITUserProfile();
                userProfile.setData(userInfoJson);
                TITGameUserInfo gameUserInfo=new TITGameUserInfo();
                gameUserInfo.setData(userInfoJson);
                
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.SYSTEM_USER_ID,-1);
                userProfile.getPublicInfo(jsonObject);
                gameUserInfo.getPublicInfo(jsonObject);
                jsonArray.add(jsonObject);
            }
        }
        //System.out.println(jsonArray);
        return jsonArray;
    }
}
