package gameserver.titdatabase;

import gameserver.titconfig.TITDatabaseConfig;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import gameserver.titconfig.KJS;

public class GameUserInfoTable extends TITTable
{
    public GameUserInfoTable(Connection connection) 
    {
        super(connection);
    }

    @Override
    public JsonObject insertRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_GAMEINFO_INSERT_SQL;
        try 
        {
            String userId=jsonData.get(KJS.USER_ID).getAsString();
            int nMatchCount=jsonData.get(KJS.NMATCH_COUNT).getAsInt();
            int winNMatchCount=jsonData.get(KJS.WIN_NMATCH_COUNT).getAsInt();
            int rMatchCount=jsonData.get(KJS.RMATCH_COUNT).getAsInt();
            int winRMatchCount=jsonData.get(KJS.WIN_RMATCH_COUNT).getAsInt();
            int experiencePoint=jsonData.get(KJS.EXPERIENCE_POINT).getAsInt();
            int rankPoint=jsonData.get(KJS.RANK_POINT).getAsInt();
            int afkmatchCount=jsonData.get(KJS.AFKMATCH_COUNT).getAsInt();
            int speed=jsonData.get(KJS.SPEED).getAsInt();
            int observe=jsonData.get(KJS.OBSERVE).getAsInt();
            int calculate=jsonData.get(KJS.CALCULATE).getAsInt();
            int memory=jsonData.get(KJS.MEMORY).getAsInt();
            int logic=jsonData.get(KJS.LOGIC).getAsInt();
            int reflex=jsonData.get(KJS.REFLEX).getAsInt();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1,userId);
            stt.setInt(2,nMatchCount);
            stt.setInt(3,winNMatchCount);
            stt.setInt(4,rMatchCount);
            stt.setInt(5,winRMatchCount);
            stt.setInt(6,experiencePoint);
            stt.setInt(7,rankPoint);
            stt.setInt(8,afkmatchCount);
            stt.setInt(9,speed);
            stt.setInt(10,observe);
            stt.setInt(11,calculate);
            stt.setInt(12,memory);
            stt.setInt(13,logic);
            stt.setInt(14,reflex);
            stt.execute();
            jsonData.addProperty(KJS.SUCESS,true);
        } catch (SQLException ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION,ex.toString());
        }
        return jsonData;
    }

    @Override
    public JsonObject getRecord(JsonObject jsonData) 
    {
        String sql = TITDatabaseConfig.USER_GAMEINFO_GET_SQL;
        try 
        {
            jsonData.addProperty(KJS.RESULT,false);
            String userId = jsonData.get(KJS.USER_ID).getAsString();
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setString(1, userId);
            ResultSet rs = stt.executeQuery();
            if (rs.first()) 
            {
                rs.beforeFirst();
                while (rs.next()) 
                {
                    jsonData.addProperty(KJS.USER_ID, rs.getString(KJS.USER_ID));
                    jsonData.addProperty(KJS.NMATCH_COUNT,
                                                rs.getInt(KJS.NMATCH_COUNT));
                    jsonData.addProperty(KJS.WIN_NMATCH_COUNT,
                                            rs.getInt(KJS.WIN_NMATCH_COUNT));
                    jsonData.addProperty(KJS.RMATCH_COUNT,
                                                rs.getInt(KJS.RMATCH_COUNT));
                    jsonData.addProperty(KJS.WIN_RMATCH_COUNT,
                                            rs.getInt(KJS.WIN_RMATCH_COUNT));
                    jsonData.addProperty(KJS.EXPERIENCE_POINT,
                                            rs.getInt(KJS.EXPERIENCE_POINT));
                    jsonData.addProperty(KJS.RANK_POINT,
                                                rs.getInt(KJS.RANK_POINT));
                    jsonData.addProperty(KJS.AFKMATCH_COUNT,
                                                rs.getInt(KJS.AFKMATCH_COUNT));
                    jsonData.addProperty(KJS.SPEED,rs.getInt(KJS.SPEED));
                    jsonData.addProperty(KJS.OBSERVE,rs.getInt(KJS.OBSERVE));
                    jsonData.addProperty(KJS.CALCULATE,rs.getInt(KJS.CALCULATE));
                    jsonData.addProperty(KJS.MEMORY,rs.getInt(KJS.MEMORY));
                    jsonData.addProperty(KJS.LOGIC,rs.getInt(KJS.LOGIC));
                    jsonData.addProperty(KJS.REFLEX,rs.getInt(KJS.REFLEX));
                    
                    jsonData.addProperty(KJS.RESULT,true);
                }
            }
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
            jsonData.addProperty(KJS.EXCEPTION, ex.toString());
        }
        return jsonData;
    }
    
    public JsonObject updateRecord(JsonObject jsonData)
    {
        
        String sql = TITDatabaseConfig.USER_GAMEINFO_UPDATE_SQL;
        try 
        {
            String userId=jsonData.get(KJS.USER_ID).getAsString();
            int nMatchCount=jsonData.get(KJS.NMATCH_COUNT).getAsInt();
            int winNMatchCount=jsonData.get(KJS.WIN_NMATCH_COUNT).getAsInt();
            int rMatchCount=jsonData.get(KJS.RMATCH_COUNT).getAsInt();
            int winRMatchCount=jsonData.get(KJS.WIN_RMATCH_COUNT).getAsInt();
            int experiencePoint=jsonData.get(KJS.EXPERIENCE_POINT).getAsInt();
            int rankPoint=jsonData.get(KJS.RANK_POINT).getAsInt();
            int afkmatchCount=jsonData.get(KJS.AFKMATCH_COUNT).getAsInt();
            int speed=jsonData.get(KJS.SPEED).getAsInt();
            int observe=jsonData.get(KJS.OBSERVE).getAsInt();
            int calculate=jsonData.get(KJS.CALCULATE).getAsInt();
            int memory=jsonData.get(KJS.MEMORY).getAsInt();
            int logic=jsonData.get(KJS.LOGIC).getAsInt();
            int reflex=jsonData.get(KJS.REFLEX).getAsInt();
            
            PreparedStatement stt = this.connection.prepareStatement(sql);
            stt.setInt(1,nMatchCount);
            stt.setInt(2,winNMatchCount);
            stt.setInt(3,rMatchCount);
            stt.setInt(4,winRMatchCount);
            stt.setInt(5,experiencePoint);
            stt.setInt(6,rankPoint);
            stt.setInt(7,afkmatchCount);
            stt.setInt(8,speed);
            stt.setInt(9,observe);
            stt.setInt(10,calculate);
            stt.setInt(11,memory);
            stt.setInt(12,logic);
            stt.setInt(13,reflex);
            stt.setString(14,userId);
            stt.execute();
            
            jsonData.addProperty(KJS.SUCESS,true);
        }
        catch (Exception ex) 
        {
            jsonData.addProperty(KJS.SUCESS,false);
        }
        return jsonData;
    }
}
