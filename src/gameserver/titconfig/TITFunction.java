package gameserver.titconfig;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import gameserver.titsystem.titentities.room.TITRoom;
import gameserver.titsystem.titentities.user.TITUser;

public class TITFunction 
{
    public static int randInt(int max) 
    {
        Random random = new Random();
        int d = random.nextInt(max);
        return d + 1;
    }

    public static String covertDate(Date date) 
    {
        Integer hours = date.getHours();
        Integer minutes = date.getMinutes();
        Integer seconds = date.getSeconds();
        return hours.toString() + ":" + minutes.toString() + ":" + seconds.toString();
    }

    public static ArrayList<TITUser> sortUserRoundPoint(TITRoom room) 
    {
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for (int i = 0; i < userArr.size() - 1;i++) 
        {
            for (int j = i + 1; j < userArr.size();j++) 
            {
                TITUser user1 = userArr.get(i);
                TITUser user2 = userArr.get(j);
                int roomUserId1=user1.getDefaultUserInfo().getRoomUserId();
                int roomUserId2=user2.getDefaultUserInfo().getRoomUserId();
                int point1=room.getGameContext().getCurrentRoundContext().
                                getUserRoundGameContext(roomUserId1).getPoint();
                int point2=room.getGameContext().getCurrentRoundContext().
                                getUserRoundGameContext(roomUserId2).getPoint();
                if (point1<point2) 
                    TITFunction.swapPostionArrayList(userArr,i,j);
            }
        }
        return userArr;
    }
    
    public static ArrayList<TITUser> sortUserTotalPoint(TITRoom room) 
    {
        ArrayList<TITUser> userArr=room.getDefaultRoomInfo().getPlayerArr();
        for (int i = 0; i < userArr.size() - 1; ++i) 
        {
            for (int j = i + 1; j < userArr.size(); ++j) 
            {
                TITUser user1 = userArr.get(i);
                TITUser user2 = userArr.get(j);
                int roomUserId1=user1.getDefaultUserInfo().getRoomUserId();
                int roomUserId2=user2.getDefaultUserInfo().getRoomUserId();
                int point1=room.getGameContext().getTotalPoint(roomUserId1);
                int point2=room.getGameContext().getTotalPoint(roomUserId2);
                if (point1<point2)
                    TITFunction.swapPostionArrayList(userArr,i,j);
            }
        }
        return userArr;
    }

    public static void swapPostionArrayList(ArrayList arrayList, int a, int b) 
    {
        Object tmp = arrayList.get(a);
        arrayList.set(a, arrayList.get(b));
        arrayList.set(b, tmp);
    }

    public static JsonObject covertToJsonObject(SFSObject sfsObject) 
    {
        Gson gson = new Gson();
        String jsonStr = sfsObject.toJson();
        JsonObject jsonObject = (JsonObject)
                                        gson.fromJson(jsonStr,JsonObject.class);
        return jsonObject;
    }

    public static SFSObject covertToSFSObject(JsonObject jsonObject) 
    {
        String jsonStr = jsonObject.toString();
        SFSObject sfsObject = (SFSObject)SFSObject.newFromJsonData(jsonStr);
        return sfsObject;
    }

    public static JsonArray covertToJsonArray(SFSArray sfsArray) 
    {
        Gson gson = new Gson();
        String jsonStr = sfsArray.toJson();
        JsonArray jsonArray = (JsonArray)gson.fromJson(jsonStr,JsonArray.class);
        return jsonArray;
    }

    public static SFSArray covertToSFSArray(JsonArray jsonArray) 
    {
        String jsonStr = jsonArray.toString();
        SFSArray sfsArray = SFSArray.newFromJsonData((String)jsonStr);
        return sfsArray;
    }
    
    public static ArrayList<Integer> covertToIntArrayList(JsonArray jsonArray)
    {
        ArrayList<Integer> arrayList=new ArrayList<Integer>();
        for(int i=0;i<jsonArray.size();i++)
        {
            int number=jsonArray.get(i).getAsInt();
            arrayList.add(number);
        }
        return arrayList;
    }
    
    public static JsonArray covertToIntJsonArray(ArrayList<Integer> arrayList)
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<arrayList.size();i++)
        {
            int number=arrayList.get(i);
            jsonArray.add(new JsonPrimitive(number));
        }
        return jsonArray;
    }
    
    public static SFSArray covertToIntSFSArray(ArrayList<Integer> arrayList)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<arrayList.size();i++)
        {
            int number=arrayList.get(i);
            sfsArray.addInt(number);
        }
        return sfsArray;
    }
    
    public static ArrayList<Float> covertToFloatArrayList(JsonArray jsonArray)
    {
        ArrayList<Float> arrayList=new ArrayList<Float>();
        for(int i=0;i<jsonArray.size();i++)
        {
            Float f=jsonArray.get(i).getAsFloat();
            arrayList.add(f);
        }
        return arrayList;
    }

    public static JsonArray covertToFloatJsonArray(ArrayList<Float> arrayList)
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<arrayList.size();i++)
        {
            Float f=arrayList.get(i);
            jsonArray.add(new JsonPrimitive(f));
        }
        return jsonArray;
    }

    public static SFSArray covertToFloatSFSArray(JsonArray jsonArray)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<jsonArray.size();i++)
        {
            Float f=jsonArray.get(i).getAsFloat();
            sfsArray.addFloat(f);
        }
        return sfsArray;
    }
    
    public static SFSArray covertToFloatSFSArray(ArrayList<Float> arrayList)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<arrayList.size();i++)
        {
            Float f=arrayList.get(i);
            sfsArray.addFloat(f);
        }
        return sfsArray;
    }
    
    public static int max(int i1,int i2)
    {
        if(i1>=i2)
            return i1;
        else
            return i2;
    }
    
    public static int min(int i1,int i2)
    {
        if(i1<=i2)
            return i1;
        else
            return i2;
    }
    
    public static boolean checkExtends(Class superClass,Class subClass)
    {        
        while(true)
        {
            if(subClass.equals(Object.class))
            {
                if(subClass.equals(superClass))
                    return true;
                else
                    return false;
            }
            else
            {
                if(subClass.equals(superClass))
                    return true;
                else
                    subClass=subClass.getSuperclass();
            }   
        }
    }
}

