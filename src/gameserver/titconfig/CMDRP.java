package gameserver.titconfig;

public class CMDRP 
{
    public static final String USERPROFILE_RP = "user_profile_rp";
    public static final String CHALLENGE_RP = "challenge_rp";
    public static final String CANCEL_CHALLENGE_RP = "cancel_challenge_rp";
    public static final String TOP_PLAYER_RP = "top_player_rp";
    public static final String CANCEL_TOP_PLAYER_RP = "cancel_top_player_rp";
    public static final String CREATEROOM_RP = "create_room_rp";
    public static final String JOINROOM_RP = "join_room_rp";
    public static final String EXITROOM_RP = "exit_room_rp";
    public static final String KICKPLAYER_RP = "kick_player_rp";
    public static final String INVITATION_RP = "invitation_rp";
    public static final String CANCELINVITATION_RP = "cancel_invitation_rp";
    public static final String INVITEFRIEND_RP = "invite_friend_rp";
    public static final String INVITATIONANSWER_RP = "invitation_answer_rp";
    public static final String ROOMLIST_RP = "room_list_rp";
    public static final String CANCELROOMLIST_RP = "cancel_room_list_rp";
    public static final String FRIENDLIST_RP = "friend_list_rp";
    public static final String CANCELFRIENDLIST_RP = "cancel_friend_list_rp";
    public static final String ROOMINFO_RP = "room_info_rp";
    public static final String ALLPLAYERINFO_R = "all_player_info_rp";
    public static final String SENDMESSAGE_RP = "send_message_rp";
    public static final String ALLMESSAGEINFO_RP = "all_message_info_rp";
    public static final String STARTGAME_RP = "start_game_rp";
    public static final String CHOICEGAME_RP = "choice_game_rp";
    public static final String BACKROOMFINISH_RP = "back_room_finish_rp";
    public static final String EXITROOMFINISH_RP = "exit_room_finish_rp";
}

