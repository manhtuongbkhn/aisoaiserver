package gameserver.titconfig;

public class CMDNF 
{
    public static final String USERJOINZONE_NF = "user_join_zone_nf";
    public static final String USERJOINROOM_NF = "user_join_room_nf";
    public static final String USEREXITROOM_NF = "user_exit_room_nf";
    public static final String CHALLENGE_NF = "challenge_nf";
    public static final String RELOADFRIENDLIST_NF = "reload_friend_list_nf";
    public static final String RELOADROOMLIST_NF = "reload_room_list_nf";
    public static final String RELOADTOPPLAYER_NF = "reload_top_player_nf";
    public static final String ROOMINFO_NF = "room_info_nf";
    public static final String ALLPLAYERINFO_NF = "all_player_info_nf";
    public static final String USERWASKICKED_NF = "user_was_kicked_user_nf";
    public static final String NEWMESSAGE_NF = "new_message_nf";
    public static final String RELOADINVITATIONFRIENDLIST_NF = "reload_invitation_friend_list_nf";
    public static final String WASINVITED_NF = "was_invited_nf";
    public static final String BEGINGAMEDOWNTIME_NF = "begin_game_down_time_nf";
    public static final String READYSTARTGAME_NF = "ready_start_game_nf";
    public static final String STARTGAMEDOWNTIME_NF = "start_game_down_time_nf";
    public static final String STARTCHOICEGAME_NF = "start_choice_game_nf";
    public static final String CHOICEGAMEINFO_NF = "choice_game_info_nf";
    public static final String CHOICEGAMEUSERINFO_NF = "choice_game_user_info_nf";
    public static final String CHOICEGAMEDOWNTIME_NF = "choice_game_down_time_nf";
    public static final String CHOICEDGAMEINFO_NF = "choiced_game_info_nf";
    public static final String STARTGUIDEGAME_NF = "start_guide_game_nf";
    public static final String GAMEGUIDEINFO_NF = "game_guide_nf";
    public static final String GUIDEGAMEUSERINFO_NF = "guide_game_user_info_nf";
    public static final String GAMESCRIPT_NF = "game_script_nf";
    public static final String GUIDEGAMEDOWNTIME_NF = "guide_game_down_time_nf";
    public static final String STARTPLAYINGGAME_NF = "start_playing_game_nf";
    public static final String PLAYINGGAMEUSERINFO_NF = "playing_game_user_info_nf";
    public static final String PLAYINGGAMEDOWNTIME_NF = "playing_game_down_time_nf";
    public static final String RELOADPLAYINGPOINT_NF = "reload_playing_point_nf";
    public static final String STARTROUNDSCORING_NF = "start_round_scoring_nf";
    public static final String ROUNDSCORINGINFO_NF = "round_scoring_info_nf";
    public static final String ROUNDSCORINGUSERINFO_NF = "round_scoring_user_info_nf";
    public static final String ROUNDSCORINGDOWNTIME_NF = "round_scoring_down_time_nf";
    public static final String STARTTOTALSCORING_NF = "start_total_scoring_nf";
    public static final String TOTALSCORINGUSERINFO_NF = "total_scoring_user_info_nf";
    public static final String TOTALSCORINGDOWNTIME_NF = "total_scoring_down_time_nf";
    public static final String WASKICKROOMBYSYSTEM_NF = "was_kickroom_by_system_nf";
}

