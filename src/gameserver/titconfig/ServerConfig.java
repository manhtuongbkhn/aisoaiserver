package gameserver.titconfig;

import gameserver.tithandler.roomextension.ChallengeRoomExtension;
import gameserver.tithandler.roomextension.NormalRoomExtension;

public class ServerConfig 
{
    public static final String NORMALROOM_GROUP = "NORMAL_ROOM_GROUP";
    public static final String CHALLENGEROOM_GROUP = "CHALLENGE_ROOM_GROUP";
    public static final String NORMALROOM_NAME = "NRoom";
    public static final String CHALLENGEROOM_NAME = "CRoom";
    public static final String EXTENSION_FOLDER = "zone";
    public static final String NORMALEXTENSION_CLASS = 
                                            NormalRoomExtension.class.getName();
    public static final String CHALLENGEEXTENSION_CLASS = 
                                        ChallengeRoomExtension.class.getName();
    public static final boolean IS_FIRECLIENTEVENT = true;
    public static final boolean IS_FIRESERVEREVENT = true;
    
    public static final int RELOAD_ROOMLIST_MILLIS = 2000;
    public static final int RELOAD_FRIENDLIST_MILLIS =2000;
    public static final int RELOAD_INVITATION_FRIENDLIST_MILLIS = 500;
    
    public static final int MESSAGE_MAX = 30;
    public static final int BEGINGAME_DOWNTIME = 20;
    public static final int STARTGAME_DOWNTIME = 3;
    public static final int CHOICEGAME_DOWNTIME = 6;
    public static final int CHECK_CHOICED_GAME_DELAY_MILLIS=200;
    public static final int SYSTEMCHOICEGAME_DOWNTIME = 3;
    public static final int ROUND_SCORING_DOWNTIME = 3;
    public static final int WAITUSERVIEW_MILLIS = 2000;
    public static final int GUIDEGAME_DOWNTIME = 6;
    public static final int TOTAL_SCORING_DOWNTIME = 15;
    public static final int VIEWDELAY_MILLIS = 1200;
    public static final int PLAYINGAMEDELAY_MILLIS = 2000;
    public static final int RELOAD_POINT_TIME_MILLIS = 500;
    
    public static final int CHALLENGE_TOKEN_MAX = 10;
    public static final int CHALLENGE_HAND_TIME_MILLIS = 60000;
    public static final int CHALLENGE_DELAY_TIME_MILLIS = 500;
    
    public static final int RELOAD_TOPPLAYER_TIME_MILLIS = 60000;
    
    public static final int PLAYING_THREAD_PRIORITY=Thread.NORM_PRIORITY;
}

