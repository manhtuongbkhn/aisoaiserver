package gameserver.titconfig;

import java.util.ArrayList;

public class TITFilter 
{
    public static String nameFilter
                            (String fullName,String firstName,String lastName) 
    {
        return fullName;
    }     
                            
    public static void gameIdFilter(ArrayList<Integer> gameIdArr,
                                            ArrayList<Integer> choicedGameIdArr)
    {
        for(int i=0;i<choicedGameIdArr.size();i++)
        {
            Integer gameId=choicedGameIdArr.get(i);
            gameIdArr.remove(gameId);
        }
    }
    
    public static String messageFilter(String message)
    {
        return message;
    }
}

