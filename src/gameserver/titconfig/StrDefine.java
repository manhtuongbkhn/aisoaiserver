package gameserver.titconfig;

public class StrDefine 
{
    public static final String WAITING = "WAITING";
    public static final String PLAYING = "PLAYING";
    public static final String SCORING = "SCORING";
    public static final String ROOM_NOT_EXIST = "ROOM_NOT_EXIST";
    public static final String ROOM_STATUS_INVALIDATE = 
                                                    "ROOM_STATUS_INVALIDATE";
    public static final String ROOM_FULL = "ROOM_FULL";
    public static final String PASSWORD_FAIL = "PASSWORD_FAIL";
    public static final String SYSTEM_ERROR = "SYSTEM_ERROR";
    public static final String ISNT_ROOM_OWNER = "ISNT_ROOM_OWNER";
    public static final String USER_NOT_IN_ROOM = "USER_NOT_IN_ROOM";
    public static final String USER_NOT_ONLINE = "USER_NOT_ONLINE";
    public static final String USER_NOT_FRIEND = "USER_NOT_FRIEND";
    public static final String NOT_ENOUGH_USER = "NOT_ENOUGH_USER";
    public static final String NOT_PERMISSION_CHOICE_GAME = 
                                                "NOT_PERMISSION_CHOICE_GAME";
    public static final String GAME_NOT_INVALID = "GAME_NOT_INVALID";
    public static final String CHOICED_GAME ="CHOICED_GAME";
    public static final String FRIEND_CANT_JOIN = "FRIEND_CANT_JOIN";
    public static final String DEFAULT_CHALLENGE_INVITE = 
                            "Ch\u00fac tr\u1eadn \u0111\u1ea5u vui v\u1ebb!";
    public static final String DEFAULT_ROOM_INVITE=
                                            "Choi vui ve nao !";
}

