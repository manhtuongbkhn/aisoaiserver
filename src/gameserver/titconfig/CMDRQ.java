package gameserver.titconfig;

public class CMDRQ 
{
    public static final String KEEPCONNECTION_RQ = "keep_connection_rq";
    public static final String USERPROFILE_RQ = "user_profile_rq";
    public static final String CHALLENGE_RQ = "challenge_rq";
    public static final String CANCEL_CHALLENGE_RQ = "cancel_challenge_rq";
    public static final String TOP_PLAYER_RQ = "top_player_rq";
    public static final String CANCEL_TOP_PLAYER_RQ = "cancel_top_player_rq";
    public static final String ROOMLIST_RQ = "room_list_rq";
    public static final String CANCELROOMLIST_RQ = "cancel_room_list_rq";
    public static final String FRIENDLIST_RQ = "friend_list_rq";
    public static final String CANCELFRIENDLIST_RQ = "cancel_friend_list_rq";
    public static final String CREATEROOM_RQ = "create_room_rq";
    public static final String JOINROOM_RQ = "join_room_rq";
    public static final String EXITROOM_RQ = "exit_room_rq";
    public static final String KICKPLAYER_RQ = "kick_player_rq";
    public static final String INVITATION_RQ = "invitation_rq";
    public static final String CANCELINVITATION_RQ = "cancel_invitation_rq";
    public static final String INVITEFRIEND_RQ = "invite_friend_rq";
    public static final String INVITATIONANSWER_RQ = "invitation_answer_rq";
    public static final String ROOMINFO_RQ = "room_info_rq";
    public static final String ALLPLAYERINFO_RQ = "all_player_info_rq";
    public static final String SENDMESSAGE_RQ = "send_message_rq";
    public static final String ALLMESSAGEINFO_RQ = "all_message_info_rq";
    public static final String STARTGAME_RQ = "start_game_rq";
    public static final String CHOICEGAME_RQ = "choice_game_rq";
    public static final String GAMEANSWER_RQ = "game_answer_rq";
    public static final String BACKROOMFINISH_RQ = "back_room_finish_rq";
    public static final String EXITROOMFINISH_RQ = "exit_room_finish_rq";
}

