package gameserver.titconfig;

public class TITDatabaseConfig 
{
    public static final String USER_PROFILE_INSERT_SQL=
                    "insert into UserProfile values (?,?,?,?,?,?,?,?,?,?,?)";
    public static final String USER_PROFILE_GET_SQL=
                                "select * from UserProfile where user_id=?";
    public static final String USER_PROFILE_GET_BYFB_SQL=
                            "select * from UserProfile where facebook_id=?";
    public static final String USER_PROFILE_GET_MAXUSERID_SQL=
                            "select max(user_id) as user_id from UserProfile";
    public static final String USER_PROFILE_UPDATE_ONLINE=
                            "update UserProfile set online=? where user_id=?";
    public static final String USER_PROFILE_UPDATE_SYSTEMUSERID=
                    "update UserProfile set system_user_id=? where user_id=?";
    public static final String USER_PROFILE_UPDATE_RECORD="update UserProfile "
            + "set facebook_id=?,full_name=?,email=?,birthday=?,gender=?,"
            + "facebook_url=?,avatar_url=? where user_id=?";
    public static final String TOP_USER_GET_SQL=
                                "select * from UserProfile limit 10";
    ////////////////////////////////////////////////////////////////////////////
    public static final String LOGIN_HISTORY_INSERT_RECORD=
                                "insert into LoginHistory values (?,?,?,?)";
    public static final String LOGIN_HISTORY_GET_RECORD=
                                "select * from LoginHistory where user_id=?";
    public static final String LOGIN_HISTORY_GET_RECORD_ACCESSTOKEN=
            "select * from LoginHistory where user_id=? and access_token=?";
    ////////////////////////////////////////////////////////////////////////////
    public static final String USER_GAMEINFO_INSERT_SQL=
             "insert into GameUserInfo values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String USER_GAMEINFO_GET_SQL=
                                "select * from GameUserInfo where user_id=?";
    public static final String USER_GAMEINFO_UPDATE_SQL="update GameUserInfo set "
            + "nmatch_count=?,win_nmatch_count=?,rmatch_count=?,win_rmatch_count=?"
            + ",experience_point=?,rank_point=?,afkmatch_count=?,speed=?,observe=?"
            + ",calculate=?,memory=?,logic=?,reflex=? where user_id=?";
    ////////////////////////////////////////////////////////////////////////////
    
    public static final String USER_GAME_MATURITY_INSERT_SQL=
                                    "insert into UserGameMaturity values(?,?,?)";
    public static final String USER_GAME_MATURITY_GET_SQL=
                            "select * from UserGameMaturity where user_id=?";
    public static final String USER_GAME_MATURITY_UPDATE_SQL=
                "update UserGameMaturity set game_maturity=? where user_id=? and game_id=?";
    ////////////////////////////////////////////////////////////////////////////
    
}   
