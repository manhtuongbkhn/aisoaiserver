package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class ZombieGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default:
                return ZombieGameScriptFactory.createEasyGameScript();
        }
    }

    private static JsonObject createEasyGameScript() 
    {
        int type;
        int radom;
        int i;
        int zombieType;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int postion = 1;
        int olderPostion = 1;
        int index = 1;
        int maxPoint=0;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        int point=0;
        //Phase 1
        for (i = 0; i < 26; ++i) 
        {
            jsonObject = new JsonObject();
            radom = TITFunction.randInt(2);
            switch (olderPostion) 
            {
                case 1: 
                    if (radom == 1)
                        postion = 2;
                    if (radom != 2) 
                        break;
                    postion = 3;
                        break;
                case 2:
                    if (radom == 1)
                        postion = 1;
                    if (radom != 2) 
                        break;
                    postion = 3;
                    break;
                case 3:
                    if (radom == 1)
                        postion = 1;
                    if (radom != 2) 
                        break;
                    postion = 2;
            }
            olderPostion = postion;
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            type = TITFunction.randInt(4);
            switch (type) 
            {
                case 1: 
                case 2: 
                case 3: 
                    jsonObject.addProperty(KJS.METHOD_NAME,"createNewZombie");
                    zombieType = TITFunction.randInt(4);
                    jsonObject.addProperty(KJS.PARAM3,zombieType);
                    switch (zombieType) 
                    {
                        case 1: 
                        case 2: 
                            jsonObject.addProperty(KJS.PARAM2,1.8f);
                            point=20;
                            break;
                        case 3: 
                        case 4: 
                            jsonObject.addProperty(KJS.PARAM2,1.8f);
                            point=30;
                            break;
                    }
                    break;
                default:
                    jsonObject.addProperty(KJS.METHOD_NAME,"createNewPeople");
                    jsonObject.addProperty(KJS.PARAM2,1.8f);
                    point=0;
                    break;
            }
            gameScript.add(jsonObject);
            gameScript.add(ZombieGameScriptFactory.createSleep(750));
            maxPoint=maxPoint+point;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,point);
            gameAnswer.add(jsonObject);
        }
        
        gameScript.add((JsonElement)ZombieGameScriptFactory.createSleep(1000));
        
        //Phase 2
        for (i = 0; i < 26; ++i) 
        {
            jsonObject = new JsonObject();
            radom = TITFunction.randInt(2);
            switch (olderPostion) 
            {
                case 1:
                    if (radom == 1)
                        postion = 2;
                    if (radom != 2) 
                        break;
                    postion = 3;
                    break;
                case 2:
                    if (radom == 1)
                        postion = 1;
                    if (radom != 2) 
                        break;
                    postion = 3;
                    break;
                case 3:
                    if (radom == 1)
                        postion = 1;
                    if (radom != 2) 
                        break;
                    postion = 2;
                    break;
            }
            
            olderPostion = postion;
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            type = TITFunction.randInt(4);
            switch (type) 
            {
                case 1: 
                case 2: 
                case 3: 
                    jsonObject.addProperty(KJS.METHOD_NAME,"createNewZombie");
                    zombieType = TITFunction.randInt(6);
                    jsonObject.addProperty(KJS.PARAM3,zombieType);
                    switch (zombieType) 
                    {
                        case 1: 
                        case 2:
                            jsonObject.addProperty(KJS.PARAM2,1.5f);
                            point=25;
                            break;
                        case 3: 
                        case 4:
                            jsonObject.addProperty(KJS.PARAM2,1.5f);
                            point=35;
                            break;
                        case 5: 
                        case 6:
                            jsonObject.addProperty(KJS.PARAM2,1.5f);
                            point=45;
                    }
                    break;
                default:
                    jsonObject.addProperty(KJS.METHOD_NAME, "createNewPeople");
                    jsonObject.addProperty(KJS.PARAM2,1.5f);
                    point=0;
                    break;
            }
            
            gameScript.add(jsonObject);
            gameScript.add(ZombieGameScriptFactory.createSleep(750));
            maxPoint=maxPoint+point;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,point);
            gameAnswer.add(jsonObject);
        }
        gameScript.add(ZombieGameScriptFactory.createSleep(1000));
        
        //Phase 3
        for (i = 0; i < 19; ++i) 
        {
            jsonObject = new JsonObject();
            radom = TITFunction.randInt(2);
            switch (olderPostion) 
            {
                case 1: 
                    if (radom == 1) 
                        postion = 2;
                    if (radom != 2) 
                        break;
                    postion = 3;
                    break;
                case 2:
                    if (radom == 1) 
                        postion = 1;
                    if (radom != 2) 
                        break;
                    postion = 3;
                    break;
                case 3: 
                    if (radom == 1) 
                        postion = 1;
                    if (radom != 2) 
                        break;
                    postion = 2;
            }
            
            olderPostion = postion;
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            type = TITFunction.randInt(4);
            switch (type) 
            {
                case 1: 
                case 2: 
                case 3: 
                {
                    jsonObject.addProperty(KJS.METHOD_NAME,"createNewZombie");
                    zombieType = TITFunction.randInt(4) + 2;
                    jsonObject.addProperty(KJS.PARAM3,zombieType);
                    switch (zombieType) 
                    {
                        case 3: 
                        case 4:
                            jsonObject.addProperty(KJS.PARAM2,1.2f);
                            point=40;
                            break;
                        case 5: 
                        case 6:
                            jsonObject.addProperty(KJS.PARAM2,1.2f);
                            point=50;
                    }
                    break;
                }
                default:
                    jsonObject.addProperty(KJS.METHOD_NAME, "createNewPeople");
                    jsonObject.addProperty(KJS.PARAM2,1.2f);
                    point=0;
                    break;
            }
            gameScript.add(jsonObject);
            gameScript.add(ZombieGameScriptFactory.createSleep(1000));
            maxPoint=maxPoint+point;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,point);
            gameAnswer.add(jsonObject);
        }
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"finish");
        gameScript.add(jsonObject);
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60f);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }

    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        float finishPostion = userGameAnswer.get(KJS.PARAM1).getAsFloat();
        float gcHeight = userGameAnswer.get(KJS.PARAM2).getAsFloat();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        switch (systemGameAnswer.get(KJS.PARAM1).getAsInt()) 
        {
            case 1:
                if (finishPostion < gcHeight)
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
                else
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    return  result;
                }
            case 2:
                if (finishPostion < gcHeight)
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
                else
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
            case 3:
                if (finishPostion < gcHeight)
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
                else
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
            case 4:
                if (finishPostion < gcHeight)
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,-15);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
        }
        return result;
    }
}

