
package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class DoublePokemonGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return DoublePokemonGameScriptFactory.createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int maxPoint=0;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        //Phase 1
        for (int i=1;i<=15;i++) 
        {
            int level;
            switch(i)
            {
                case 1:
                case 2:
                    level=1;
                    break;
                case 3:
                case 4:
                    level=2;
                    break;
                case 5:
                case 6:
                    level=3;
                    break;
                case 7:
                case 8:
                    level=4;
                    break;
                case 9:
                case 10:
                    level=5;
                    break;
                case 11:
                case 12:
                    level=6;
                    break;
                case 13:
                case 14:
                case 15:
                default:
                    level=7;
                    break;
            }
            
            int widthColumn=getWidthColumn(level);
            int heightRow=getHeightRow(level);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewPokemonQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.add(KJS.PARAM1,new JsonPrimitive(widthColumn));
            jsonObject.add(KJS.PARAM2,new JsonPrimitive(heightRow));
            JsonArray question=createQuestion(level);
            jsonObject.add(KJS.PARAM3,question);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+50;
            
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.POINT,50);
            jsonObject.add(KJS.PARAM1,question);
            gameAnswer.add(jsonObject);
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }

    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }
    
    private static JsonArray createQuestion(int level)
    {
        ArrayList<Integer> resultArr=new ArrayList<Integer>();
        int widthColumn=getWidthColumn(level);
        int heightRow=getHeightRow(level);
        
        int countBox=widthColumn*heightRow;
        
        ArrayList<Integer> indexArr=new ArrayList<Integer>();
        
        for(int i=1;i<=countBox;i++)
        {
            resultArr.add(0);
            indexArr.add(i);
        }
        
        ArrayList<Integer> pokemonTypeArr=new ArrayList<Integer>();
        for(int i=1;i<=10;i++)
        {
            pokemonTypeArr.add(i);
        }
        
        for(int i=1;i<=countBox;i=i+2)
        {
            int index=TITFunction.randInt(pokemonTypeArr.size());
            int pokemonType=pokemonTypeArr.get(index-1);
            pokemonTypeArr.remove(index-1);
            
            index=TITFunction.randInt(indexArr.size());
            int number1=indexArr.get(index-1);
            indexArr.remove(index-1);
            
            index=TITFunction.randInt(indexArr.size());
            int number2=indexArr.get(index-1);
            indexArr.remove(index-1);
            
            resultArr.set(number1-1,pokemonType);
            resultArr.set(number2-1,pokemonType);
        }
        return TITFunction.covertToIntJsonArray(resultArr);
    }
    
    public static int getWidthColumn(int level) 
    {
        switch (level)
        {
            case 1:
                return 2;
            case 2: 
                return 3;
            case 3: 
                return 4;
            case 4:
                return 5;
            case 5: 
                return 4;
            case 6:
                return 4;
            case 7:
            default:
                return 5;        
        }
    }

    public static int getHeightRow(int level) 
    {
        switch (level) 
        {
            case 1:
            case 2: 
            case 3:
            case 4:
                return 2;
            case 5:
                return 3;
            case 6:
            case 7:
            default:
                return 4;
        }
    }
    
    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        JsonArray userAnswerJsonArray=userGameAnswer.get
                                                (KJS.PARAM1).getAsJsonArray();
        JsonArray systemQuestionJsonArray=systemGameAnswer.get
                                                (KJS.PARAM1).getAsJsonArray();
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
        jsonObject.addProperty(KJS.VALUE,point);
        result.add(jsonObject);
        jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
        jsonObject.addProperty(KJS.VALUE,1);
        result.add(jsonObject);
        return result;
    }
}
