package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class ChoiceModGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default:
                return ChoiceModGameScriptFactory.createEasyGameScript();
        }
    }

    private static JsonObject createEasyGameScript() 
    {
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int index = 1;
        int maxPoint=0;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        jsonObject.addProperty(KJS.PARAM2,false);
        gameScript.add((JsonElement)jsonObject);
        
        for (int i = 1; i < 50; ++i) 
        {
            int number2 = TITFunction.randInt(8);
            int number1 = TITFunction.randInt(number2 * 10);
            int mod = number1 % number2;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,number1);
            jsonObject.addProperty(KJS.PARAM2,number2);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+20;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,mod);
            jsonObject.addProperty(KJS.POINT,20);
            gameAnswer.add(jsonObject);
        }
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        return result;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonObject systemGameAnswer=systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        int systemMod=systemGameAnswer.get(KJS.PARAM1).getAsInt();
        int userMod = userGameAnswer.get(KJS.PARAM1).getAsInt();
        if (userMod == systemMod ) 
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        else
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-0.5f*point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
    }
}

