package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class SortRecGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int maxPoint=0;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        int type;
        JsonArray questionJsonArray,answerJsonArray;
        
        for (int level = 1; level <= 10;level++) 
        {
            jsonObject = new JsonObject();
            type=getType(level);
            questionJsonArray=createQuestion(level);
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.add(KJS.PARAM2,questionJsonArray);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+50;
            
            jsonObject = new JsonObject();
            if(type==0)
                answerJsonArray=questionJsonArray;
            else
                answerJsonArray=reverseJsonArray(questionJsonArray);
            jsonObject.add(KJS.PARAM1,answerJsonArray);
            jsonObject.addProperty(KJS.POINT,50);
            gameAnswer.add(jsonObject);
            ////////////////////////////////////////////////////////////////////
            jsonObject = new JsonObject();
            type=getType(level);
            questionJsonArray=createQuestion(level);
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.add(KJS.PARAM2,questionJsonArray);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+50;
            
            jsonObject = new JsonObject();
            if(type==0)
                answerJsonArray=questionJsonArray;
            else
                answerJsonArray=reverseJsonArray(questionJsonArray);
            jsonObject.add(KJS.PARAM1,answerJsonArray);
            jsonObject.addProperty(KJS.POINT,50);
            gameAnswer.add(jsonObject);
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }
    
    private static JsonArray createQuestion(int level)
    {
        JsonArray jsonArray=new JsonArray();
        int recCount=getRecCount(level);
        ArrayList<Integer> indexArr=initIndexArr();
        
        for(int i=0;i<recCount;i++)
        {
            int radomPostion=TITFunction.randInt(indexArr.size())-1;
            int radomIndex=indexArr.get(radomPostion);
            jsonArray.add(new JsonPrimitive(radomIndex));
            indexArr.remove(radomPostion);
        }
        return jsonArray;
    }
    
    private static ArrayList<Integer> initIndexArr()
    {
        ArrayList<Integer> indexArr=new ArrayList<Integer>();
        for(int i=0;i<30;i++)
        {
            indexArr.add(i);
        }
        return indexArr;
    }
    
    private static int getType(int level)
    {
        switch(level)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 9:
                return 0;
            case 2:
            case 4:
            case 6:
            case 8:
            case 10:
            default:
                return 1;
        }
    }
    
    private static int getRecCount(int level)
    {
        switch(level)
        {
            case 1:
            case 2:
                return 2;
            case 3:
            case 4:
                return 3;
            case 5:
            case 6:
                return 4;
            case 7:
            case 8:
                return 5;
            case 9:
            case 10:
                return 6;
            default:
                return 1;
        }
    }
    
    private static JsonArray reverseJsonArray(JsonArray questionArr )
    {
        JsonArray reverseJsonArray=new JsonArray();
        for(int i=questionArr.size()-1;i>=0;i--)
        {
            int index=questionArr.get(i).getAsInt();
            reverseJsonArray.add(new JsonPrimitive(index));
        }
        return reverseJsonArray;
    }
    
    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
        JsonArray userAnswerJsonArray=userGameAnswer.get
                                                (KJS.PARAM1).getAsJsonArray();
        JsonArray systemAnswerJsonArray=systemGameAnswer.get
                                                (KJS.PARAM1).getAsJsonArray();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        JsonObject jsonObject;
        switch(compareIntJsonArray(systemAnswerJsonArray,userAnswerJsonArray))
        {
            case -1:
            case 1:
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            case 0:
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
        }
        return result;
    }
    
    public static int compareIntJsonArray
                                    (JsonArray jsonArray1,JsonArray jsonArray2)
    {
        if(jsonArray1.size()>jsonArray2.size())
            return 1;
        if(jsonArray1.size()<jsonArray2.size())
            return -1;
        if(jsonArray1.size()==jsonArray2.size())
        {
            for(int i=0;i<jsonArray1.size();i++)
            {
                if(jsonArray1.get(i).getAsInt()>jsonArray2.get(i).getAsInt())
                    return 1;
                if(jsonArray1.get(i).getAsInt()<jsonArray2.get(i).getAsInt())
                    return -1;
            }
            return 0;
        }
        return 0;
    }
}
