package gameserver.titgameinfo;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.util.ArrayList;

public class TITGameInfoFactory 
{
    private static ArrayList<Integer> allGameIdArr = new ArrayList();
    private static ArrayList<Integer> freeGameIdArr=new ArrayList<Integer>();
    private static ArrayList<TITGameInfo> allGameInfoArr = new ArrayList();
    private static SFSArray allGameInfoSFSArr = new SFSArray();

    public static void init() 
    {
        allGameIdArr.add(1);
        allGameIdArr.add(2);
        allGameIdArr.add(3);
        allGameIdArr.add(4);
        allGameIdArr.add(5);
        allGameIdArr.add(6);
        allGameIdArr.add(7);
        allGameIdArr.add(8);
        allGameIdArr.add(9);
        allGameIdArr.add(10);
        allGameIdArr.add(11);
        allGameIdArr.add(12);
        allGameIdArr.add(13);
        allGameIdArr.add(14);
        allGameIdArr.add(15);
        freeGameIdArr=allGameIdArr;
        allGameInfoArr = TITGameInfoFactory.
                                    covertGameIdArrToGameInfoArr(allGameIdArr);
        allGameInfoSFSArr = TITGameInfoFactory.
                                covertGameIdArrToGameInfoSFSArr(allGameIdArr);
    }

    public static TITGameInfo covertGameIdToGameInfo(int gameId) 
    {
        TITGameInfo titGameInfo = new TITGameInfo();
        switch (gameId) 
        {
            case 1:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Circle Run");
                titGameInfo.setGameMainType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameSecondaryType("Quan S\u00e1t");
                titGameInfo.setGameGuide("Nh\u1ea5n v\u00e0o c\u00e1c h\u00ecnh tr\u00f2n \u1edf gi\u1eefa thay \u0111\u1ed5i m\u00e0u sao cho c\u00f9ng m\u00e0u v\u1edbi h\u00ecnh tr\u00f2n r\u01a1i xu\u1ed1ng!");
                break;
            case 2:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Zombie Smasher");
                titGameInfo.setGameMainType("Quan S\u00e1t");
                titGameInfo.setGameSecondaryType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameGuide("Nh\u1ea5n v\u00e0o zombie \u0111\u1ec3 ti\u00eau di\u1ec7t ch\u00fang tr\u01b0\u1edbc khi ch\u00fang ch\u1ea1y qua,Kh\u00f4ng \u0111\u01b0\u1ee3c ti\u00eau di\u1ec7t ng\u01b0\u1eddi!");
                break;
            case 3:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Audition");
                titGameInfo.setGameMainType("Quan S\u00e1t");
                titGameInfo.setGameSecondaryType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameGuide("Nh\u1ea5n v\u00e0o h\u00ecnh m\u0169i t\u00ean d\u01b0\u1edbi theo h\u00ecnh m\u0169i t\u00ean \u1edf tr\u00ean, m\u00e0u xanh c\u00f9ng chi\u1ec1u,m\u00e0u cam ng\u01b0\u1ee3c chi\u1ec1u!");
                break;
            case 4:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Operations");
                titGameInfo.setGameMainType("T\u00ednh To\u00e1n");
                titGameInfo.setGameSecondaryType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameGuide("Nh\u1ea5n v\u00e0o ph\u00e9p to\u00e1n \u0111\u1ec3 \u0111i\u1ec1n v\u00e0o \u00f4 tr\u1ed1ng sao cho ph\u00e9p t\u00ednh ph\u00eda tr\u00ean \u0111\u00fang!");
                break;
            case 5:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Find Pokemon");
                titGameInfo.setGameMainType("Quan S\u00e1t");
                titGameInfo.setGameSecondaryType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameGuide("T\u00ecm ki\u1ebfm v\u00e0 nh\u1ea5n v\u00e0o \u1ea3nh pokemon kh\u00e1c so v\u1edbi c\u00e1c \u1ea3nh c\u00f2n l\u1ea1i !");
                break;
            case 6: 
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Choice Mod");
                titGameInfo.setGameMainType("T\u00ednh To\u00e1n");
                titGameInfo.setGameSecondaryType("Quan S\u00e1t");
                titGameInfo.setGameGuide("K\u00e9o ph\u00e9p chia gi\u1eefa m\u00e0n h\u00ecnh v\u1ec1 h\u01b0\u1edbng \u0111\u00e1p \u00e1n \u0111\u00fang.8 g\u00f3c m\u00e0n h\u00ecnh l\u00e0 \u0111\u00e1p \u00e1n d\u01b0 c\u1ee7a ph\u00e9p chia!");
                break;
            case 7:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Collect Fruit");
                titGameInfo.setGameMainType("Quan S\u00e1t");
                titGameInfo.setGameSecondaryType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameGuide("Nh\u1ea5n v\u00e0 di chuy\u1ec3n ch\u00fa kh\u1ec9 \u0111\u1ec3 h\u1ee9ng nh\u1eefng hoa qu\u1ea3 r\u01a1i xu\u1ed1ng v\u00e0 tr\u00e1nh nh\u1eefng tr\u00e1i bom!");
                break;
            case 8:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Play Piano");
                titGameInfo.setGameMainType("Quan S\u00e1t");
                titGameInfo.setGameSecondaryType("Nhanh Nh\u1ea1y");
                titGameInfo.setGameGuide("Nh\u1ea5n v\u00e0o c\u00e1c ph\u00edm \u0111ang ch\u1ea1y piano \u0111\u1ec3 ch\u01a1i nh\u1ea1c!");
                break;
            case 9:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Double Pokemon");
                titGameInfo.setGameMainType(GameTypeDefine.MEMORY);
                titGameInfo.setGameSecondaryType(GameTypeDefine.LOGIC);
                titGameInfo.setGameGuide("Xem và ghi nho cac hinh pokemon.Sau do"
                                    + "chon cac cap hinh pokemon giong nhau.");
                break;
            case 10:                
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Flappy Doraemon");
                titGameInfo.setGameMainType(GameTypeDefine.OBSERVE);
                titGameInfo.setGameSecondaryType(GameTypeDefine.SPEED);
                titGameInfo.setGameGuide("Nhan vao man hinh de dieu khien "
                                + "doraemon vuot qua cac chuong ngai vat.");
                break;
            case 11:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Dinosaur Jump");
                titGameInfo.setGameMainType(GameTypeDefine.OBSERVE);
                titGameInfo.setGameSecondaryType(GameTypeDefine.SPEED);
                titGameInfo.setGameGuide("Nhan vao ma hinh dieu khien chu khung "
                            + "long nhanh de vuot qua cac chuong ngai vat !");
                break;
            case 12:
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Sort Rec");
                titGameInfo.setGameMainType(GameTypeDefine.MEMORY);
                titGameInfo.setGameSecondaryType(GameTypeDefine.OBSERVE);
                titGameInfo.setGameGuide("Ghi nho thu tu xuat hien cua cac hinh "
                + "vuong.Sau do nhan vao hinh vuong mau lam theo thu tu xuat "
                                            + "hien,mau cam thi nguoc lai.");
                break;
            case 13:  
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Spider Web");
                titGameInfo.setGameMainType(GameTypeDefine.LOGIC);
                titGameInfo.setGameSecondaryType(GameTypeDefine.OBSERVE);
                titGameInfo.setGameGuide("Tim dia diem roi xuong cua chu nhen !");
                break;
            case 14:    
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Snake Hunting");
                titGameInfo.setGameMainType(GameTypeDefine.OBSERVE);
                titGameInfo.setGameSecondaryType(GameTypeDefine.LOGIC);
                titGameInfo.setGameGuide("Dieu khien chu ran an cac thuc an va "
                                            + "tranh cac chuong ngai vat !");
                break;
            case 15:     
            default:   
                titGameInfo.setGameId(gameId);
                titGameInfo.setGameName("Tetris");
                titGameInfo.setGameMainType(GameTypeDefine.LOGIC);
                titGameInfo.setGameSecondaryType(GameTypeDefine.SPEED);
                titGameInfo.setGameGuide("Di chuyen va xoay cac vien gach de dat"
                                                        + " diem so cao nhat!");
                break;
        }
        return titGameInfo;
    }

    public static ArrayList<TITGameInfo> covertGameIdArrToGameInfoArr(ArrayList<Integer> gameIdArr) 
    {
        ArrayList<TITGameInfo> gameInfoArr = new ArrayList<TITGameInfo>();
        for (int i = 0; i < gameIdArr.size(); ++i) {
            int gameId = gameIdArr.get(i);
            TITGameInfo gameInfo = TITGameInfoFactory.covertGameIdToGameInfo(gameId);
            gameInfoArr.add(gameInfo);
        }
        return gameInfoArr;
    }

    public static SFSArray covertGameIdArrToGameInfoSFSArr(ArrayList<Integer> gameIdArr) {
        SFSArray sfsArray = new SFSArray();
        for (int i = 0; i < gameIdArr.size(); ++i) {
            int gameId = gameIdArr.get(i);
            TITGameInfo gameInfo = TITGameInfoFactory.covertGameIdToGameInfo(gameId);
            SFSObject sfsObject = gameInfo.toSFSObject();
            sfsArray.addSFSObject((ISFSObject)sfsObject);
        }
        return sfsArray;
    }

    public static ArrayList<Integer> getAllGameIdArr() 
    {
        ArrayList<Integer> copyAllGameIdArr=new ArrayList<>();
        for(int i=0;i<allGameIdArr.size();i++)
        {
            Integer gameId=allGameIdArr.get(i);
            Integer copygameId=new Integer(gameId);
            copyAllGameIdArr.add(copygameId);
        }
        return copyAllGameIdArr;
    }

    public static ArrayList<Integer> getFreeGameIdArr() 
    {
        ArrayList<Integer> copyFreeGameIdArr=new ArrayList<>();
        for(int i=0;i<freeGameIdArr.size();i++)
        {
            Integer gameId=freeGameIdArr.get(i);
            Integer copygameId=new Integer(gameId);
            copyFreeGameIdArr.add(copygameId);
        }
        return copyFreeGameIdArr;
    }
}

