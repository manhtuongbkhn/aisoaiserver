package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class SpiderWebGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int maxPoint=0;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        //Phase1
        for (int i = 1; i <= 5;i++) 
        {
            jsonObject = new JsonObject();
            int startPointColum=TITFunction.randInt(5);
            JsonArray verLineIndexJsonArray=createQuestion(startPointColum,12);
            SpiderWebGameAnswerFactory gameAnswerFactory=
                   new SpiderWebGameAnswerFactory(startPointColum,verLineIndexJsonArray);
            JsonArray pointIndexJsonArray=
                                    gameAnswerFactory.getPointIndexJsonArray();
            
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,startPointColum);
            jsonObject.add(KJS.PARAM2,verLineIndexJsonArray);
            jsonObject.add(KJS.PARAM3,pointIndexJsonArray);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+100;
            jsonObject = new JsonObject();
            int size=pointIndexJsonArray.size();
            int lastPointIndex=pointIndexJsonArray.get(size-1).getAsInt();
            int answerColumn=SpiderWebGameFunction.getPointColumn(lastPointIndex);
            jsonObject.addProperty(KJS.PARAM1,answerColumn);
            jsonObject.addProperty(KJS.POINT,100);
            gameAnswer.add(jsonObject);
        }
        
        //Phase2
        for (int i = 1; i <= 5;i++) 
        {
            jsonObject = new JsonObject();
            int startPointColum=TITFunction.randInt(5);
            JsonArray verLineIndexJsonArray=createQuestion(startPointColum,15);
            SpiderWebGameAnswerFactory gameAnswerFactory=
                   new SpiderWebGameAnswerFactory(startPointColum,verLineIndexJsonArray);
            JsonArray pointIndexJsonArray=
                                    gameAnswerFactory.getPointIndexJsonArray();
            
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,startPointColum);
            jsonObject.add(KJS.PARAM2,verLineIndexJsonArray);
            jsonObject.add(KJS.PARAM3,pointIndexJsonArray);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+100;
            jsonObject = new JsonObject();
            int size=pointIndexJsonArray.size();
            int lastPointIndex=pointIndexJsonArray.get(size-1).getAsInt();
            int answerColumn=SpiderWebGameFunction.getPointColumn(lastPointIndex);
            jsonObject.addProperty(KJS.PARAM1,answerColumn);
            jsonObject.addProperty(KJS.POINT,100);
            gameAnswer.add(jsonObject);
        }
        
        //Phase3
        for (int i = 1; i <= 5;i++) 
        {
            jsonObject = new JsonObject();
            int startPointColum=TITFunction.randInt(5);
            JsonArray verLineIndexJsonArray=createQuestion(startPointColum,18);
            SpiderWebGameAnswerFactory gameAnswerFactory=
                   new SpiderWebGameAnswerFactory(startPointColum,verLineIndexJsonArray);
            JsonArray pointIndexJsonArray=
                                    gameAnswerFactory.getPointIndexJsonArray();
            
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,startPointColum);
            jsonObject.add(KJS.PARAM2,verLineIndexJsonArray);
            jsonObject.add(KJS.PARAM3,pointIndexJsonArray);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+100;
            jsonObject = new JsonObject();
            int size=pointIndexJsonArray.size();
            int lastPointIndex=pointIndexJsonArray.get(size-1).getAsInt();
            int answerColumn=SpiderWebGameFunction.getPointColumn(lastPointIndex);
            jsonObject.addProperty(KJS.PARAM1,answerColumn);
            jsonObject.addProperty(KJS.POINT,100);
            gameAnswer.add(jsonObject);
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }
    
    private static JsonArray createQuestion(int startPointColum,int lineCount)
    {
        JsonArray verLineIndexJsonArray=new JsonArray();
        ArrayList<Integer> indexArr=initIndexArr();
        
        if(1<startPointColum&&startPointColum<5)
        {
            int radom=TITFunction.randInt(2);
            if(radom==1)//left
            {
                verLineIndexJsonArray.add(new JsonPrimitive(startPointColum-2+4));
            }
        
            if(radom==2)//right
            {
                verLineIndexJsonArray.add(new JsonPrimitive(startPointColum-1+4));
            }
            
            indexArr.remove(new Integer(startPointColum+2));
            indexArr.remove(new Integer(startPointColum+3));
        }
        
        if(startPointColum==1)
        {
            verLineIndexJsonArray.add(new JsonPrimitive(4));
            indexArr.remove(new Integer(4));
        }
        
        if(startPointColum==5)
        {
            verLineIndexJsonArray.add(new JsonPrimitive(7));
            indexArr.remove(new Integer(7));
        }
        
        for(int i=0;i<lineCount-1;i++)
        {
            int radomPostion=TITFunction.randInt(indexArr.size())-1;
            int radomIndex=indexArr.get(radomPostion);
            verLineIndexJsonArray.add(new JsonPrimitive(radomIndex));
            indexArr.remove(new Integer(radomIndex));
        }
        return verLineIndexJsonArray;
    }
    
    private static ArrayList<Integer> initIndexArr()
    {
        ArrayList<Integer> indexArr=new ArrayList<Integer>();
        for(int i=0;i<32;i++)
        {
            indexArr.add(i+4);
        }
        return indexArr;
    }
    
    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
        
        int userAnswerColumn=userGameAnswer.get(KJS.PARAM1).getAsInt();
        int systemAnswerColumn=systemGameAnswer.get(KJS.PARAM1).getAsInt();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        if(userAnswerColumn==systemAnswerColumn)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        else
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-0.5f*point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
    }
}

class SpiderWebGameAnswerFactory
{
    private int pointRow;
    private int pointColumn;
    private int priorityDirection=0;
    private int inputDirection=0;
    /*
        1:right
        2:bot
        3:left
        4:top
    */
    private JsonArray verLineIndexJsonArray;
    private JsonArray pointIndexJsonArray=new JsonArray();
    
    public SpiderWebGameAnswerFactory(int startPointColum,JsonArray verLineIndexJsonArray)
    {
        this.pointRow=1;
        this.pointColumn=startPointColum;
        this.verLineIndexJsonArray=verLineIndexJsonArray;
        run();
    }
    
    private void run()
    {
        //System.out.print("VerLineIndexArr:"+verLineIndexJsonArray.toString());
        while(true)
        {
            //System.out.println("Point Row:"+pointRow);
            //System.out.println("Point Colum:"+pointColumn);
            addPoint();
            if(pointRow==10)
                break;
            if(hasLeftVerline()&&hasRightVerline())
            {
                switch(inputDirection)
                {
                    case 0:
                        turnLeft();
                        break;
                    case 1:
                        turnRight();
                        break;
                    case 2:
                        if(priorityDirection==3)
                            turnLeft();
                        else
                        {
                            if(priorityDirection==1)
                                turnRight();
                        }
                        break;
                    case 3:
                        turnLeft();
                        break;
                }
            }
            else
            {              
                if(hasLeftVerline()||hasRightVerline())
                {
                    if(hasLeftVerline())
                    {
                        switch(inputDirection)
                        {
                            case 0:
                                turnLeft();
                                break;
                            case 1:
                                down();
                                break;
                            case 2:
                            case 3:
                                turnLeft();
                                break;
                        }
                    }
                    else
                    {
                        if(hasRightVerline())
                        {
                            switch(inputDirection)
                            {
                                case 0:
                                case 1:
                                case 2:
                                    turnRight();
                                    break;
                                case 3:
                                    down();
                                 break;
                            }
                        }
                    }
                }
                else
                    down();    
            }
        }
    }
    
    private void addPoint()
    {
        int pointIndex=SpiderWebGameFunction.getPointIndex(pointRow,pointColumn);
        pointIndexJsonArray.add(new JsonPrimitive(pointIndex));
    }
    
    private void turnLeft()
    {
        //System.out.println("Turn left");
        pointColumn--;
        priorityDirection=3;
        inputDirection=3;
    }
    
    public void turnRight()
    {
        //System.out.println("Turn right");
        pointColumn++;
        priorityDirection=1;
        inputDirection=1;
    }
    
    public void down()
    {
        //System.out.println("Down");
        pointRow++;
        inputDirection=2;
    }
    
    private boolean hasLeftVerline()
    {
        if(pointColumn==1)
            return false;
        ArrayList<Integer> questionArr=
                        TITFunction.covertToIntArrayList(verLineIndexJsonArray);
        int leftVerLineIndex=(pointColumn-2)+(pointRow-1)*4;
        return questionArr.contains(new Integer(leftVerLineIndex));
    }
    
    private boolean hasRightVerline()
    {
        if(pointColumn==5)
            return false;
        ArrayList<Integer> questionArr=
                        TITFunction.covertToIntArrayList(verLineIndexJsonArray);
        int rightVerLineIndex=pointColumn-1+(pointRow-1)*4;
        return questionArr.contains(new Integer(rightVerLineIndex));
    }

    public JsonArray getPointIndexJsonArray() 
    {
        return pointIndexJsonArray;
    }    
}

class SpiderWebGameFunction
{
    public static int getIndex(int verLineRow,int verLineColumn)
    {
        return (verLineRow-1)*4+(verLineColumn-1);
    }

    public static int getVerLineRow(int verLineIndex)
    {
        return (verLineIndex/4)+1;
    }

    public static int getVerLineColum(int verLineIndex)
    {
        return verLineIndex%4+1;
    }
    
    public static int getPointIndex(int pointRow,int pointColumn)
    {
        return pointColumn-1+(pointRow-1)*5;
    }
    
    public static int getPointRow(int pointIndex)
    {
        return pointIndex/5+1;
    }
    
    public static int getPointColumn(int pointIndex)
    {
        return pointIndex%5+1;
    }
    
}