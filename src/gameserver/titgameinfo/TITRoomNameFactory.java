/*
 * Decompiled with CFR 0_110.
 */
package gameserver.titgameinfo;

import java.util.ArrayList;

public class TITRoomNameFactory {
    private static ArrayList<Integer> normalRoomArr = new ArrayList();
    private static ArrayList<Integer> challengeRoomArr = new ArrayList();

    public static Integer getNewNornalRoomNameId() {
        return (Integer)TITRoomNameFactory.normalRoomArrSynMethod("getNewNormalRoomNameId", null);
    }

    public static Integer getNewChallengeRoomNameId() {
        return (Integer)TITRoomNameFactory.challengeRoomArrSynMethod("getNewChallengeRoomNameId", null);
    }

    public static void removeNormalRoomNameId(Integer nameId) {
        TITRoomNameFactory.normalRoomArrSynMethod("removeNormalRoomNameId", nameId);
    }

    public static void removeChallengeRoomNameId(Integer nameId) {
        TITRoomNameFactory.challengeRoomArrSynMethod("removeChallengeRoomNameId", nameId);
    }

    private static synchronized Object normalRoomArrSynMethod(String nameMethod, Object obj) {
        switch (nameMethod) {
            case "getNewNormalRoomNameId": {
                return TITRoomNameFactory.privateGetNewNormalRoomNameId();
            }
            case "removeNormalRoomNameId": {
                Integer nameId = (Integer)obj;
                TITRoomNameFactory.privateRemoveNormalRoomNameId(nameId);
                return null;
            }
        }
        return null;
    }

    private static Integer privateGetNewNormalRoomNameId() {
        int n = 1;
        int i = 0;
        do {
            if (i >= normalRoomArr.size()) {
                normalRoomArr.add(i, n);
                return n;
            }
            int currentId = normalRoomArr.get(i);
            if (currentId != n) break;
            ++n;
            ++i;
        } while (true);
        normalRoomArr.add(i, n);
        return n;
    }

    private static void privateRemoveNormalRoomNameId(Integer nameId) {
        normalRoomArr.remove(nameId);
    }

    private static synchronized Object challengeRoomArrSynMethod(String nameMethod, Object obj) {
        switch (nameMethod) {
            case "getNewChallengeRoomNameId": {
                return TITRoomNameFactory.privateGetNewChallengeRoomNameId();
            }
            case "removeChallengeRoomNameId": {
                Integer nameId = (Integer)obj;
                TITRoomNameFactory.privateRemoveChallengeRoomNameId(nameId);
                return null;
            }
        }
        return null;
    }

    private static Integer privateGetNewChallengeRoomNameId() {
        int n = 1;
        int i = 0;
        do {
            if (i >= challengeRoomArr.size()) {
                challengeRoomArr.add(i, n);
                return n;
            }
            int currentId = challengeRoomArr.get(i);
            if (currentId != n) break;
            ++n;
            ++i;
        } while (true);
        challengeRoomArr.add(i, n);
        return n;
    }

    private static void privateRemoveChallengeRoomNameId(Integer nameId) {
        challengeRoomArr.remove(nameId);
    }
}

