package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class TetrisGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        ArrayList<Integer> wallIndexArr;
        int maxPoint=1500;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,300);
        jsonObject.addProperty(KJS.PARAM2,40);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        int currentBrickType=0;
        int nextBrickType=TITFunction.randInt(7);
        
        for (int i=1;i<=10;i++) 
        {
            jsonObject = new JsonObject();
            
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewBrick");
            jsonObject.addProperty(KJS.INDEX,index);
            currentBrickType=nextBrickType;
            jsonObject.addProperty(KJS.PARAM1,currentBrickType);
            nextBrickType=TITFunction.randInt(7);
            jsonObject.addProperty(KJS.PARAM2,nextBrickType);
            jsonObject.addProperty(KJS.PARAM3,300);
            gameScript.add(jsonObject);
            index++;
            
            jsonObject = new JsonObject();
            gameAnswer.add(jsonObject);
        }
        
        for (int i=1;i<=10;i++) 
        {
            jsonObject = new JsonObject();
            
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewBrick");
            jsonObject.addProperty(KJS.INDEX,index);
            currentBrickType=nextBrickType;
            jsonObject.addProperty(KJS.PARAM1,currentBrickType);
            nextBrickType=TITFunction.randInt(7);
            jsonObject.addProperty(KJS.PARAM2,nextBrickType);
            jsonObject.addProperty(KJS.PARAM3,250);
            gameScript.add(jsonObject);
            index++;
            
            jsonObject = new JsonObject();
            gameAnswer.add(jsonObject);
        }
        
        for (int i=1;i<=10;i++) 
        {
            jsonObject = new JsonObject();
            
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewBrick");
            jsonObject.addProperty(KJS.INDEX,index);
            currentBrickType=nextBrickType;
            jsonObject.addProperty(KJS.PARAM1,currentBrickType);
            nextBrickType=TITFunction.randInt(7);
            jsonObject.addProperty(KJS.PARAM2,nextBrickType);
            jsonObject.addProperty(KJS.PARAM3,200);
            gameScript.add(jsonObject);
            index++;
            
            jsonObject = new JsonObject();
            gameAnswer.add(jsonObject);
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }
    
    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int rowFullCount=userGameAnswer.get(KJS.PARAM1).getAsInt();
        
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
        jsonObject.addProperty(KJS.VALUE,50*rowFullCount*rowFullCount);
        result.add(jsonObject);
        
        jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
        jsonObject.addProperty(KJS.VALUE,rowFullCount);
        result.add(jsonObject);
        return result;
    }
}
