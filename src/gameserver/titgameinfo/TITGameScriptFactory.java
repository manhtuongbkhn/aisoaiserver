package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class TITGameScriptFactory 
{
    public static JsonObject creteGameScript(int gameId, int hardLevel) 
    {
        switch (gameId) 
        {
            case 1:
                return CircleGameScriptFactory.createGameScript(hardLevel);
            case 2:
                return ZombieGameScriptFactory.createGameScript(hardLevel);
            case 3:
                return AuditionGameScriptFactory.createGameScript(hardLevel);
            case 4:
                return OperationGameScriptFactory.createGameScript(hardLevel);
            case 5:
                return FindPokemonGameScriptFactory.createGameScript(hardLevel);
            case 6:
                return ChoiceModGameScriptFactory.createGameScript(hardLevel);
            case 7:
                return CollectFruitGameScriptFactory.createGameScript(hardLevel);
            case 8:
                return PianoGameScriptFactory.createGameScript(hardLevel);
            case 9:
                return DoublePokemonGameScriptFactory.createGameScript(hardLevel);
            case 10:
                return FlappyDoraemonGameScriptFactory.createGameScript(hardLevel);
            case 11:
                return DinosaurJumpGameScriptFactory.createGameScript(hardLevel);
            case 12:
                return SortRecGameScriptFactory.createGameScript(hardLevel);
            case 13:
                return SpiderWebGameScriptFactory.createGameScript(hardLevel);
            case 14:
                return SnakeHuntingGameScriptFactory.createGameScript(hardLevel);
            case 15:
                return TetrisGameScriptFactory.createGameScript(hardLevel);
            default:
                return CircleGameScriptFactory.createGameScript(hardLevel);
        }
    }

    public static JsonArray checkGameAnswer
                    (int gameId, JsonArray gameAnswer,JsonObject userGameAnswer) 
    {
        switch (gameId) 
        {
            case 1:
                return CircleGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 2:
                return ZombieGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 3:
                return AuditionGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 4:
                return OperationGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 5:
                return FindPokemonGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 6:
                return ChoiceModGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 7:
                return CollectFruitGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 8:
                return PianoGameScriptFactory.
                                        checkAnswer(gameAnswer, userGameAnswer);
            case 9:
                return DoublePokemonGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 10:
                return FlappyDoraemonGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 11:
                return DinosaurJumpGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 12:
                return SortRecGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 13:
                return SpiderWebGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 14:
                return SnakeHuntingGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
            case 15:
                return TetrisGameScriptFactory.checkAnswer
                                                    (gameAnswer,userGameAnswer);
            default:
                return CircleGameScriptFactory.
                                        checkAnswer(gameAnswer,userGameAnswer);
        }
    }
}

