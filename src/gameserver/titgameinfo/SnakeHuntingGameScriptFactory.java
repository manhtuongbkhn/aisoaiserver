package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class SnakeHuntingGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        ArrayList<Integer> wallIndexArr;
        int maxPoint=0;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        jsonObject.addProperty(KJS.PARAM4,200);
        SnakeHuntingGamneMapFactory.createMap(jsonObject);
        wallIndexArr=TITFunction.covertToIntArrayList
                                (jsonObject.get(KJS.PARAM7).getAsJsonArray());
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        for (int i=1;i<=25;i++) 
        {
            jsonObject = new JsonObject();
            
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewApple");
            jsonObject.addProperty(KJS.INDEX,index);
            int appleIndex1=createQuestion(wallIndexArr);
            jsonObject.addProperty(KJS.PARAM1,appleIndex1);
            int appleIndex2=createQuestion(wallIndexArr);
            jsonObject.addProperty(KJS.PARAM2,appleIndex2);
            int appleIndex3=createQuestion(wallIndexArr);
            jsonObject.addProperty(KJS.PARAM3,appleIndex3);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+40;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }
    
    private static int createQuestion(ArrayList<Integer> wallIndexArr)
    {
        while(true)
        {
            int radom=TITFunction.randInt(320)-1;
            if(!wallIndexArr.contains(new Integer(radom)))
                return radom;
        }
    }
    
    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        if(index>0)
        {
            JsonObject systemGameAnswer = systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
            int point=systemGameAnswer.get(KJS.POINT).getAsInt();
            boolean userAnswerColumn=userGameAnswer.
                                                get(KJS.PARAM1).getAsBoolean();
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        else
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.MUL_POINT);
            jsonObject.addProperty(KJS.VALUE,0.5f);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
    }
}

class SnakeHuntingGamneMapFactory
{
    public static JsonObject createMap(JsonObject jsonObject)
    {
        int radom=TITFunction.randInt(1);
        switch(radom)
        {
            case 1:
            default:
                return map1(jsonObject);
        }
    }
    
    private static JsonObject map1(JsonObject jsonObject)
    {
        JsonArray jsonArray=new JsonArray();
        JsonObject startPostion=new JsonObject();
        
        JsonObject wall=new JsonObject();
        wall.addProperty(KJS.PARAM1,4);
        wall.addProperty(KJS.PARAM2,5);
        wall.addProperty(KJS.PARAM3,12);
        wall.addProperty(KJS.PARAM4,2);
        jsonArray.add(wall);
        
        wall=new JsonObject();
        wall.addProperty(KJS.PARAM1,12);
        wall.addProperty(KJS.PARAM2,5);
        wall.addProperty(KJS.PARAM3,12);
        wall.addProperty(KJS.PARAM4,2);
        jsonArray.add(wall);
        JsonArray wallIndexJsonArray=new JsonArray();
        
        for(int row=4;row<=5;row++)
        {
            for(int column=5;column<=16;column++)
            {
                int wallIndex=SnakeHuntingGameFunction.getItemIndex(row,column);
                wallIndexJsonArray.add(new JsonPrimitive(wallIndex));
            }
        }
        
        for(int row=12;row<=13;row++)
        {
            for(int column=5;column<=16;column++)
            {
                int wallIndex=SnakeHuntingGameFunction.getItemIndex(row,column);
                wallIndexJsonArray.add(new JsonPrimitive(wallIndex));
            }
        }
        
        startPostion.addProperty(KJS.PARAM1,8);
        startPostion.addProperty(KJS.PARAM2,11);
        startPostion.addProperty(KJS.PARAM3,1);
        startPostion.addProperty(KJS.PARAM4,2);
        
        jsonObject.add(KJS.PARAM5,jsonArray);
        jsonObject.add(KJS.PARAM6,startPostion);
        jsonObject.add(KJS.PARAM7,wallIndexJsonArray);
        return jsonObject;
    }
}

class SnakeHuntingGameFunction
{    
    public static int getItemIndex(int itemRow,int itemColumn)
    {
        return itemColumn-1+(itemRow-1)*20;
    }
    
    public static int getItentRow(int itemIndex)
    {
        return itemIndex/20+1;
    }
    
    public static int getItemColumn(int itemIndex)
    {
        return itemIndex%20+1;
    }   
}