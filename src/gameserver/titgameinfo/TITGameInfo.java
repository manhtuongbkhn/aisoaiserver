package gameserver.titgameinfo;

import com.smartfoxserver.v2.entities.data.SFSObject;
import gameserver.titconfig.KJS;

public class TITGameInfo 
{
    private int gameId;
    private String gameName;
    private String gameMainType;
    private String gameSecondaryType;
    private String gameGuide;

    public int getGameId() 
    {
        return this.gameId;
    }

    public void setGameId(int gameId) 
    {
        this.gameId = gameId;
    }

    public String getGameName() 
    {
        return this.gameName;
    }

    public void setGameName(String gameName) 
    {
        this.gameName = gameName;
    }

    public String getGameMainType() 
    {
        return this.gameMainType;
    }

    public void setGameMainType(String gameMainType) 
    {
        this.gameMainType = gameMainType;
    }

    public String getGameSecondaryType() 
    {
        return this.gameSecondaryType;
    }

    public void setGameSecondaryType(String gameSecondaryType) 
    {
        this.gameSecondaryType = gameSecondaryType;
    }

    public String getGameGuide() 
    {
        return this.gameGuide;
    }

    public void setGameGuide(String gameGuide) 
    {
        this.gameGuide = gameGuide;
    }

    public SFSObject toSFSObject() 
    {
        SFSObject sfsObject = new SFSObject();
        sfsObject.putInt(KJS.GAME_ID,gameId);
        sfsObject.putUtfString(KJS.GAME_NAME,gameName);
        sfsObject.putUtfString(KJS.GAME_MAIN_TYPE,gameMainType);
        sfsObject.putUtfString(KJS.GAME_SECONDARY_TYPE,gameSecondaryType);
        return sfsObject;
    }
}

