package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class AuditionGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default:
                return AuditionGameScriptFactory.createEasyGameScript();
        }
    }

    private static JsonObject createEasyGameScript() 
    {
        int directionQuestion;
        int type;
        JsonArray answerArray;
        int i;
        JsonArray questionArray;
        int level;
        int direction;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int index = 1;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,75);
        jsonObject.addProperty(KJS.PARAM2,2.0f);
        gameScript.add(jsonObject);
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,2000);
        jsonObject.addProperty(KJS.PARAM2,false);
        gameScript.add(jsonObject);
        
        int maxPoint=0;
        for (level=1;level<=8;level++) 
        {
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewDirectionQuestion");
            questionArray = new JsonArray();
            answerArray = new JsonArray();
            for (i = 1; i <= level; ++i) 
            {
                direction = TITFunction.randInt(4);
                type = level == 8 && i == 6 ? 2 : 1;
                directionQuestion = (type - 1) * 4 + direction;
                questionArray.add(new JsonPrimitive(directionQuestion));
                answerArray.add(new JsonPrimitive(AuditionGameScriptFactory.
                                        getAnswerDirection(directionQuestion)));
            }
            jsonObject.addProperty(KJS.PARAM1,level);
            jsonObject.add(KJS.PARAM2,questionArray);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            gameScript.add(jsonObject);
            
            jsonObject = new JsonObject();
            jsonObject.add(KJS.PARAM1,answerArray);
            jsonObject.addProperty(KJS.POINT,50);
            gameAnswer.add(jsonObject);
            
            maxPoint=maxPoint+50;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,4.0f);
            gameScript.add(jsonObject);
            gameScript.add(AuditionGameScriptFactory.createSleep(4000,true));
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,1.0f);
            gameScript.add(jsonObject);
            gameScript.add(AuditionGameScriptFactory.createSleep(1000, false));
        }
        
        gameScript.add(AuditionGameScriptFactory.createSleep(1000, false));
        
        for (level = 1; level <= 8; ++level) 
        {
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewDirectionQuestion");
            questionArray = new JsonArray();
            answerArray = new JsonArray();
            for (i = 1; i <= level;i++) 
            {
                direction = TITFunction.randInt(4);
                type = 1;
                directionQuestion = (type - 1) * 4 + direction;
                questionArray.add(new JsonPrimitive(directionQuestion));
                answerArray.add(new JsonPrimitive(AuditionGameScriptFactory.
                                        getAnswerDirection(directionQuestion)));
            }
            jsonObject.addProperty(KJS.PARAM1,level);
            jsonObject.add(KJS.PARAM2,questionArray);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            gameScript.add(jsonObject);
            
            jsonObject = new JsonObject();
            jsonObject.add(KJS.PARAM1,answerArray);
            jsonObject.addProperty(KJS.POINT,75);
            gameAnswer.add(jsonObject);
            
            maxPoint=maxPoint+75;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,3.0f);
            gameScript.add(jsonObject);
            gameScript.add(AuditionGameScriptFactory.createSleep(3000, true));
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,1.0f);
            gameScript.add(jsonObject);
            gameScript.add(AuditionGameScriptFactory.createSleep(1000, false));
        }
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,75);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        return result;
    }

    private static JsonObject createSleep(int millis, boolean playing) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        jsonObject.addProperty(KJS.PARAM2,playing);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr, JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonArray userAnswerArr = userGameAnswer.get(KJS.PARAM1).getAsJsonArray();
        float postionPercent = userGameAnswer.get(KJS.PARAM2).getAsFloat();
        JsonObject systemGameAnswer = 
                        systemGameAnswerArr.get(index - 1).getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        JsonArray systemAnswerArr = 
                            systemGameAnswer.get(KJS.PARAM1).getAsJsonArray();
        if (userAnswerArr.equals(systemAnswerArr)) 
        {
            if (66.0f <= postionPercent && postionPercent <= 74.0f) 
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
 
            }
            if (60.0f<=postionPercent&&postionPercent<66.0f||
                                    74.0f<postionPercent&&postionPercent<=80.0f) 
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,0.6f*point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
            if (50.0f<=postionPercent&&postionPercent<60.0f||
                                    80.0f<postionPercent&&postionPercent<=90.0f) 
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,0.4f*point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
            if (50.0f > postionPercent || postionPercent > 90.0f) 
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,-0.4f*point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
        }
        else
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-0.4f*point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        return result;
    }

    public static int getAnswerDirection(int questionDirection) 
    {
        switch (questionDirection) 
        {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 1;
        }
        return 2;
    }

    public static int radomType() 
    {
        int radom = TITFunction.randInt(4);
        switch (radom) 
        {
            case 1: 
            case 2: 
            case 3: 
                return 1;
        }
        return 2;
    }
}

