package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class CollectFruitGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return CollectFruitGameScriptFactory.createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        StatusLaneArr statusLaneArr=new StatusLaneArr();
        float runTime;
        int type;
        int i;
        int postion;
        int maxPoint=0;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int index = 1;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        //Phase 1
        for (i = 1; i<=40; i++) 
        {
            jsonObject = new JsonObject();
            type = TITFunction.randInt(8);
            if(type==8) type=7;
            
            switch(type)
            {
                case 1:
                    runTime=0.75f;
                    break;
                case 2:
                    runTime=0.8f;
                    break;
                case 3:
                    runTime=0.85f;
                    break;
                case 4:
                    runTime=0.9f;
                    break;
                case 5:
                    runTime=0.95f;
                    break;
                case 6:
                case 7:
                default:
                    runTime=1.0f;
                    break;
            }
            
            postion = statusLaneArr.getNewRadomLane(2);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunSprite");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            jsonObject.addProperty(KJS.PARAM3,postion);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+10;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,10);
            gameAnswer.add(jsonObject);
            gameScript.add(CollectFruitGameScriptFactory.createSleep(750));
        }
        gameScript.add(CollectFruitGameScriptFactory.createSleep(1000));
        
        //Phase 2
        for (i = 1; i <= 30; i++) 
        {
            jsonObject = new JsonObject();
            type = TITFunction.randInt(8);
            if(type==8) type=7;
            
            switch(type)
            {
                case 1:
                    runTime=0.75f;
                    break;
                case 2:
                    runTime=0.8f;
                    break;
                case 3:
                    runTime=0.85f;
                    break;
                case 4:
                    runTime=0.9f;
                    break;
                case 5:
                    runTime=0.95f;
                    break;
                case 6:
                case 7:
                default:
                    runTime=1.0f;
                    break;
            }
            postion = statusLaneArr.getNewRadomLane(2);
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewRunSprite");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            jsonObject.addProperty(KJS.PARAM3,postion);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+20;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,20);
            gameAnswer.add(jsonObject);
            gameScript.add(CollectFruitGameScriptFactory.createSleep(500));
        }
        gameScript.add(CollectFruitGameScriptFactory.createSleep(1000));
        
        //Phase 3
        for (i = 1; i <= 24;i++) 
        {
            jsonObject = new JsonObject();
            type = TITFunction.randInt(8);
            if(type==8) type=7;
            
            switch(type)
            {
                case 1:
                    runTime=0.6f;
                    break;
                case 2:
                    runTime=0.65f;
                    break;
                case 3:
                    runTime=0.7f;
                    break;
                case 4:
                    runTime=0.75f;
                    break;
                case 5:
                    runTime=0.8f;
                    break;
                case 6:
                case 7:
                default:
                    runTime=0.85f;
                    break;
            }
            
            postion = statusLaneArr.getNewRadomLane(2);
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewRunSprite");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            jsonObject.addProperty(KJS.PARAM3,postion);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+30;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,30);
            gameAnswer.add(jsonObject);
            gameScript.add(CollectFruitGameScriptFactory.createSleep(500));
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }
    
    public static ArrayList<Integer> initStatusLaneArr()
    {
        ArrayList<Integer> statusLaneArr=new ArrayList<Integer>();
        for(int i=0;i<8;i++)
        {
            statusLaneArr.add(0);
        }
        return statusLaneArr;
    }
    
    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        int finishType = userGameAnswer.get(KJS.PARAM1).getAsInt();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index - 1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        int typeRunSprite = systemGameAnswer.get(KJS.PARAM1).getAsInt();
        if (typeRunSprite == 7) 
        {
            if (finishType == 1)
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
            else
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
        }
        else
        {
            if (finishType == 1) 
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
            else
            {
                JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                result.add(jsonObject);
                jsonObject=new JsonObject();
                jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                jsonObject.addProperty(KJS.VALUE,1);
                result.add(jsonObject);
                return result;
            }
        }       
    }
}

class StatusLaneArr
{
    private ArrayList<Integer> statusLaneArr;
    
    public StatusLaneArr()
    {
        statusLaneArr=new ArrayList<Integer>();
        for(int i=0;i<8;i++)
        {
            statusLaneArr.add(0);
        }
    }
    
    public int getNewRadomLane(int delay)
    {
        ArrayList<Integer> spaceLaneArr=getSpaceLaneArr();
        int radomIndex=TITFunction.randInt(spaceLaneArr.size());
        int laneIndex=spaceLaneArr.get(radomIndex-1);
        statusLaneArr.set(laneIndex-1,delay+1);
        reduceStatusArr();
        return laneIndex;
    }
    
    public ArrayList<Integer> getSpaceLaneArr()
    {
        ArrayList<Integer> spaceLaneArr=new ArrayList<Integer>();
        for(int i=0;i<8;i++)
        {
            int status=statusLaneArr.get(i);
            if(status==0)
                spaceLaneArr.add(i+1);
        }
        return spaceLaneArr;
    }
    
    public void reduceStatusArr()
    {
        for(int i=0;i<8;i++)
        {
            int status=statusLaneArr.get(i);
            if(status!=0)
                statusLaneArr.set(i,status-1);
        }
    }
    
    public void print()
    {
        for(int i=0;i<8;i++)
        {
            int status=statusLaneArr.get(i);
            System.out.print(status+"\t");
        }
        System.out.println();
    }
}

