package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class CircleGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) {
            default: 
        }
        return CircleGameScriptFactory.createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int status;
        int postion;
        int i;
        int maxPoint=0;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int index = 1;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        
        jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,1f);
        gameScript.add(jsonObject);
        //Phase 1
        for (i=1; i<=30; ++i) 
        {
            jsonObject = new JsonObject();
            postion = TITFunction.randInt(4);
            status = TITFunction.randInt(2);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,status-1);
            jsonObject.addProperty(KJS.PARAM3,1.2f);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            maxPoint=maxPoint+20;
            gameScript.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,status-1);
            jsonObject.addProperty(KJS.POINT,20);
            gameAnswer.add(jsonObject);
            gameScript.add(CircleGameScriptFactory.createSleep(750));
        }
        
        jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"enbleGreenCircle");
        gameScript.add(jsonObject);
        
        gameScript.add(CircleGameScriptFactory.createSleep(1000));
        
        //Phase 2
        for (i=1;i<=25;++i) 
        {
            jsonObject = new JsonObject();
            postion = TITFunction.randInt(4);
            status = TITFunction.randInt(3);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,status-1);
            jsonObject.addProperty(KJS.PARAM3,1.2f);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            maxPoint=maxPoint+30;
            gameScript.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,status-1);
            jsonObject.addProperty(KJS.POINT,30);
            gameAnswer.add(jsonObject);
            gameScript.add(CircleGameScriptFactory.createSleep(750));
        }
        
        gameScript.add(CircleGameScriptFactory.createSleep(1000));
        
        //Phase 3
        for (i=0;i<21;i++) 
        {
            jsonObject = new JsonObject();
            postion = TITFunction.randInt(4);
            status = TITFunction.randInt(3);
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewRunCircle");
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,status - 1);
            jsonObject.addProperty(KJS.PARAM3,1.0f);
            jsonObject.addProperty(KJS.INDEX,index);
            index++;
            maxPoint=maxPoint+40;
            gameScript.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,status-1);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
            gameScript.add(CircleGameScriptFactory.createSleep(750));
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        return result;
    }

    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        int userLStatus = userGameAnswer.get(KJS.PARAM1).getAsInt();
        int userRStatus = userGameAnswer.get(KJS.PARAM2).getAsInt();
        JsonObject systemGameAnswer = systemGameAnswerArr.
                                                get(index-1).getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        switch (systemGameAnswer.get(KJS.PARAM1).getAsInt()) 
        {
            case 1: 
            case 4:
                int lStatus = systemGameAnswer.get(KJS.PARAM2).getAsInt();
                if (userLStatus == lStatus)
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
                else
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
            case 2: 
            case 3:
                int rStatus = systemGameAnswer.get(KJS.PARAM2).getAsInt();
                if (userRStatus == rStatus)
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
                else
                {
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
                    jsonObject.addProperty(KJS.VALUE,-0.5f*point);
                    result.add(jsonObject);
                    jsonObject=new JsonObject();
                    jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
                    jsonObject.addProperty(KJS.VALUE,1);
                    result.add(jsonObject);
                    return result;
                }
        }
        return result;
    }
}

