package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class PianoGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return PianoGameScriptFactory.createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        int i;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int maxPoint=0;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        //Phase 1
        for (i = 1; i <= 20;i++) 
        {
            int type=1;
            postion = TITFunction.randInt(4);
            runTime = 3.5f;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewButton");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            jsonObject.addProperty(KJS.PARAM3,type);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+30;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,30);
            gameAnswer.add(jsonObject);
            gameScript.add(PianoGameScriptFactory.createSleep(1000));
        }
        
        //Phase 2
        
        for (i = 1; i <= 20;i++) 
        {
            postion = TITFunction.randInt(4);
            runTime = 3.0f;
            int type=TITFunction.randInt(3);
            
            if(type==3)
                type=2;
            else
                type=1;
            
            if(type==2)
            {
                jsonObject=new JsonObject();
                Integer orangeLinePostion=TITFunction.randInt(50);
                orangeLinePostion=orangeLinePostion+33;
                jsonObject.addProperty(KJS.METHOD_NAME,"changeOrangeLine");
                jsonObject.addProperty(KJS.PARAM1,orangeLinePostion.floatValue());
                gameScript.add(jsonObject);
            }
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewButton");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            jsonObject.addProperty(KJS.PARAM3,type);
            gameScript.add(jsonObject);
            index++;
            
            if(type==1)
                maxPoint=maxPoint+30;
            else
                maxPoint=maxPoint+40;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            if(type==1)
                jsonObject.addProperty(KJS.POINT,30);
            if(type==2)
                jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
            gameScript.add(PianoGameScriptFactory.createSleep(1000));
        }
        
        //Phase 3
        for (i = 1; i <=20;i++) 
        {
            jsonObject = new JsonObject();
            postion = TITFunction.randInt(4);
            runTime = 2.5f;
            int type=TITFunction.randInt(2);
            
            if(type==1)
            {
                jsonObject=new JsonObject();
                Integer greenLinePostion=TITFunction.randInt(50);
                greenLinePostion=greenLinePostion+33;
                jsonObject.addProperty(KJS.METHOD_NAME,"changeGreenLine");
                jsonObject.addProperty(KJS.PARAM1,greenLinePostion.floatValue());
                gameScript.add(jsonObject);
            }
            
            if(type==2)
            {
                jsonObject=new JsonObject();
                Integer orangeLinePostion=TITFunction.randInt(50);
                orangeLinePostion=orangeLinePostion+33;
                jsonObject.addProperty(KJS.METHOD_NAME,"changeOrangeLine");
                jsonObject.addProperty(KJS.PARAM1,orangeLinePostion.floatValue());
                gameScript.add(jsonObject);
            }
            
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewButton");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,postion);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            jsonObject.addProperty(KJS.PARAM3,type);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+40;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,type);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
            gameScript.add(PianoGameScriptFactory.createSleep(1000));
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }

    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        float deltaYPercent=userGameAnswer.get(KJS.PARAM1).getAsFloat();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index - 1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        int type=systemGameAnswer.get(KJS.PARAM1).getAsInt();
        if(deltaYPercent<0||deltaYPercent>40)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-0.5f*point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        
        if(0<=deltaYPercent&&deltaYPercent<8)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        
        if(8<deltaYPercent&&deltaYPercent<=20)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,0.8f*point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        
        if(20<=deltaYPercent&&deltaYPercent<40)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,0.4f*point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        return result;
    }
}

