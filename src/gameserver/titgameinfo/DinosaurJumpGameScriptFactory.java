package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class DinosaurJumpGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default: 
        }
        return createEasyGameScript();
    }

    private static JsonObject createEasyGameScript() 
    {
        int postion;
        float runTime;
        int i;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int maxPoint=0;
        int index = 1;
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        float mScale=2f/(1.25f*1.25f*9.8f);
        jsonObject.addProperty(KJS.PARAM2,mScale);
        gameScript.add(jsonObject);
        
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        //Phase 1
        for (i = 1; i <= 14;i++) 
        {
            int cactusType=TITFunction.randInt(3);
            runTime = 2f;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewCactus");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,cactusType);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+30;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.POINT,30);
            gameAnswer.add(jsonObject);
            gameScript.add(createSleep(1500));
        }
        gameScript.add(createSleep(1500));
        
        //Phase 2
        for (i = 1; i <= 14;i++) 
        {
            int cactusType=TITFunction.randInt(3);
            runTime = 1.5f;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewCactus");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,cactusType);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+50;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
            gameScript.add(createSleep(1500));
        }
        gameScript.add(createSleep(1500));
        
        //Phase 3
        for (i = 1; i <= 10;i++) 
        {
            int cactusType=TITFunction.randInt(2)+1;
            runTime = 1.5f;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME, "createNewCactus");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,cactusType);
            jsonObject.addProperty(KJS.PARAM2,runTime);
            gameScript.add(jsonObject);
            index++;
            
            maxPoint=maxPoint+50;
            
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.POINT,50);
            gameAnswer.add(jsonObject);
            gameScript.add(createSleep(1500));
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }

    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonObject systemGameAnswer=systemGameAnswerArr.get(index-1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        int type = userGameAnswer.get(KJS.PARAM1).getAsInt();
        
        if(type==1)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        
        if(type==2)
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        return result;
    }
}
