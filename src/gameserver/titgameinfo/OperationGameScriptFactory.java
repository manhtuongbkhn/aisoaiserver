package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class OperationGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel) 
        {
            default:
                return OperationGameScriptFactory.createEasyGameScript();
        }
    }

    private static JsonObject createEasyGameScript() 
    {
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int index = 1;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        jsonObject.addProperty(KJS.PARAM2,false);
        gameScript.add((JsonElement)jsonObject);
        
        int maxPoint=0;
        for (int level = 1; level <= 50; ++level) 
        {
            jsonObject = OperationGameScriptFactory.createScriptNode();
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            gameScript.add(jsonObject);
                        
            maxPoint=maxPoint+20;
            
            gameAnswer.add(jsonObject);
            jsonObject.addProperty(KJS.POINT,20);
            index++;
        }
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }

    public static JsonObject createScriptNode() 
    {
        int number2;
        int result;
        int number1;
        JsonObject jsonObject = new JsonObject();
        int operator = TITFunction.randInt(4);
        if (operator == 1 || operator == 2) 
        {
            number1 = TITFunction.randInt(50);
            number2 = TITFunction.randInt(50);
            result = number1 + number2;
            if (operator == 1) 
            {
                jsonObject.addProperty(KJS.PARAM1,number1);
                jsonObject.addProperty(KJS.PARAM2,number2);
                jsonObject.addProperty(KJS.PARAM3,result);
            }
            if (operator == 2) 
            {
                jsonObject.addProperty(KJS.PARAM1,result);
                jsonObject.addProperty(KJS.PARAM2,number1);
                jsonObject.addProperty(KJS.PARAM3,number2);
            }
        }
        if (operator == 3 || operator == 4) 
        {
            number1 = TITFunction.randInt(10);
            number2 = TITFunction.randInt(10);
            result = number1 * number2;
            if (operator == 3) 
            {
                jsonObject.addProperty(KJS.PARAM1,number1);
                jsonObject.addProperty(KJS.PARAM2,number2);
                jsonObject.addProperty(KJS.PARAM3,result);
            }
            if (operator == 4) 
            {
                jsonObject.addProperty(KJS.PARAM1,result);
                jsonObject.addProperty(KJS.PARAM2,number1);
                jsonObject.addProperty(KJS.PARAM3,number2);
            }
        }
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        JsonObject systemGameAnswer=systemGameAnswerArr.get(index - 1).
                                                            getAsJsonObject();
        int point=systemGameAnswer.get(KJS.POINT).getAsInt();
        int operator = userGameAnswer.get(KJS.PARAM1).getAsInt();
        if (OperationGameScriptFactory.checkOperator(operator,systemGameAnswer)) 
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        else
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-point);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
    }

    private static boolean checkOperator
                                    (int operator,JsonObject systemGameAnswer) 
    {
        int number1 = systemGameAnswer.get(KJS.PARAM1).getAsInt();
        int number2 = systemGameAnswer.get(KJS.PARAM2).getAsInt();
        int result = systemGameAnswer.get(KJS.PARAM3).getAsInt();
        switch (operator) 
        {
            case 1:
                return number1 + number2 == result;
            case 2:
                return number1 - number2 == result;
            case 3:
                return number1 * number2 == result;
        }
        return number1 / number2 == result;
    }
}

