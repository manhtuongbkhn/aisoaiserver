package gameserver.titgameinfo;

class GameTypeDefine 
{
    public static final String SPEED = "Nhanh Nh\u1ea1y";
    public static final String OBSERVE = "Quan S\u00e1t";
    public static final String MEMORY = "Ghi Nh\u1edb";
    public static final String LOGIC = "Logic";
    public static final String CALCULATE = "T\u00ednh To\u00e1n";

    GameTypeDefine() 
    {
        
    }
}

