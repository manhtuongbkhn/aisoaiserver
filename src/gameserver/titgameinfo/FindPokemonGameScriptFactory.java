package gameserver.titgameinfo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import gameserver.titconfig.TITFunction;
import gameserver.titconfig.KJS;

public class FindPokemonGameScriptFactory 
{
    public static JsonObject createGameScript(int hardLevel) 
    {
        switch (hardLevel)
        {
            default:
                return FindPokemonGameScriptFactory.createEasyGameScript();
        }
    }

    private static JsonObject createEasyGameScript() 
    {
        int differentRow;
        int pokemonType;
        int differentColumn;
        int level;
        int heightRow;
        int widthColumn;
        JsonArray gameScript = new JsonArray();
        JsonArray gameAnswer = new JsonArray();
        int index = 1;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME, "init");
        jsonObject.addProperty(KJS.PARAM1,60);
        gameScript.add(jsonObject);
        jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,1000);
        gameScript.add(jsonObject);
        
        int maxPoint=0;
        
        //Phase 1
        for (level=1;level<=8;level++) 
        {
            jsonObject = new JsonObject();
            pokemonType = TITFunction.randInt(7);
            heightRow = FindPokemonGameScriptFactory.getHeightRow(level);
            widthColumn = FindPokemonGameScriptFactory.getWidthColumn(level);
            differentRow = TITFunction.randInt(heightRow);
            differentColumn = TITFunction.randInt(widthColumn);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewPokemonQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,pokemonType);
            jsonObject.addProperty(KJS.PARAM2,heightRow);
            jsonObject.addProperty(KJS.PARAM3,widthColumn);
            jsonObject.addProperty(KJS.PARAM4,differentRow);
            jsonObject.addProperty(KJS.PARAM5,differentColumn);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+40;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,differentRow);
            jsonObject.addProperty(KJS.PARAM2,differentColumn);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
        }
       
        //Phase 2
        for (level=3;level<=8;level++) 
        {
            jsonObject = new JsonObject();
            pokemonType = TITFunction.randInt(7);
            heightRow = FindPokemonGameScriptFactory.getHeightRow(level);
            widthColumn = FindPokemonGameScriptFactory.getWidthColumn(level);
            differentRow = TITFunction.randInt(heightRow);
            differentColumn = TITFunction.randInt(widthColumn);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewPokemonQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,pokemonType);
            jsonObject.addProperty(KJS.PARAM2,heightRow);
            jsonObject.addProperty(KJS.PARAM3,widthColumn);
            jsonObject.addProperty(KJS.PARAM4,differentRow);
            jsonObject.addProperty(KJS.PARAM5,differentColumn);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+40;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,differentRow);
            jsonObject.addProperty(KJS.PARAM2,differentColumn);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
        }
        
        //Phase 3
        for (level=5;level<=8;level++) 
        {
            jsonObject = new JsonObject();
            pokemonType = TITFunction.randInt(7);
            heightRow = FindPokemonGameScriptFactory.getHeightRow(level);
            widthColumn = FindPokemonGameScriptFactory.getWidthColumn(level);
            differentRow = TITFunction.randInt(heightRow);
            differentColumn = TITFunction.randInt(widthColumn);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewPokemonQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,pokemonType);
            jsonObject.addProperty(KJS.PARAM2,heightRow);
            jsonObject.addProperty(KJS.PARAM3,widthColumn);
            jsonObject.addProperty(KJS.PARAM4,differentRow);
            jsonObject.addProperty(KJS.PARAM5,differentColumn);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+40;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,differentRow);
            jsonObject.addProperty(KJS.PARAM2,differentColumn);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
        }
        
        //Phase 4
        for (level=7;level<=8;level++) 
        {
            jsonObject = new JsonObject();
            pokemonType = TITFunction.randInt(7);
            heightRow = FindPokemonGameScriptFactory.getHeightRow(level);
            widthColumn = FindPokemonGameScriptFactory.getWidthColumn(level);
            differentRow = TITFunction.randInt(heightRow);
            differentColumn = TITFunction.randInt(widthColumn);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewPokemonQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,pokemonType);
            jsonObject.addProperty(KJS.PARAM2,heightRow);
            jsonObject.addProperty(KJS.PARAM3,widthColumn);
            jsonObject.addProperty(KJS.PARAM4,differentRow);
            jsonObject.addProperty(KJS.PARAM5,differentColumn);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+40;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,differentRow);
            jsonObject.addProperty(KJS.PARAM2,differentColumn);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
        }
       
        for (int i = 1; i <= 5;i++) 
        {
            int level2 = 8;
            jsonObject = new JsonObject();
            int pokemonType2 = TITFunction.randInt(7);
            int heightRow2 = FindPokemonGameScriptFactory.getHeightRow(level2);
            int widthColumn2 = FindPokemonGameScriptFactory.getWidthColumn(level2);
            int differentRow2 = TITFunction.randInt(heightRow2);
            int differentColumn2 = TITFunction.randInt(widthColumn2);
            jsonObject.addProperty(KJS.METHOD_NAME,"createNewPokemonQuestion");
            jsonObject.addProperty(KJS.INDEX,index);
            jsonObject.addProperty(KJS.PARAM1,pokemonType2);
            jsonObject.addProperty(KJS.PARAM2,heightRow2);
            jsonObject.addProperty(KJS.PARAM3,widthColumn2);
            jsonObject.addProperty(KJS.PARAM4,differentRow2);
            jsonObject.addProperty(KJS.PARAM5,differentColumn2);
            gameScript.add(jsonObject);
            index++;
            maxPoint=maxPoint+40;
            jsonObject = new JsonObject();
            jsonObject.addProperty(KJS.PARAM1,differentRow2);
            jsonObject.addProperty(KJS.PARAM2,differentColumn2);
            jsonObject.addProperty(KJS.POINT,40);
            gameAnswer.add(jsonObject);
        }
        
        JsonObject result = new JsonObject();
        result.addProperty(KJS.TIME,60);
        result.add(KJS.GAME_SCRIPT,gameScript);
        result.add(KJS.GAME_ANSWER,gameAnswer);
        result.addProperty(KJS.MAX_POINT,maxPoint);
        return result;
    }

    public static int getWidthColumn(int level) 
    {
        switch (level) 
        {
            case 1: 
            case 2:
                return 2;
            case 3: 
            case 4:
                return 3;
            case 5: 
            case 6:
                return 4;
        }
        return 5;
    }

    public static int getHeightRow(int level) 
    {
        switch (level) 
        {
            case 1:
                return 2;
            case 2: 
            case 3:
                return 3;
            case 4: 
            case 5:
                return 4;
            case 6: 
            case 7:
                return 5;
        }
        return 6;
    }

    private static JsonObject createSleep(int millis) 
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(KJS.METHOD_NAME,"sleep");
        jsonObject.addProperty(KJS.PARAM1,millis);
        return jsonObject;
    }

    public static JsonArray checkAnswer(JsonArray systemGameAnswerArr,
                                                    JsonObject userGameAnswer) 
    {
        JsonArray result=new JsonArray();
        int index = userGameAnswer.get(KJS.INDEX).getAsInt();
        int userAnswerRow = userGameAnswer.get(KJS.PARAM1).getAsInt();
        int userAnswerColumn = userGameAnswer.get(KJS.PARAM2).getAsInt();
        JsonObject systemGameAnswer = systemGameAnswerArr.get(index - 1).
                                                            getAsJsonObject();
        int systemAnswerRow = systemGameAnswer.get(KJS.PARAM1).getAsInt();
        int systemAnswerColumn = systemGameAnswer.get(KJS.PARAM2).getAsInt();
        if (userAnswerRow == systemAnswerRow && 
                                        userAnswerColumn == systemAnswerColumn) 
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,40);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_TRUE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
        else
        {
            JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_POINT);
            jsonObject.addProperty(KJS.VALUE,-15);
            result.add(jsonObject);
            jsonObject=new JsonObject();
            jsonObject.addProperty(KJS.METHOD_NAME,KJS.ADD_FALSE_COUNT);
            jsonObject.addProperty(KJS.VALUE,1);
            result.add(jsonObject);
            return result;
        }
    }
}

